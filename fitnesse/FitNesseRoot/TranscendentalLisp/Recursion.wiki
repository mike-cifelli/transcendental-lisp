---
Test
---
Test recursion capabilities of various functions.

| script                | lisp interpreter fixture                                       |
| show  | evaluate text | (load "lisp/util/big-list.lisp")                               |
| show  | evaluate text | (load "lisp/util/list-builder.lisp")                           |
| check | evaluate text | big-list                                                       | =~/1\)$/   |
| check | evaluate text | (length big-list)                                              | 16384      |
| check | evaluate text | (length (apply 'list big-list))                                | 16384      |
| check | evaluate text | (apply '/ big-list)                                            | 1          |
| check | evaluate text | (apply '* big-list)                                            | 1          |
| check | evaluate text | (apply '+ big-list)                                            | 16384      |
| check | evaluate text | (apply '- big-list)                                            | -16382     |
| check | evaluate text | (apply '= big-list)                                            | T          |
| check | evaluate text | (apply '> (decreasing-list 0 10000))                           | T          |
| check | evaluate text | (apply '< (increasing-list 10000 10000))                       | T          |
| check | evaluate text | (apply 'and big-list)                                          | 1          |
| check | evaluate text | (apply 'or (list-of nil 10000))                                | NIL        |
| check | evaluate text | (eval `(defun big-defun ,(list-of 'x 10000) ,big-list))        | BIG-DEFUN  |
| check | evaluate text | (eval `(define-special special ,(list-of 'x 10000) ,big-list)) | SPECIAL    |
| check | evaluate text | (eval `(defmacro big-macro ,(list-of 'x 10000) ,big-list))     | BIG-MACRO  |
| check | evaluate text | (eval `(lambda ,(list-of 'x 10000) ,big-list))                 | =~/1\)\)$/ |
| check | evaluate text | (apply 'progn big-list)                                        | 1          |
| check | evaluate text | (eval (append `(let ,(list-of '(x 20) 10000)) big-list))       | 1          |
| check | evaluate text | (apply 'cond (list-of '((= 1 2) 1) 10000))                     | NIL        |
| check | evaluate text | (eval (append '(case :a) (list-of '((:b :c :d) 1) 10000)))     | NIL        |
| check | evaluate text | (nested-list 1500)                                             | =~/\)\)$/  |
