---
Test
---
A simple lexical closure.

| script                | lisp interpreter fixture                 |
| show  | evaluate text | (defun adder-x (x) (lambda (y) (+ x y))) |
| show  | evaluate text | (setq adder-20 (adder-x 20))             |
| check | evaluate text | (funcall adder-20 2)                     | 22 |
