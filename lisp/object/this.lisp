(defun counter (initial-count)
  (let ((this) (name)
        (count initial-count))

    (setq name "Counter")

    (setq this
      (dlambda
        (:inc ()
          (setq count (+ count 1)))

        (:inc-3 ()
          (call this :inc)
          (call this :inc)
          (call this :inc))

        (:dec ()
          (setq count (- count 1)))

        (:dec-3 ()
          (call this :dec)
          (call this :dec)
          (call this :dec))

        (:get ()
          count)

        (:set (value)
          (setq count value))

        (t ()
          (cons name count))))))


(let ((instance (counter 0)))
  (defun my-counter (&rest args) (apply instance args)))

(setq my-counter2 (counter 10000))
