;; This is based on an example presented in "Let Over Lambda" by Doug Hoyte.

(let ((direction 'up))
  (defun toggle-counter-direction ()
    (setq direction
          (if (eq direction 'up)
            'down
            'up)))

  (defun counter-class ()
    (let ((counter 0))
      (lambda ()
        (if (eq direction 'up)
          (setq counter (+ counter 1))
          (setq counter (- counter 1)))))))
