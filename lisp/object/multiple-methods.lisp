;; This is based on an example presented in "Let Over Lambda" by Doug Hoyte.

(defun counter-class ()
  (let ((counter 0))
    (lambda (msg)
      (case msg
        ((:inc) (setq counter (+ counter 1)))
        ((:dec) (setq counter (- counter 1)))))))
