
(defun my-append (list-one list-two)
  (append-tail (reverse list-one) list-two))

(defun append-tail (reversed-list list-two)
  (if (null reversed-list) list-two
    (recur (rest reversed-list) (cons (car reversed-list) list-two))))
