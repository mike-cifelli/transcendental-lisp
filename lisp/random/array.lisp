(let ((static))

  (setq static
    (dlambda
      (:create-indices (length)
        (call static :create-indices-tail (- length 1) ()))

      (:create-indices-tail (index accumulator)
        (if (< index 0)
          accumulator
          (recur (- index 1) (cons index accumulator))))))

  (defun array (length)
    (let* ((this)
           (indices (call static :create-indices length))
           (index-bindings (map (λ (i) (list (fuse 'index i))) indices)))

      (eval
        `(let ,index-bindings

          (setq this
            (dlambda
              (:get (i)
                (eval (fuse 'index i)))

              (:set (i value)
                (if (and (< i ,length) (> i -1))
                  (set (fuse 'index i) value)
                  (call this :get i))) ;; show error

              (:length ()
                ,length)

              (t ()
                ((λ (size accumulator)
                   (if (< size 1)
                     accumulator
                     (recur
                       (- size 1)
                       (cons (call this :get (- size 1)) accumulator))))
                 ,length nil)))))))))
