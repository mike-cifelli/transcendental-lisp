;; A list containing the values of single-letter Roman numerals.
(setq roman-number-list
  '((I 1) (V 5) (X 10) (L 50) (C 100) (D 500) (M 1000)))

;; Converts a single Roman numeral letter into its equivalent decimal value.
(defun letter-to-decimal (letter)
  (car
    (cdr
      (
        (lambda (lst f)
          (cond
            ((null lst) ())
            ((eq (car (car lst)) letter) (car lst))
            (t (funcall f (cdr lst) f))))
        roman-number-list
        (lambda (lst f)
          (cond
            ((null lst) ())
            ((eq (car (car lst)) letter) (car lst))
            (t (funcall f (cdr lst) f))))))))
