(defmacro keys (&rest fields)
  `(let ,(mapcar 'list fields)
    (dlambda
      (:get (field) (eval field))
      (:set (field value) (set field value)))))

(defun process-data (data)
  (let ((one (call data :get 'one))
        (two (call data :get 'two))
        (three (call data :get 'three)))
    (list one two three)))

(setq data (keys one two three))
(call data :set 'one "goodbye")
(call data :set 'two "key")
(call data :set 'three "!")
(process-data data)
