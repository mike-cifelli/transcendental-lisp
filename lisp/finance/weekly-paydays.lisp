(let* ((first-payday 5)
       (leap-year 0)
       (days-in-year 365)
       (total-days (+ days-in-year leap-year))
       (one-week 7))

  (+ (/ (- total-days first-payday) one-week) 1))
