(let ((static))

  (setq static
    (dlambda
      (:percent-of-number (n percentage)
        (if (> percentage 0)
          (/ (+ (* n percentage) 50) 100)
          (/ (- (* n percentage) 50) 100)))))

  (defun interest-compounder (initial-principal initial-interest-rate)
    (let ((private) (public)
          (principal initial-principal)
          (interest-rate initial-interest-rate)
          (years-passed 0))

      (setq private
        (dlambda
          (:add-years (years)
            (if (> years 0)
              (setq years-passed (+ years-passed years))))

          (:compound-interest (years)
            (if (> years 0)
              (begin
                (setq principal
                  (+ principal
                    (call static :percent-of-number principal interest-rate)))
                (recur (- years 1)))))))

      (setq public
        (dlambda
          (:get-years-passed ()
            years-passed)

          (:get-principal ()
            principal)

          (:get-interest-rate ()
            interest-rate)

          (:set-interest-rate (new-interest-rate)
            (setq interest-rate new-interest-rate))

          (:make-contribution (contribution)
            (setq principal (+ principal contribution)))

          (:move-forward-years (years)
            (call private :compound-interest years)
            (call private :add-years years)))))))
