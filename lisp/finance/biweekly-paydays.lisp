(let* ((first-payday 5)
       (leap-year 0)
       (days-in-year 365)
       (total-days (+ days-in-year leap-year))
       (two-weeks 14))

  (+ (/ (- total-days first-payday) two-weeks) 1))
