(load "../unit/unit-tester.lisp")
(load "interest-compounder.lisp")

(setq assertions (unit-tester-assertions))

(setq tests
  (let ((compounder))
    (list

      (defun principal-initialized ()
        (setq compounder (interest-compounder 1000 0))
        (call assertions :assert= 1000 (call compounder :get-principal)))

      (defun interest-rate-initialized ()
        (setq compounder (interest-compounder 0 10))
        (call assertions :assert= 10 (call compounder :get-interest-rate)))

      (defun many-years-with-no-interest-rate ()
        (setq compounder (interest-compounder 1000 0))
        (call compounder :move-forward-years 1803)
        (call assertions :assert= 1000 (call compounder :get-principal)))

      (defun no-years-with-positive-interest-rate ()
        (setq compounder (interest-compounder 1000 10))
        (call assertions :assert= 1000 (call compounder :get-principal)))

      (defun one-year-with-positive-interest-rate ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 1)
        (call assertions :assert= 105000 (call compounder :get-principal)))

      (defun two-years-with-positive-interest-rate ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 2)
        (call assertions :assert= 110250 (call compounder :get-principal)))

      (defun three-years-with-positive-interest-rate ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 3)
        (call assertions :assert= 115763 (call compounder :get-principal)))

      (defun four-years-with-positive-interest-rate ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 4)
        (call assertions :assert= 121551 (call compounder :get-principal)))

      (defun one-year-with-negative-interest-rate ()
        (setq compounder (interest-compounder 100000 -5))
        (call compounder :move-forward-years 1)
        (call assertions :assert= 95000 (call compounder :get-principal)))

      (defun two-years-with-negative-interest-rate ()
        (setq compounder (interest-compounder 100000 -5))
        (call compounder :move-forward-years 2)
        (call assertions :assert= 90250 (call compounder :get-principal)))

      (defun three-years-with-negative-interest-rate ()
        (setq compounder (interest-compounder 100000 -5))
        (call compounder :move-forward-years 3)
        (call assertions :assert= 85737 (call compounder :get-principal)))

      (defun four-years-with-negative-interest-rate ()
        (setq compounder (interest-compounder 100000 -5))
        (call compounder :move-forward-years 4)
        (call assertions :assert= 81450 (call compounder :get-principal)))

      (defun negative-number-of-years-does-nothing ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years -4)
        (call assertions :assert= 100000 (call compounder :get-principal))
        (call compounder :move-forward-years 1)
        (call compounder :move-forward-years -4)
        (call assertions :assert= 105000 (call compounder :get-principal)))

      (defun zero-number-of-years-does-nothing ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 0)
        (call assertions :assert= 100000 (call compounder :get-principal))
        (call compounder :move-forward-years 1)
        (call compounder :move-forward-years 0)
        (call assertions :assert= 105000 (call compounder :get-principal)))

      (defun variable-interest-rate ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 2)
        (call compounder :set-interest-rate 10)
        (call compounder :move-forward-years 2)
        (call assertions :assert= 133403 (call compounder :get-principal)))

      (defun years-are-updated ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 27)
        (call assertions :assert= 27 (call compounder :get-years-passed)))

      (defun negative-number-of-years-does-not-update-years ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 27)
        (call compounder :move-forward-years -2)
        (call assertions :assert= 27 (call compounder :get-years-passed)))

      (defun zero-number-of-years-does-not-update-years ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :move-forward-years 27)
        (call compounder :move-forward-years 0)
        (call assertions :assert= 27 (call compounder :get-years-passed)))

      (defun make-contribution ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :make-contribution 2000)
        (call assertions :assert= 102000 (call compounder :get-principal)))

      (defun make-several-contributions ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :make-contribution 2000)
        (call compounder :make-contribution 2000)
        (call compounder :make-contribution 1000)
        (call assertions :assert= 105000 (call compounder :get-principal)))

      (defun make-several-contributions-and-earn-interest ()
        (setq compounder (interest-compounder 100000 5))
        (call compounder :make-contribution 2000)
        (call compounder :move-forward-years 2)
        (call compounder :make-contribution 2000)
        (call compounder :move-forward-years 2)
        (call assertions :assert= 126187 (call compounder :get-principal))))))


(setq tester (unit-tester tests))
(call tester :run)
