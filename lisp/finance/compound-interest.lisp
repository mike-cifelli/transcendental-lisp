(defun decrement (n) (- n 1))

(defun percent-of-number (n percentage)
  (if (> percentage 0)
    (/ (+ (* n percentage) 50) 100)
    (/ (- (* n percentage) 50) 100)))

(defun compound-interest (principal interest-rate years)
  (if (< years 1)
    principal
    (compound-interest
      (+ principal (percent-of-number principal interest-rate))
      interest-rate
      (decrement years))))
