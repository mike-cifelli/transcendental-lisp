(load "../unit/unit-tester.lisp")
(load "compound-interest.lisp")

(setq assertions (unit-tester-assertions))

(defun assert= (expected actual)
  (call assertions :assert= expected actual))

(setq tests
  (list

    (defun many-years-with-no-interest-rate ()
      (assert= 100000 (compound-interest 100000 0 10)))

    (defun no-years-with-positive-interest-rate ()
      (assert= 100000 (compound-interest 100000 10 0)))

    (defun one-year-with-positive-interest-rate ()
      (assert= 105000 (compound-interest 100000 5 1)))

    (defun two-years-with-positive-interest-rate ()
      (assert= 110250 (compound-interest 100000 5 2)))

    (defun three-years-with-positive-interest-rate ()
      (assert= 115763 (compound-interest 100000 5 3)))

    (defun four-years-with-positive-interest-rate ()
      (assert= 121551 (compound-interest 100000 5 4)))

    (defun one-year-with-negative-interest-rate ()
      (assert= 95000 (compound-interest 100000 -5 1)))

    (defun two-years-with-negative-interest-rate ()
      (assert= 90250 (compound-interest 100000 -5 2)))

    (defun three-years-with-negative-interest-rate ()
      (assert= 85737 (compound-interest 100000 -5 3)))

    (defun four-years-with-negative-interest-rate ()
      (assert= 81450 (compound-interest 100000 -5 4)))

    (defun negative-number-of-years ()
      (assert= 100000 (compound-interest 100000 5 -4)))))


(setq tester (unit-tester tests))
(call tester :run)
