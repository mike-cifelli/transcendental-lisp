(defun list-doubler (seed times-to-double)
  (if (< times-to-double 1) seed
   (recur (append seed seed) (- times-to-double 1))))

(defun decreasing-list (end size)
  (decreasing-list-tail (list end) (+ end 1) size))

(defun decreasing-list-tail (seed end size)
  (if (< size 1) seed
    (recur (cons (+ (car seed) 1) seed) (+ end 1) (- size 1))))

(defun increasing-list (end size)
  (increasing-list-tail (list end) (+ end 1) size))

(defun increasing-list-tail (seed end size)
  (if (< size 1) seed
    (recur (cons (- (car seed) 1) seed) (- end 1) (- size 1))))

(defun list-of (item size)
  (list-of-tail nil item size))

(defun list-of-tail (seed item size)
  (if (< size 1) seed
    (recur (cons item seed) item (- size 1))))

(defun nested-list (size)
  (nested-list-tail () size))

(defun nested-list-tail (seed size)
  (if (< size 1) seed
    (recur (cons seed nil) (- size 1))))
