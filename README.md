# Transcendental Lisp

A dialect of Lisp still in its early stages of development.


### Build Setup

```
# build the application
mvn clean verify

# run the interpreter
java -jar target/transcendental-lisp-*.jar
```
