;; This is based on the dlambda macro presented in "Let Over Lambda" by Doug Hoyte.

(defmacro dlambda (&rest methods)
  (let ((arguments (gensym)))

    `(lambda (&rest ,arguments)
      (case (first ,arguments)
        ,@(mapcar
            (lambda (method)
              `(,(first method)
                (apply (lambda ,@(rest method))
                       ,(if (equal t (first method))
                         arguments
                         `(rest ,arguments)))))
            methods)))))
