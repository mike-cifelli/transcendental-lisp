(defun mapcar (function-name the-list)
  (if the-list
    (cons
      (funcall function-name (first the-list))
      (mapcar function-name (rest the-list)))))

(defun maplist (function-name the-list)
  (cond
   ((null the-list) nil)
   (t (cons (funcall function-name the-list)
       (maplist function-name (rest the-list))))))

(defun map (function the-list)
  (reverse (map-tail function the-list nil)))

(defun map-tail (function the-list accumulator)
  (if the-list
    (recur
      function
      (rest the-list)
      (cons (call function (first the-list)) accumulator))
    accumulator))

(defun flat-map (function the-list)
  (reverse (flat-map-tail function the-list nil)))

(defun flat-map-tail (function the-list accumulator)
  (if the-list
    (recur
      function
      (rest the-list)
      (append
        (let ((result (call function (first the-list))))
          (if (list? result)
            (reverse result)
            (list result)))
        accumulator))
    accumulator))

(defun reverse (the-list)
  (reverse-tail () the-list))

(defun reverse-tail (accumulator the-list)
  (if (null the-list) accumulator
    (recur (cons (first the-list) accumulator) (rest the-list))))

(defun deep-reverse (the-list)
  (if the-list
    (append
      (deep-reverse (rest the-list))
      (list
        (if (listp (first the-list))
          (deep-reverse (first the-list))
          (first the-list))))))

(defun nth (n listA)
  (cond
   ((equal 0 n) (first listA))
   (t (nth (- n 1) (rest listA)))))

(eval
  (let ((expr (gensym)))
    `(defun global-eval (,expr) (eval ,expr))))
