package util

import java.io.File

object Path {

    fun getPathPrefix(fileName: String) = fileName.substring(0, fileName.lastIndexOf(File.separator) + 1)
}
