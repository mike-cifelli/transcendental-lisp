package util

object Characters {

    const val EOF = -1
    const val AT_SIGN = '@'
    const val BACKQUOTE = '`'
    const val BACKSLASH = '\\'
    const val COMMA = ','
    const val DASH = '-'
    const val DOUBLE_QUOTE = '\"'
    const val HASH = '#'
    const val LEFT_PARENTHESIS = '('
    const val LEFT_SQUARE_BRACKET = '['
    const val NEWLINE = '\n'
    const val PERIOD = '.'
    const val PLUS = '+'
    const val RIGHT_PARENTHESIS = ')'
    const val RIGHT_SQUARE_BRACKET = ']'
    const val SEMICOLON = ';'
    const val SINGLE_QUOTE = '\''
    const val UNICODE_ESCAPE = '\u001B'
    const val UNICODE_NULL = '\u0000'

    private val illegalIdentifierCharacters =
        setOf(AT_SIGN,
              BACKQUOTE,
              BACKSLASH,
              COMMA,
              DOUBLE_QUOTE,
              HASH,
              LEFT_PARENTHESIS,
              LEFT_SQUARE_BRACKET,
              PERIOD,
              RIGHT_PARENTHESIS,
              RIGHT_SQUARE_BRACKET,
              SEMICOLON,
              SINGLE_QUOTE)

    fun isIdentifierCharacter(c: Char) = !c.isWhitespace() && !illegalIdentifierCharacters.contains(c)
    fun isNumberPrefix(c: Char) = c == DASH || c == PLUS
}
