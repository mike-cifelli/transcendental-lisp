package stream

import error.CriticalLispException

import java.io.IOException

class LispIOException(private val exception: IOException) : CriticalLispException() {

    override val message: String
        get() = exception.message ?: ""
}