package stream

import java.io.IOException
import java.io.OutputStream

class SafeOutputStream(private val underlyingStream: OutputStream) {

    fun write(b: ByteArray) {
        try {
            underlyingStream.write(b)
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }

    fun flush() {
        try {
            underlyingStream.flush()
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }

    fun close() {
        try {
            underlyingStream.close()
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }
}