package stream

import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

import java.nio.charset.StandardCharsets.UTF_8

class SafeInputStream(underlyingStream: InputStream) {

    private val underlyingStream: InputStreamReader = InputStreamReader(underlyingStream, UTF_8)

    fun read(): Int {
        try {
            return underlyingStream.read()
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }

    fun close() {
        try {
            underlyingStream.close()
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }
}