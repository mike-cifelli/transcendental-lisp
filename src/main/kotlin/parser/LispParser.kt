package parser

import error.LispException
import scanner.LispScanner
import sexpression.SExpression
import token.Eof
import token.Token
import java.io.InputStream

/**
 * Converts a stream of bytes into internal representations of Lisp s-expressions.
 */
class LispParser(inputStream: InputStream, fileName: String) : AbstractIterator<SExpression>() {

    private val scanner: LispScanner = LispScanner(inputStream, fileName)
    private var nextToken: Token? = null
    private var delayedException: LispException? = null
    private var isNextTokenStored: Boolean = false

    override fun computeNext() = if (isEof()) done() else setNext(nextSExpression())

    fun isEof(): Boolean {
        if (!isNextTokenStored)
            storeNextToken()

        return nextToken is Eof
    }

    fun nextSExpression(): SExpression {
        throwDelayedExceptionIfNecessary()

        if (!isNextTokenStored)
            nextToken = scanner.nextToken

        isNextTokenStored = false

        return nextToken!!.parseSExpression { scanner.nextToken }
    }

    private fun storeNextToken() {
        try {
            nextToken = scanner.nextToken
            isNextTokenStored = true
        } catch (e: LispException) {
            delayedException = e
        }
    }

    private fun throwDelayedExceptionIfNecessary() {
        if (delayedException != null) {
            val exceptionToThrow = delayedException!!
            delayedException = null

            throw exceptionToThrow
        }
    }
}
