package token

import error.CriticalLineColumnException
import error.LineColumnException
import file.FilePosition

interface TokenFactory {

    fun createToken(text: String, position: FilePosition): Token
    fun createEofToken(position: FilePosition): Token

    class EmptyTokenTextException(position: FilePosition) : CriticalLineColumnException(position) {
        override val messagePrefix: String
            get() = "empty token"
    }

    class BadCharacterException(private val text: String, position: FilePosition) : LineColumnException(position) {
        override val messagePrefix: String
            get() = "illegal character >>$text<<"
    }
}
