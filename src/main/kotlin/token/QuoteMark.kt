package token

import file.FilePosition
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol

class QuoteMark(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        val nextToken = getNextToken()
        val argument = nextToken.parseSExpression(getNextToken)

        return Cons(Symbol.createQuote(), Cons(argument, Nil))
    }
}
