package token

import file.FilePosition
import sexpression.CommaExpression
import sexpression.SExpression

class Comma(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        val nextToken = getNextToken()
        val argument = nextToken.parseSExpression(getNextToken)

        return CommaExpression(argument)
    }
}
