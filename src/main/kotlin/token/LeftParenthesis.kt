package token

import file.FilePosition
import sexpression.SExpression

class LeftParenthesis(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        val nextToken = getNextToken()

        return nextToken.parseListTail(getNextToken).invoke()
    }
}
