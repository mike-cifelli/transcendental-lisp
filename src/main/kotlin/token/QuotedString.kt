package token

import file.FilePosition
import sexpression.LispString

class QuotedString(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token) = LispString(text)
}
