package token

import file.FilePosition
import sexpression.AtSignExpression
import sexpression.SExpression

class AtSign(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        val nextToken = getNextToken()
        val argument = nextToken.parseSExpression(getNextToken)

        return AtSignExpression(argument)
    }
}
