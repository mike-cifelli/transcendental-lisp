package token

import file.FilePosition
import sexpression.BackquoteExpression
import sexpression.SExpression

class Backquote(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        val nextToken = getNextToken()
        val argument = nextToken.parseSExpression(getNextToken)

        return BackquoteExpression(argument)
    }
}
