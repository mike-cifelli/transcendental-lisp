package token

import error.LineColumnException
import file.FilePosition
import sexpression.SExpression

class Eof(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        throw EofEncounteredException(position)
    }

    class EofEncounteredException(position: FilePosition) : LineColumnException(position) {
        override val messagePrefix: String
            get() = "end-of-file encountered"
    }
}
