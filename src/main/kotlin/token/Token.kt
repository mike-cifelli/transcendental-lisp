package token

import file.FilePosition
import recursion.MutualTailCall
import recursion.MutualTailCalls.recursiveCall
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

abstract class Token(val text: String, val position: FilePosition) {

    // --------------------------------------------------------------------------------------------
    // s-expression
    // ::= NUMBER | IDENTIFIER | STRING | QUOTE_MARK s-expression | LEFT_PAREN list-tail
    //
    // list-tail
    // ::= RIGHT_PAREN | s-expression list-tail
    // --------------------------------------------------------------------------------------------

    abstract fun parseSExpression(getNextToken: () -> Token): SExpression

    open fun parseListTail(getNextToken: () -> Token): MutualTailCall<Cons> {
        val firstCons = Cons(parseSExpression(getNextToken), Nil)
        val next = getNextToken()

        return recursiveCall { next.parseListTailRecursive(firstCons, firstCons, getNextToken) }
    }

    protected open fun parseListTailRecursive(start: Cons, end: Cons, getNextToken: () -> Token): MutualTailCall<Cons> {
        val newEnd = Cons(parseSExpression(getNextToken), Nil)
        val next = getNextToken()
        end.rest = newEnd

        return recursiveCall { next.parseListTailRecursive(start, newEnd, getNextToken) }
    }
}
