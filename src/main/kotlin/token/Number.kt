package token

import file.FilePosition
import sexpression.LispNumber

class Number(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token) = LispNumber(text)
}
