package token

import error.LineColumnException
import file.FilePosition
import recursion.MutualTailCall
import recursion.MutualTailCalls.terminalValue
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

class RightParenthesis(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token): SExpression {
        throw StartsWithRightParenthesisException(position)
    }

    override fun parseListTail(getNextToken: () -> Token): MutualTailCall<Cons> {
        return terminalValue(Nil)
    }

    override fun parseListTailRecursive(start: Cons, end: Cons, getNextToken: () -> Token): MutualTailCall<Cons> {
        return terminalValue(start)
    }

    class StartsWithRightParenthesisException(position: FilePosition) : LineColumnException(position) {
        override val messagePrefix: String
            get() = "expression begins with ')'"
    }
}
