package token

import file.FilePosition
import sexpression.Symbol

class Identifier(text: String, position: FilePosition) : Token(text, position) {

    override fun parseSExpression(getNextToken: () -> Token) = Symbol(text)
}
