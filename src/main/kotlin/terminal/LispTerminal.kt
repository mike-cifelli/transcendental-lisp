package terminal

import com.googlecode.lanterna.TerminalPosition
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType.ArrowDown
import com.googlecode.lanterna.input.KeyType.ArrowLeft
import com.googlecode.lanterna.input.KeyType.ArrowRight
import com.googlecode.lanterna.input.KeyType.ArrowUp
import com.googlecode.lanterna.input.KeyType.Backspace
import com.googlecode.lanterna.input.KeyType.Character
import com.googlecode.lanterna.input.KeyType.Delete
import com.googlecode.lanterna.input.KeyType.Enter
import stream.SafeInputStream
import stream.SafeOutputStream
import terminal.ControlSequenceHandler.Companion.isEscape
import util.Characters
import util.Characters.EOF
import java.io.ByteArrayInputStream
import java.util.concurrent.Executors

class LispTerminal(configuration: TerminalConfiguration) {

    private var inputLine = ""
    private var outputSegment = ""
    private var isStopped = false
    private var originColumn = 0
    private var originRow = 0
    private val inputWriter = SafeOutputStream(configuration.inputWriter)
    private val outputReader = SafeInputStream(configuration.outputReader)
    private val controlSequenceHandler = ControlSequenceHandler()
    private val history = TerminalHistory()
    private val executorService = Executors.newFixedThreadPool(2)
    private val terminal = configuration.terminal!!
    private var terminalSize = terminal.terminalSize

    init {
        setOriginToCurrentPosition()
        terminal.addResizeListener { _, _ -> resize() }
    }

    @Synchronized
    private fun setOriginToCurrentPosition() {
        val cursorPosition = terminal.cursorPosition
        originColumn = cursorPosition.column
        originRow = cursorPosition.row
    }

    @Synchronized
    private fun resize() {
        terminalSize = terminal.terminalSize
        terminal.clearScreen()
        terminal.setCursorPosition(0, 0)
        redisplayInput()
    }

    @Synchronized
    private fun redisplayInput() {
        setOriginToCurrentPosition()
        putStringToTerminal(inputLine)
        moveCursorToEndOfInput()
    }

    @Synchronized
    private fun putStringToTerminal(characters: String) {
        for (c in characters.toCharArray())
            terminal.putCharacter(c)
    }

    @Synchronized
    private fun moveCursorToEndOfInput() {
        terminal.cursorPosition = getLeadingEdge()
    }

    @Synchronized
    private fun getLeadingEdge(): TerminalPosition {
        val inputLength = inputLine.length
        val totalColumns = terminalSize.columns
        val rowDifference = inputLength / totalColumns
        val columnDifference = inputLength % totalColumns

        return TerminalPosition(originColumn + columnDifference, originRow + rowDifference)
    }

    fun start() {
        executorService.execute { this.readInput() }
        executorService.execute { this.writeOutput() }
        executorService.shutdown()
    }

    private fun readInput() {
        while (!isStopped)
            processNextKey()
    }

    private fun processNextKey() {
        val keyStroke = terminal.pollInput()

        if (keyStroke != null)
            handleKey(keyStroke)
        else
            takeNap()
    }

    @Synchronized
    private fun handleKey(keyStroke: KeyStroke) {
        doKey(keyStroke)
        terminal.flush()
    }

    @Synchronized
    private fun doKey(keyStroke: KeyStroke) {
        if (keyStroke.isCtrlDown)
            doControlKey(keyStroke)
        else
            doNormalKey(keyStroke)
    }

    @Synchronized
    private fun doControlKey(keyStroke: KeyStroke) {
        if (keyStroke.keyType == Character)
            doControlCharacter(keyStroke)
    }

    @Synchronized
    private fun doControlCharacter(keyStroke: KeyStroke) {
        if (keyStroke.character == 'd')
            doControlD()
    }

    @Synchronized
    private fun doControlD() {
        doEnter()
        stop()
    }

    @Suppress("NON_EXHAUSTIVE_WHEN")
    @Synchronized
    private fun doNormalKey(keyStroke: KeyStroke) {
        when (keyStroke.keyType) {
            Enter      -> doEnter()
            ArrowUp    -> doUpArrow()
            ArrowDown  -> doDownArrow()
            ArrowLeft  -> doLeftArrow()
            ArrowRight -> doRightArrow()
            Backspace  -> doBackspace()
            Delete     -> doDelete()
            Character  -> doCharacter(keyStroke.character!!)
        }
    }

    @Synchronized
    private fun doEnter() {
        moveCursorToEndOfInput()
        history.addLine(inputLine)
        terminal.putCharacter('\n')
        writeInputLine()
        setOriginToCurrentPosition()
    }

    @Synchronized
    private fun writeInputLine() {
        inputLine += "\n"
        inputWriter.write(inputLine.toByteArray())
        inputWriter.flush()
        inputLine = ""
    }

    @Synchronized
    private fun doUpArrow() {
        if (!history.isBeginning())
            replaceInputWithPreviousLine()
    }

    @Synchronized
    private fun replaceInputWithPreviousLine() {
        history.updateCurrentLine(inputLine)
        clearInput()
        displayPreviousInputLine()
    }

    @Synchronized
    private fun clearInput() {
        terminal.setCursorPosition(originColumn, originRow)
        putStringToTerminal(inputLine.replace(".".toRegex(), " "))
        terminal.setCursorPosition(originColumn, originRow)
    }

    @Synchronized
    private fun displayPreviousInputLine() {
        inputLine = history.getPreviousLine()
        val inputCharacters = inputLine.toCharArray()

        for (i in inputCharacters.indices)
            displayCharacter(inputCharacters[i], i)
    }

    @Synchronized
    private fun displayCharacter(character: Char, offsetFromOrigin: Int) {
        if (isLastColumn(offsetFromOrigin)) {
            val cursorPosition = terminal.cursorPosition
            terminal.putCharacter(character)
            moveCursorToNextRow(cursorPosition)
        } else
            terminal.putCharacter(character)
    }

    @Synchronized
    private fun isLastColumn(offsetFromOrigin: Int): Boolean {
        val totalColumns = terminalSize.columns

        return (originColumn + offsetFromOrigin) % totalColumns == totalColumns - 1
    }

    @Synchronized
    private fun doDownArrow() {
        if (!history.isEnd())
            replaceInputWithNextLine()
    }

    @Synchronized
    private fun replaceInputWithNextLine() {
        history.updateCurrentLine(inputLine)
        clearInput()
        displayNextInputLine()
    }

    @Synchronized
    private fun displayNextInputLine() {
        inputLine = history.getNextLine()
        putStringToTerminal(inputLine)
    }

    @Synchronized
    private fun doLeftArrow() {
        val cursorPosition = terminal.cursorPosition

        if (isPossibleToMoveLeft(cursorPosition))
            moveCursorLeft(cursorPosition)
    }

    @Synchronized
    private fun isPossibleToMoveLeft(cursorPosition: TerminalPosition): Boolean {
        return getDistanceFromOrigin(cursorPosition) > 0
    }

    @Synchronized
    private fun getDistanceFromOrigin(cursorPosition: TerminalPosition): Int {
        val columnDifference = cursorPosition.column - originColumn
        val rowDifference = cursorPosition.row - originRow
        val totalColumns = terminalSize.columns

        return columnDifference + totalColumns * rowDifference
    }

    @Synchronized
    private fun moveCursorLeft(cursorPosition: TerminalPosition) {
        var newPosition = cursorPosition.withRelativeColumn(-1)

        if (isAtStartOfRow(cursorPosition))
            newPosition = cursorPosition.withColumn(terminalSize.columns).withRelativeRow(-1)

        terminal.cursorPosition = newPosition
    }

    @Synchronized
    private fun isAtStartOfRow(cursorPosition: TerminalPosition): Boolean {
        return cursorPosition.column == 0
    }

    @Synchronized
    private fun doRightArrow() {
        val cursorPosition = terminal.cursorPosition

        if (isPossibleToMoveRight(cursorPosition))
            moveCursorRight(cursorPosition)
    }

    @Synchronized
    private fun isPossibleToMoveRight(cursorPosition: TerminalPosition): Boolean {
        return getDistanceFromOrigin(cursorPosition) < inputLine.length
    }

    @Synchronized
    private fun moveCursorRight(cursorPosition: TerminalPosition) {
        if (isEndOfRow(cursorPosition))
            moveCursorToNextRow(cursorPosition)
        else
            terminal.cursorPosition = cursorPosition.withRelativeColumn(1)
    }

    @Synchronized
    private fun isEndOfRow(cursorPosition: TerminalPosition): Boolean {
        return cursorPosition.column >= terminalSize.columns - 1
    }

    @Synchronized
    private fun moveCursorToNextRow(cursorPosition: TerminalPosition) {
        if (isEndOfBuffer(cursorPosition))
            createNewRowForCursor(cursorPosition)
        else
            terminal.cursorPosition = cursorPosition.withColumn(0).withRelativeRow(1)
    }

    @Synchronized
    private fun isEndOfBuffer(cursorPosition: TerminalPosition): Boolean {
        return cursorPosition.row == terminalSize.rows - 1
    }

    @Synchronized
    private fun createNewRowForCursor(cursorPosition: TerminalPosition) {
        terminal.cursorPosition = cursorPosition
        terminal.putCharacter('\n')
        terminal.cursorPosition = cursorPosition.withColumn(0)
        --originRow
    }

    @Synchronized
    private fun doBackspace() {
        val cursorPosition = terminal.cursorPosition

        if (isPossibleToMoveLeft(cursorPosition))
            deletePreviousCharacter(cursorPosition)
    }

    @Synchronized
    private fun deletePreviousCharacter(cursorPosition: TerminalPosition) {
        val distanceFromOrigin = getDistanceFromOrigin(cursorPosition)
        val remaining = inputLine.substring(distanceFromOrigin, inputLine.length)
        inputLine = inputLine.substring(0, distanceFromOrigin - 1) + remaining
        moveCursorLeft(cursorPosition)
        putStringToTerminal("$remaining ")
        moveCursorLeft(cursorPosition)
    }

    @Synchronized
    private fun doDelete() {
        val cursorPosition = terminal.cursorPosition

        if (isPossibleToMoveRight(cursorPosition))
            deleteCharacterAtPosition(cursorPosition)
    }

    @Synchronized
    private fun deleteCharacterAtPosition(cursorPosition: TerminalPosition) {
        val distanceFromOrigin = getDistanceFromOrigin(cursorPosition)
        val remaining = inputLine.substring(distanceFromOrigin + 1, inputLine.length)
        inputLine = inputLine.substring(0, distanceFromOrigin) + remaining
        putStringToTerminal("$remaining ")
        terminal.cursorPosition = cursorPosition
    }

    @Synchronized
    private fun doCharacter(character: Char) {
        val cursorPosition = terminal.cursorPosition

        if (!isBufferFilled())
            if (isPossibleToMoveRight(cursorPosition))
                insertCharacter(character, cursorPosition)
            else
                appendCharacter(character, cursorPosition)
    }

    @Synchronized
    private fun isBufferFilled() = getLeadingEdge().let { leadingEdge ->
        leadingEdge.row == terminalSize.rows - 1
                && leadingEdge.column >= terminalSize.columns - 1
                && originRow <= 0
    }

    @Synchronized
    private fun insertCharacter(character: Char, cursorPosition: TerminalPosition) {
        val shiftedPosition = shiftPositionIfNewRowAdded(cursorPosition)
        terminal.cursorPosition = shiftedPosition

        val distanceFromOrigin = getDistanceFromOrigin(shiftedPosition)
        val remaining = character + inputLine.substring(distanceFromOrigin, inputLine.length)
        inputLine = inputLine.substring(0, distanceFromOrigin) + remaining
        putStringToTerminal(remaining)
        moveCursorRight(shiftedPosition)
    }

    @Synchronized
    private fun shiftPositionIfNewRowAdded(cursorPosition: TerminalPosition): TerminalPosition {
        val oldOriginRow = originRow
        moveCursorRight(getLeadingEdge())

        return if (isNewRowAdded(oldOriginRow)) adjustCursorPosition(cursorPosition) else cursorPosition
    }

    @Synchronized
    private fun isNewRowAdded(oldOriginRow: Int): Boolean {
        return originRow != oldOriginRow
    }

    @Synchronized
    private fun adjustCursorPosition(cursorPosition: TerminalPosition): TerminalPosition {
        terminal.cursorPosition = TerminalPosition(originColumn, originRow)
        putStringToTerminal(inputLine)

        return cursorPosition.withRelativeRow(-1)
    }

    @Synchronized
    private fun appendCharacter(character: Char, cursorPosition: TerminalPosition) {
        terminal.putCharacter(character)
        inputLine += character
        moveCursorRight(cursorPosition)
    }

    private fun takeNap() {
        Thread.sleep(1)
    }

    private fun writeOutput() {
        var c = outputReader.read()

        while (c != EOF) {
            processOutput(c.toChar())
            c = outputReader.read()
        }

        terminal.flush()
        terminal.close()
    }

    @Synchronized
    private fun processOutput(c: Char) {
        if (c == END_OF_SEGMENT)
            writeSegment()
        else
            outputSegment += c
    }

    @Synchronized
    private fun writeSegment() {
        printSegmentCharacters()
        outputSegment = ""
        redisplayInput()
        terminal.flush()
    }

    @Synchronized
    private fun printSegmentCharacters() {
        moveCursorToEndOfInput()
        putOutputToTerminal()
        moveCursorToNextRowIfNecessary()
    }

    @Synchronized
    private fun putOutputToTerminal() {
        val input = convertOutputToStream()
        var c = input.read()

        while (c != EOF) {
            if (isEscape(c.toChar()))
                applyControlSequence(input)
            else
                terminal.putCharacter(c.toChar())

            c = input.read()
        }
    }

    @Synchronized
    private fun convertOutputToStream(): SafeInputStream {
        return SafeInputStream(ByteArrayInputStream(outputSegment.toByteArray()))
    }

    @Synchronized
    private fun applyControlSequence(input: SafeInputStream) {
        val controlSequence = controlSequenceHandler.parse(input)
        controlSequence.applyTo(terminal)
    }

    @Synchronized
    private fun moveCursorToNextRowIfNecessary() {
        val cursorPosition = terminal.cursorPosition

        if (isEndOfRow(cursorPosition))
            moveCursorToNextRow(cursorPosition)
    }

    fun stop() {
        isStopped = true
        inputWriter.close()
    }

    companion object {
        const val END_OF_SEGMENT = Characters.UNICODE_NULL
    }
}
