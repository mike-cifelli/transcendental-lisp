package terminal

import stream.SafeInputStream
import util.Characters.EOF
import util.Characters.LEFT_SQUARE_BRACKET
import util.Characters.UNICODE_ESCAPE
import java.lang.Character.isDigit

class ControlSequenceHandler {

    private val controlSequenceLookup: ControlSequenceLookup = ControlSequenceLookup()
    private var currentCharacter: Int = 0
    private lateinit var input: SafeInputStream
    private lateinit var code: String

    private fun isExpectedFirstCharacter() = isCharacter() && isLeftBracket()
    private fun isCharacter() = currentCharacter != EOF
    private fun isLeftBracket() = currentCharacter.toChar() == LEFT_SQUARE_BRACKET
    private fun isPartOfCode() = isCharacter() && isDigit(currentCharacter.toChar())

    fun parse(inputStream: SafeInputStream): ControlSequence {
        input = inputStream
        code = ""

        readCharacter()

        if (isExpectedFirstCharacter())
            readCode()

        return controlSequenceLookup[currentCharacter.toChar(), code]
    }

    private fun readCharacter() {
        currentCharacter = input.read()
    }

    private fun readCode() {
        readCharacter()

        while (isPartOfCode()) {
            code += currentCharacter.toChar()
            readCharacter()
        }
    }

    companion object {

        fun isEscape(c: Char): Boolean {
            return c == UNICODE_ESCAPE
        }
    }
}
