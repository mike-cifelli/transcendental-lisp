package terminal

class TerminalHistory {

    private val history = mutableListOf("")
    private var lineIndex = 0

    fun getPreviousLine() = if (isBeginning()) history[lineIndex] else history[--lineIndex]
    fun getNextLine() = if (isEnd()) history[lineIndex] else history[++lineIndex]
    fun isBeginning() = lineIndex == 0
    fun isEnd() = lineIndex == lastIndex()

    private fun lastIndex() = history.size - 1

    fun addLine(line: String) {
        history[lastIndex()] = line
        history.add("")
        lineIndex = lastIndex()
    }

    fun updateCurrentLine(line: String) {
        history[lineIndex] = line
    }
}
