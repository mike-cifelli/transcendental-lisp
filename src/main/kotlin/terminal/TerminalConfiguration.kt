package terminal

import com.googlecode.lanterna.terminal.IOSafeTerminal
import stream.LispIOException

import java.io.IOException
import java.io.PipedInputStream
import java.io.PipedOutputStream

class TerminalConfiguration {

    lateinit var inputWriter: PipedOutputStream
    lateinit var inputReader: PipedInputStream
    lateinit var outputWriter: PipedOutputStream
    lateinit var outputReader: PipedInputStream
    var terminal: IOSafeTerminal? = null

    fun setInputPair(inputWriter: PipedOutputStream, inputReader: PipedInputStream) {
        this.inputWriter = inputWriter
        this.inputReader = inputReader
        connectInputPair()
    }

    private fun connectInputPair() {
        try {
            inputWriter.connect(inputReader)
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }

    fun setOutputPair(outputWriter: PipedOutputStream, outputReader: PipedInputStream) {
        this.outputWriter = outputWriter
        this.outputReader = outputReader
        connectOutputPair()
    }

    private fun connectOutputPair() {
        try {
            outputWriter.connect(outputReader)
        } catch (e: IOException) {
            throw LispIOException(e)
        }
    }
}