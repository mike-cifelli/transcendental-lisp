package terminal

import terminal.ControlSequence.NullControlSequence
import terminal.SelectGraphicRendition.Companion.SGR_COMMAND

class ControlSequenceLookup {

    private val commands = mapOf(SGR_COMMAND to SelectGraphicRendition.values().associateBy({ it.code }, { it }))

    operator fun get(command: Char, code: String): ControlSequence {
        val codes = commands.getOrDefault(command, emptyMap())

        return codes.getOrDefault(code, NullControlSequence())
    }
}