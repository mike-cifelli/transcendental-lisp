package terminal

import com.googlecode.lanterna.terminal.IOSafeTerminal

interface ControlSequence {

    val code
        get() = ""

    fun applyTo(terminal: IOSafeTerminal) {}

    class NullControlSequence : ControlSequence
}