package terminal

import com.googlecode.lanterna.TextColor.ANSI
import com.googlecode.lanterna.terminal.IOSafeTerminal

enum class SelectGraphicRendition : ControlSequence {

    RESET {
        override val code = "0"

        override fun applyTo(terminal: IOSafeTerminal) {
            terminal.resetColorAndSGR()
        }
    },

    RED {
        override val code = "31"

        override fun applyTo(terminal: IOSafeTerminal) {
            terminal.setForegroundColor(ANSI.RED)
        }
    },

    GREEN {
        override val code = "32"

        override fun applyTo(terminal: IOSafeTerminal) {
            terminal.setForegroundColor(ANSI.GREEN)
        }
    },

    YELLOW {
        override val code = "33"

        override fun applyTo(terminal: IOSafeTerminal) {
            terminal.setForegroundColor(ANSI.YELLOW)
        }
    },

    PURPLE {
        override val code = "35"

        override fun applyTo(terminal: IOSafeTerminal) {
            terminal.setForegroundColor(ANSI.MAGENTA)
        }
    };

    companion object {

        val SGR_COMMAND = 'm'
    }
}