package sexpression

class AtSignExpression(val expression: SExpression) : SExpression() {

    override val isAtSign = true

    override fun toString() = "@$expression"
}
