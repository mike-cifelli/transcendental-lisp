package sexpression

import error.LispException
import java.math.BigInteger

@DisplayName("number")
class LispNumber : Atom {

    override val isNumber = true

    val value: BigInteger

    constructor(text: String) : super(text.replaceFirst("^0+(?!$)".toRegex(), "")) {
        try {
            this.value = BigInteger(text)
        } catch (e: NumberFormatException) {
            throw InvalidNumberException(text)
        }
    }

    constructor(value: BigInteger) : super(value.toString()) {
        this.value = value
    }

    companion object {
        val ZERO = LispNumber(BigInteger.ZERO)
        val ONE = LispNumber(BigInteger.ONE)
    }

    class InvalidNumberException(text: String) : LispException() {

        override val message = "$text is not a valid integer"
    }
}
