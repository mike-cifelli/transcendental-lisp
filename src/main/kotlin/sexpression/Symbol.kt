package sexpression

import java.util.Locale

@DisplayName("symbol")
open class Symbol(text: String) : Atom(text.toUpperCase(Locale.ROOT)) {

    override val isSymbol = true

    companion object {
        val T = Symbol("T")

        fun createQuote() = Symbol("QUOTE")
    }
}
