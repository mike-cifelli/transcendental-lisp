package sexpression

import function.UserDefinedFunction

@DisplayName("lambda-expression")
class LambdaExpression(val lambdaExpression: Cons, val function: UserDefinedFunction) : SExpression() {

    override val isFunction = true

    override fun toString() = lambdaExpression.toString()
}
