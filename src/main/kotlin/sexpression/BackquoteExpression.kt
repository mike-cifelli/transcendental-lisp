package sexpression

class BackquoteExpression(val expression: SExpression) : SExpression() {

    override val isBackquote = true

    override fun toString() = "`$expression"
}
