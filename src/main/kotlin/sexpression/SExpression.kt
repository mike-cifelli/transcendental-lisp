package sexpression

@DisplayName("s-expression")
abstract class SExpression {

    open val isNull
        get() = false

    open val isAtom: Boolean
        get() = false

    open val isCons: Boolean
        get() = false

    open val isList: Boolean
        get() = isCons || isNull

    open val isNumber: Boolean
        get() = false

    open val isSymbol: Boolean
        get() = false

    open val isFunction: Boolean
        get() = false

    open val isString: Boolean
        get() = false

    open val isBackquote: Boolean
        get() = false

    open val isComma: Boolean
        get() = false

    open val isAtSign: Boolean
        get() = false
}
