package sexpression

@DisplayName("atom")
abstract class Atom(private val text: String) : SExpression() {

    override val isAtom = true

    override fun toString() = text
}
