package sexpression

class CommaExpression(val expression: SExpression) : SExpression() {

    override val isComma = true

    override fun toString() = ",$expression"
}
