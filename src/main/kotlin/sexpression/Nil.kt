package sexpression

import sexpression.Symbol.Companion.T

@DisplayName("nil")
object Nil : Cons(T, T) {

    override var first: SExpression = this
        set(_) {}

    override var rest: SExpression = this
        set(_) {}

    override val isNull = true
    override val isAtom = true
    override val isCons = false
    override val isSymbol = true

    override fun toString() = "NIL"
}
