package sexpression

import kotlin.annotation.AnnotationTarget.CLASS

@Target(CLASS)
annotation class DisplayName(val value: String)
