package sexpression

@DisplayName("string")
class LispString(text: String) : Atom(text) {

    override val isString = true
}
