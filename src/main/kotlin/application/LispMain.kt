package application

import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.IOSafeTerminalAdapter.createRuntimeExceptionConvertingAdapter
import interpreter.LispInterpreter
import interpreter.LispInterpreterBuilder
import terminal.LispTerminal
import terminal.LispTerminal.Companion.END_OF_SEGMENT
import terminal.TerminalConfiguration
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.io.PrintStream
import java.text.MessageFormat.format

class LispMain constructor(var configuration: TerminalConfiguration = TerminalConfiguration()) {

    init {
        if (configuration.terminal == null) {
            configuration.setInputPair(PipedOutputStream(), PipedInputStream())
            configuration.setOutputPair(PipedOutputStream(), PipedInputStream())
            configuration.terminal = createIOSafeTerminal()
        }
    }

    private var inputReader: PipedInputStream = configuration.inputReader
    private var output: PrintStream = PrintStream(configuration.outputWriter)
    private var lispTerminal: LispTerminal = LispTerminal(configuration)
    private val version = javaClass.`package`.implementationVersion

    fun runInteractive() {
        printGreeting()
        lispTerminal.start()
        buildInteractiveInterpreter().interpret()
        shutdownTerminal()
    }

    fun runWithFile(fileName: String) {
        buildFileInterpreter(fileName).interpret()
    }

    private fun createIOSafeTerminal() = createRuntimeExceptionConvertingAdapter(createDefaultTerminal())
    private fun createDefaultTerminal() = DefaultTerminalFactory().createTerminal()
    private fun makeColorDecorator(color: String): (String) -> String = { color + it + ANSI_RESET }

    private fun printGreeting() {
        output.println(format(GREETING, (version ?: "null")))
        output.println()
    }

    private fun buildInteractiveInterpreter(): LispInterpreter {
        LispInterpreterBuilder.setInput(inputReader, "terminal")
        LispInterpreterBuilder.setOutput(output)
        LispInterpreterBuilder.setErrorOutput(output)
        LispInterpreterBuilder.setTerminationFunction { this.shutdownTerminal() }
        LispInterpreterBuilder.setErrorTerminationFunction { this.shutdownTerminal() }
        LispInterpreterBuilder.setLanguageFileNames(*LANGUAGE_FILE_NAMES)
        LispInterpreterBuilder.setPromptDecorator { it + END_OF_SEGMENT }
        LispInterpreterBuilder.setValueOutputDecorator(makeColorDecorator(ANSI_GREEN))
        LispInterpreterBuilder.setWarningOutputDecorator(makeColorDecorator(ANSI_YELLOW))
        LispInterpreterBuilder.setErrorOutputDecorator(makeColorDecorator(ANSI_RED))
        LispInterpreterBuilder.setCriticalOutputDecorator(makeColorDecorator(ANSI_PURPLE))

        return LispInterpreterBuilder.build()
    }

    private fun buildFileInterpreter(fileName: String): LispInterpreter {
        LispInterpreterBuilder.useFile(fileName)
        LispInterpreterBuilder.setOutput(System.out)
        LispInterpreterBuilder.setErrorOutput(System.err)
        LispInterpreterBuilder.setTerminationFunction { System.exit(0) }
        LispInterpreterBuilder.setErrorTerminationFunction { System.exit(1) }
        LispInterpreterBuilder.setLanguageFileNames(*LANGUAGE_FILE_NAMES)
        LispInterpreterBuilder.setValueOutputDecorator(makeColorDecorator(ANSI_GREEN))
        LispInterpreterBuilder.setWarningOutputDecorator(makeColorDecorator(ANSI_YELLOW))
        LispInterpreterBuilder.setErrorOutputDecorator(makeColorDecorator(ANSI_RED))
        LispInterpreterBuilder.setCriticalOutputDecorator(makeColorDecorator(ANSI_PURPLE))

        return LispInterpreterBuilder.build()
    }

    private fun shutdownTerminal() {
        output.print(END_OF_SEGMENT)
        lispTerminal.stop()
        output.close()
    }

    companion object {

        const val GREETING = "Transcendental Lisp - Version {0}"

        const val ANSI_RESET = "\u001B[0m"
        const val ANSI_RED = "\u001B[31m"
        const val ANSI_GREEN = "\u001B[32m"
        const val ANSI_YELLOW = "\u001B[33m"
        const val ANSI_PURPLE = "\u001B[35m"

        val LANGUAGE_FILE_NAMES = arrayOf("functions.lisp", "dlambda.lisp")

        @JvmStatic
        fun main(arguments: Array<String>) {
            with(LispMain()) {
                if (arguments.isEmpty())
                    runInteractive()
                else
                    runWithFile(arguments[0])
            }
        }
    }
}
