package error

import environment.RuntimeEnvironment

enum class Severity {

    WARNING {
        override fun decorate(output: String, environment: RuntimeEnvironment): String =
            RuntimeEnvironment.decorateWarningOutput(output)

        override fun toDisplayString() = "warning"
    },

    ERROR {
        override fun decorate(output: String, environment: RuntimeEnvironment): String =
            RuntimeEnvironment.decorateErrorOutput(output)

        override fun toDisplayString() = "error"
    },

    CRITICAL {
        override fun decorate(output: String, environment: RuntimeEnvironment): String =
            RuntimeEnvironment.decorateCriticalOutput(output)

        override fun toDisplayString() = "critical"
    };

    abstract fun decorate(output: String, environment: RuntimeEnvironment): String

    abstract fun toDisplayString(): String
}