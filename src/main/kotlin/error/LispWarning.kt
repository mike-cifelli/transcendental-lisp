package error

import error.Severity.WARNING

abstract class LispWarning : LispException() {

    override val severity = WARNING
}
