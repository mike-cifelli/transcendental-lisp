package error

import error.Severity.ERROR

abstract class LispException : RuntimeException() {

    open val severity = ERROR
}
