package error

import environment.RuntimeEnvironment
import error.Severity.CRITICAL
import error.Severity.WARNING

/**
 * Prints error messages and potentially terminates the application.
 */
class ErrorManager {

    fun handle(lispException: LispException) {
        printMessage(lispException)

        if (isCritical(lispException))
            RuntimeEnvironment.terminateExceptionally()
    }

    private fun printMessage(lispException: LispException) {
        val output = selectOutputStream(lispException.severity)
        output.println(formatMessage(lispException))
    }

    private fun selectOutputStream(severity: Severity) =
        if (severity === WARNING) RuntimeEnvironment.output!! else RuntimeEnvironment.errorOutput!!

    private fun formatMessage(lispException: LispException): String {
        val severity = lispException.severity
        val prefix = severity.toDisplayString()
        val message = "[$prefix] ${lispException.message}"

        return severity.decorate(message, RuntimeEnvironment)
    }

    private fun isCritical(lispException: LispException) = lispException.severity == CRITICAL
}
