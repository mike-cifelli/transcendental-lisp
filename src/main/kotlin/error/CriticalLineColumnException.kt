package error

import error.Severity.CRITICAL
import file.FilePosition

abstract class CriticalLineColumnException(position: FilePosition) : LineColumnException(position) {

    override val severity = CRITICAL
}