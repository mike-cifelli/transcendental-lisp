package error

import file.FilePosition

abstract class LineColumnException(private val position: FilePosition) : LispException() {

    abstract val messagePrefix: String

    override val message: String
        get() = "$messagePrefix - line ${position.lineNumber}, column ${position.columnNumber}"
}