package error

import error.Severity.CRITICAL

abstract class CriticalLispException : LispException() {

    override val severity = CRITICAL
}
