package interpreter

import environment.RuntimeEnvironment.decoratePrompt
import environment.RuntimeEnvironment.output
import sexpression.SExpression

class InteractiveLispInterpreter : LispInterpreter() {

    override fun prompt() = output?.run {
        print(decoratePrompt(PROMPT))
        flush()
    }

    override fun printSExpression(sExpression: SExpression) = output?.run {
        println()
        super.printSExpression(sExpression)
    }

    override fun applyFinishingTouches() = output?.run {
        println(decoratePrompt(""))
        flush()
    }

    companion object {
        internal const val PROMPT = "~ "
    }
}
