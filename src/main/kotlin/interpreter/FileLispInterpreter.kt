package interpreter

import sexpression.SExpression

class FileLispInterpreter : LispInterpreter() {

    private var lastSExpression: SExpression? = null

    override fun printSExpression(sExpression: SExpression) {
        lastSExpression = sExpression
    }

    override fun applyFinishingTouches() {
        if (lastSExpression != null)
            super.printSExpression(lastSExpression!!)

        super.applyFinishingTouches()
    }
}
