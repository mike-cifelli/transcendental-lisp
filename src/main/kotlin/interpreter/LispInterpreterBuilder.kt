package interpreter

import environment.RuntimeEnvironment
import error.CriticalLispException
import error.ErrorManager
import interpreter.LispInterpreter.LanguageFile
import util.Path
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.PrintStream

object LispInterpreterBuilder {

    private var inputName = ""
    private var isInteractive = true
    private var isFileBased = false
    private var languageFiles = mutableListOf<LanguageFile>()
    private var inputStream: InputStream? = null
    private var outputStream: PrintStream? = null
    private var errorOutputStream: PrintStream? = null
    private var terminationFunction: () -> Unit = {}
    private var errorTerminationFunction: () -> Unit = {}
    private var promptDecorator: (String) -> String = { it }
    private var valueOutputDecorator: (String) -> String = { it }
    private var warningOutputDecorator: (String) -> String = { it }
    private var errorOutputDecorator: (String) -> String = { it }
    private var criticalOutputDecorator: (String) -> String = { it }

    init {
        reset()
    }

    fun reset() {
        inputName = ""
        isInteractive = true
        isFileBased = false
        languageFiles = mutableListOf()
        inputStream = null
        outputStream = null
        errorOutputStream = null
        terminationFunction = {}
        errorTerminationFunction = {}
        promptDecorator = { it }
        valueOutputDecorator = { it }
        warningOutputDecorator = { it }
        errorOutputDecorator = { it }
        criticalOutputDecorator = { it }
    }

    fun setNotInteractive() {
        this.isInteractive = false
    }

    fun setInput(inputStream: InputStream, inputName: String) {
        this.inputStream = inputStream
        this.inputName = inputName
    }

    fun setOutput(outputStream: PrintStream) {
        this.outputStream = outputStream
    }

    fun setErrorOutput(errorOutputStream: PrintStream) {
        this.errorOutputStream = errorOutputStream
    }

    fun useFile(fileName: String) {
        this.isFileBased = true
        this.inputName = fileName
        this.setNotInteractive()
    }

    fun setLanguageFileNames(vararg languageFiles: String) {
        val classLoader = javaClass.classLoader
        this.languageFiles = mutableListOf()

        for (fileName in languageFiles)
            this.languageFiles.add(LanguageFile(classLoader.getResourceAsStream(fileName), fileName))
    }

    fun setTerminationFunction(terminationFunction: () -> Unit) {
        this.terminationFunction = terminationFunction
    }

    fun setErrorTerminationFunction(errorTerminationFunction: () -> Unit) {
        this.errorTerminationFunction = errorTerminationFunction
    }

    fun setPromptDecorator(decorator: (String) -> String) {
        this.promptDecorator = decorator
    }

    fun setValueOutputDecorator(decorator: (String) -> String) {
        this.valueOutputDecorator = decorator
    }

    fun setWarningOutputDecorator(decorator: (String) -> String) {
        this.warningOutputDecorator = decorator
    }

    fun setErrorOutputDecorator(decorator: (String) -> String) {
        this.errorOutputDecorator = decorator
    }

    fun setCriticalOutputDecorator(decorator: (String) -> String) {
        this.criticalOutputDecorator = decorator
    }

    fun build(): LispInterpreter {
        configureRuntimeEnvironment()
        val lispInterpreter = createInterpreter()
        lispInterpreter.interpretLanguageFiles(languageFiles)

        return lispInterpreter
    }

    private fun configureRuntimeEnvironment() {
        val errorManager = ErrorManager()

        RuntimeEnvironment.output = outputStream
        RuntimeEnvironment.errorOutput = errorOutputStream
        RuntimeEnvironment.errorManager = errorManager
        RuntimeEnvironment.terminationFunction = terminationFunction
        RuntimeEnvironment.errorTerminationFunction = errorTerminationFunction
        RuntimeEnvironment.promptDecorator = promptDecorator
        RuntimeEnvironment.valueOutputDecorator = valueOutputDecorator
        RuntimeEnvironment.warningOutputDecorator = warningOutputDecorator
        RuntimeEnvironment.errorOutputDecorator = errorOutputDecorator
        RuntimeEnvironment.criticalOutputDecorator = criticalOutputDecorator
        configurePath()
        configureInput(errorManager)
    }

    private fun configurePath() {
        if (isFileBased)
            RuntimeEnvironment.path = Path.getPathPrefix(inputName)
        else
            RuntimeEnvironment.path = ""
    }

    private fun configureInput(errorManager: ErrorManager) {
        RuntimeEnvironment.inputName = inputName

        try {
            RuntimeEnvironment.input = getInputStream()
        } catch (e: FileNotFoundException) {
            errorManager.handle(LispFileNotFoundException(e))
        }
    }

    private fun getInputStream() = if (isFileBased) FileInputStream(inputName) else inputStream

    private fun createInterpreter() = when {
        isFileBased   -> FileLispInterpreter()
        isInteractive -> InteractiveLispInterpreter()
        else          -> LispInterpreter()
    }

    class LispFileNotFoundException(val e: FileNotFoundException) : CriticalLispException() {

        override val message: String
            get() = e.message ?: ""
    }
}
