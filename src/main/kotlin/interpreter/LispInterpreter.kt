package interpreter

import environment.RuntimeEnvironment.decorateValueOutput
import environment.RuntimeEnvironment.errorManager
import environment.RuntimeEnvironment.input
import environment.RuntimeEnvironment.inputName
import environment.RuntimeEnvironment.output
import error.LispException
import function.builtin.Eval.Companion.eval
import parser.LispParser
import sexpression.SExpression
import java.io.InputStream

open class LispInterpreter {

    private lateinit var parser: LispParser

    fun interpretLanguageFiles(languageFiles: List<LanguageFile>) {
        for (file in languageFiles)
            LispParser(file.inputStream, file.name).forEach { eval(it) }
    }

    fun interpret() {
        parser = LispParser(input!!, inputName!!)
        prompt()

        while (!parser.isEof()) {
            evaluateAndPrintNextSExpression()
            prompt()
        }

        applyFinishingTouches()
    }

    protected open fun prompt(): Unit? = null

    protected open fun evaluateAndPrintNextSExpression() = try {
        printSExpression(eval(parser.nextSExpression()))
    } catch (e: LispException) {
        errorManager?.run { handle(e) }
    }

    protected open fun printSExpression(sExpression: SExpression) = output?.run {
        println(decorateValueOutput(sExpression.toString()))
        flush()
    }

    protected open fun applyFinishingTouches() = output?.run {
        println()
        flush()
    }

    class LanguageFile(val inputStream: InputStream, name: String) {
        val name: String = "lang:$name"
    }
}
