package file

class FilePositionTracker(private val fileName: String) {

    private var lineNumber = 1
    private var columnNumber = 0

    fun currentPosition() = FilePosition(fileName = fileName, lineNumber = lineNumber, columnNumber = columnNumber)

    fun incrementColumn() {
        columnNumber++
    }

    fun incrementLine() {
        lineNumber++
        columnNumber = 0
    }
}