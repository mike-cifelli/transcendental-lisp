package file

data class FilePosition(val fileName: String, val lineNumber: Int, val columnNumber: Int)