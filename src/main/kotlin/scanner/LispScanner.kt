package scanner

import error.LineColumnException
import file.FilePosition
import file.FilePositionTracker
import token.Token
import token.TokenFactoryImpl
import util.Characters.BACKSLASH
import util.Characters.DOUBLE_QUOTE
import util.Characters.NEWLINE
import util.Characters.isIdentifierCharacter
import util.Characters.isNumberPrefix
import java.io.InputStream

/**
 * Converts a stream of bytes into a stream of Lisp tokens.
 */
class LispScanner(inputStream: InputStream, fileName: String) {

    private val inputStream = LispCommentRemovingInputStream(inputStream)
    private val positionTracker = FilePositionTracker(fileName)
    private val tokenFactory = TokenFactoryImpl()

    val nextToken: Token
        get() {
            for (c in inputStream) {
                val currentCharacter = c.toChar()
                positionTracker.incrementColumn()

                if (!currentCharacter.isWhitespace())
                    return createTokenFromCharacter(currentCharacter)
                else if (currentCharacter == NEWLINE)
                    positionTracker.incrementLine()
            }

            return tokenFactory.createEofToken(positionTracker.currentPosition())
        }

    private fun createTokenFromCharacter(c: Char) = positionTracker.currentPosition().let { position ->
        tokenFactory.createToken(retrieveTokenText(c), position)
    }

    private fun retrieveTokenText(firstCharacter: Char): String {
        var tokenText = "" + firstCharacter

        when {
            firstCharacter == DOUBLE_QUOTE        -> tokenText = retrieveStringTokenText(firstCharacter)
            isNumberPrefix(firstCharacter)        -> tokenText = retrieveNumberOrIdentifierTokenText(firstCharacter)
            firstCharacter.isDigit()              -> tokenText = retrieveNumberTokenText(firstCharacter)
            isIdentifierCharacter(firstCharacter) -> tokenText = retrieveIdentifierTokenText(firstCharacter)
        }

        return tokenText
    }

    private fun retrieveNumberOrIdentifierTokenText(firstCharacter: Char): String {
        val nextCharacter = inputStream.read().toChar()
        inputStream.unreadLastCharacter()

        return if (nextCharacter.isDigit())
            retrieveNumberTokenText(firstCharacter)
        else
            retrieveIdentifierTokenText(firstCharacter)
    }

    private fun retrieveStringTokenText(firstDoubleQuote: Char) =
        ComplexTokenTextRetriever(firstDoubleQuote) { true }.retrieveToken()

    private fun retrieveNumberTokenText(firstCharacter: Char) =
        ComplexTokenTextRetriever(firstCharacter) { it.isDigit() }.retrieveToken()

    private fun retrieveIdentifierTokenText(firstCharacter: Char) =
        ComplexTokenTextRetriever(firstCharacter) { isIdentifierCharacter(it) }.retrieveToken()

    private inner class ComplexTokenTextRetriever(private val firstCharacter: Char,
                                                  private val isPartOfToken: (Char) -> Boolean) {
        private val text = StringBuilder()
        private val position = positionTracker.currentPosition()
        private var currentCharacter = firstCharacter
        private var previousCharacter = firstCharacter

        private fun isTerminatingCharacter() = isStringToken() && isTerminatingDoubleQuote()
        private fun isStringToken() = firstCharacter == DOUBLE_QUOTE
        private fun isTerminatingDoubleQuote() = currentCharacter == DOUBLE_QUOTE && previousCharacter != BACKSLASH

        fun retrieveToken(): String {
            text.append(firstCharacter)

            for (c in inputStream) {
                currentCharacter = c.toChar()

                if (!isPartOfToken(currentCharacter)) {
                    inputStream.unreadLastCharacter()

                    return text.toString()
                }

                addCharacterToToken()

                if (isTerminatingCharacter())
                    return text.toString()

                previousCharacter = currentCharacter
            }

            return terminateTokenWithEof()
        }

        private fun addCharacterToToken() {
            text.append(currentCharacter)
            positionTracker.incrementColumn()

            if (currentCharacter == NEWLINE)
                positionTracker.incrementLine()
        }

        private fun terminateTokenWithEof(): String {
            if (isStringToken())
                throw UnterminatedStringException(position)

            return text.toString()
        }
    }

    class UnterminatedStringException(position: FilePosition) : LineColumnException(position) {
        override val messagePrefix: String
            get() = "unterminated quoted string"
    }
}
