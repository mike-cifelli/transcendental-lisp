package scanner

import stream.SafeInputStream
import util.Characters.BACKSLASH
import util.Characters.DOUBLE_QUOTE
import util.Characters.EOF
import util.Characters.NEWLINE
import util.Characters.SEMICOLON
import java.io.InputStream

/**
 * Removes Lisp comments from an input stream.
 */
class LispCommentRemovingInputStream(underlyingInputStream: InputStream) : LispInputStream() {

    private val underlyingInputStream = SafeInputStream(underlyingInputStream)
    private var isInQuotedString = false
    private var rereadLastCharacter = false
    private var previousCharacter = 0
    private var currentCharacter = 0

    override fun computeNext() = read().let {
        if (it == EOF) done() else setNext(it)
    }

    override fun read(): Int {
        if (!rereadLastCharacter)
            return readFromUnderlyingInputStream()

        rereadLastCharacter = false

        return currentCharacter
    }

    override fun unreadLastCharacter() {
        if (rereadLastCharacter)
            throw LispInputStream.MaximumUnreadsExceededException()

        this.rereadLastCharacter = true
    }

    private fun readFromUnderlyingInputStream(): Int {
        readNextCharacter()

        if (haveEnteredComment())
            consumeAllBytesInComment()

        return currentCharacter
    }

    private fun readNextCharacter() {
        previousCharacter = currentCharacter
        currentCharacter = underlyingInputStream.read()

        if (haveEncounteredStringBoundary())
            isInQuotedString = !isInQuotedString
    }

    private fun haveEncounteredStringBoundary() =
        previousCharacter != BACKSLASH.toInt() && currentCharacter == DOUBLE_QUOTE.toInt()

    private fun haveEnteredComment() = currentCharacter == SEMICOLON.toInt() && !isInQuotedString

    private fun consumeAllBytesInComment() {
        while (stillInComment())
            currentCharacter = underlyingInputStream.read()
    }

    private fun stillInComment() = currentCharacter != NEWLINE.toInt() && currentCharacter != EOF
}
