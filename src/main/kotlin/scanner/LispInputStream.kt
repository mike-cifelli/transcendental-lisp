package scanner

import error.CriticalLispException

abstract class LispInputStream : AbstractIterator<Int>() {

    abstract fun read(): Int
    abstract fun unreadLastCharacter()

    class MaximumUnreadsExceededException : CriticalLispException()
}
