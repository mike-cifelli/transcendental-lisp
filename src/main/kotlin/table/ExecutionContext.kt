package table

import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import table.SymbolTable.NullSymbolTable
import java.util.Stack

object ExecutionContext {

    private val functionCalls: Stack<LispFunctionRecurInfo> = Stack()

    var scope: SymbolTable = SymbolTable(NullSymbolTable)
    var isRecur: Boolean = false
        private set

    fun isInFunctionCall() = !functionCalls.empty()
    fun isRecurInitializing() = functionCalls.peek().isRecurInitializing
    fun getCurrentFunction() = functionCalls.peek().lispFunction

    fun clearContext() {
        scope = SymbolTable(NullSymbolTable)
        functionCalls.clear()
        clearRecur()
    }

    fun restoreGlobalScope() {
        while (!scope.isGlobal())
            scope = scope.parent ?: NullSymbolTable
    }

    fun lookupSymbolValue(symbolName: String): SExpression? {
        for (table in scope)
            if (symbolName in table)
                return table[symbolName]

        return null
    }

    fun toList(): Cons {
        var symbols: Cons = Nil

        for (table in scope)
            symbols = Cons(table.toList(), symbols)

        return symbols
    }

    fun pushFunctionCall(function: LispFunction) {
        functionCalls.push(LispFunctionRecurInfo(function))
    }

    fun popFunctionCall() {
        functionCalls.pop()
    }

    fun setRecur() {
        isRecur = true
    }

    fun clearRecur() {
        isRecur = false
    }

    fun setRecurInitializing() {
        functionCalls.peek().isRecurInitializing = true
    }

    fun clearRecurInitializing() {
        functionCalls.peek().isRecurInitializing = false
    }

    private class LispFunctionRecurInfo(val lispFunction: LispFunction) {
        var isRecurInitializing: Boolean = false
    }
}
