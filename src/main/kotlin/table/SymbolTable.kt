package table

import function.builtin.cons.Append.Companion.append
import function.builtin.cons.List.Companion.makeList
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import kotlin.collections.Map.Entry

open class SymbolTable(open val parent: SymbolTable? = NullSymbolTable) : Iterable<SymbolTable> {

    private val table = mutableMapOf<String, SExpression>()

    override fun iterator(): Iterator<SymbolTable> = SymbolTableIterator(this)
    operator fun contains(symbolName: String) = symbolName in table
    operator fun get(symbolName: String) = table[symbolName]

    operator fun set(symbolName: String, value: SExpression) {
        table[symbolName] = value
    }

    fun isGlobal() = parent === NullSymbolTable

    fun toList(): Cons {
        var context: Cons = Nil

        for (binding in table.toSortedMap().entries)
            context = append(context, makeList(makeSymbolValuePair(binding)))

        return context
    }

    private fun makeSymbolValuePair(binding: Entry<String, SExpression>) =
        Cons(Symbol(binding.key), makeList(binding.value))

    object NullSymbolTable : SymbolTable(null) {
        override val parent = this
    }

    private class SymbolTableIterator(private var symbolTable: SymbolTable) : AbstractIterator<SymbolTable>() {

        override fun computeNext() {
            when (symbolTable) {
                is NullSymbolTable -> done()
                else               -> {
                    setNext(symbolTable)
                    symbolTable = symbolTable.parent ?: NullSymbolTable
                }
            }
        }
    }
}
