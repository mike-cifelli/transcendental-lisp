package environment

import error.ErrorManager
import java.io.InputStream
import java.io.PrintStream

object RuntimeEnvironment {

    var inputName: String? = null
    var input: InputStream? = null
    var output: PrintStream? = null
    var errorOutput: PrintStream? = null
    var errorManager: ErrorManager? = null
    var path: String? = null

    var terminationFunction: (() -> Unit) = {}
    var errorTerminationFunction: (() -> Unit) = {}
    var promptDecorator: ((String) -> String) = { it }
    var valueOutputDecorator: ((String) -> String) = { it }
    var warningOutputDecorator: ((String) -> String) = { it }
    var errorOutputDecorator: ((String) -> String) = { it }
    var criticalOutputDecorator: ((String) -> String) = { it }

    fun reset() {
        inputName = null
        input = null
        output = null
        errorOutput = null
        errorManager = null
        path = null
        terminationFunction = {}
        errorTerminationFunction = {}
        promptDecorator = { it }
        valueOutputDecorator = { it }
        warningOutputDecorator = { it }
        errorOutputDecorator = { it }
        criticalOutputDecorator = { it }
    }

    fun terminateSuccessfully() {
        terminationFunction()
    }

    fun terminateExceptionally() {
        errorTerminationFunction()
    }

    fun decoratePrompt(prompt: String) = promptDecorator(prompt)
    fun decorateValueOutput(valueOutput: String) = valueOutputDecorator(valueOutput)
    fun decorateWarningOutput(warningOutput: String) = warningOutputDecorator(warningOutput)
    fun decorateErrorOutput(errorOutput: String) = errorOutputDecorator(errorOutput)
    fun decorateCriticalOutput(criticalOutput: String) = criticalOutputDecorator(criticalOutput)
}
