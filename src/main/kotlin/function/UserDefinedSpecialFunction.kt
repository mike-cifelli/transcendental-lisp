package function

import sexpression.Cons

open class UserDefinedSpecialFunction(name: String,
                                      lambdaList: Cons,
                                      body: Cons) : UserDefinedFunction(name, lambdaList, body) {

    override val isArgumentListEvaluated = false
}
