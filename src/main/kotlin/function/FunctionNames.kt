package function

import kotlin.annotation.AnnotationTarget.CLASS

@Target(CLASS)
annotation class FunctionNames(vararg val value: String)
