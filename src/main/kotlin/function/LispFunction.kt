package function

import sexpression.Cons
import sexpression.SExpression

abstract class LispFunction {

    open val isArgumentListEvaluated = true
    open val isMacro = false

    abstract fun call(argumentList: Cons): SExpression
}
