package function

abstract class LispSpecialFunction : LispFunction() {

    override val isArgumentListEvaluated = false
}
