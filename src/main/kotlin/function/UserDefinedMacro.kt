package function

import sexpression.Cons

class UserDefinedMacro(name: String,
                       lambdaList: Cons,
                       body: Cons) : UserDefinedSpecialFunction(name, lambdaList, body) {

    override val isMacro = true
}
