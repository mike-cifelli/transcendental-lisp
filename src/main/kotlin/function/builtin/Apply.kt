package function.builtin

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.Eval.Companion.applyFunction
import function.builtin.Eval.Companion.lookupFunctionOrLambda
import sexpression.Cons
import sexpression.SExpression
import table.FunctionTable.lookupFunction

@FunctionNames("APPLY")
class Apply(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
        setTrailingArgumentExpectedType(Cons::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val rest = argumentList.rest as Cons
        val functionArguments = rest.first as Cons
        val functionName = argumentList.first
        val function = lookupFunctionOrLambda(functionName)

        return applyFunction(function, functionArguments)
    }

    companion object {

        fun apply(argumentList: Cons): SExpression {
            return lookupFunction("APPLY")!!.call(argumentList)
        }
    }
}
