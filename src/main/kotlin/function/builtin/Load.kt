package function.builtin

import environment.RuntimeEnvironment
import error.LispException
import error.LispWarning
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.Eval.Companion.eval
import parser.LispParser
import sexpression.Cons
import sexpression.LispString
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import util.Path
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.Stack

@FunctionNames("LOAD")
class Load(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
        setEveryArgumentExpectedType(LispString::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val quotedName = argumentList.first.toString()
        val fileName = quotedName.removeSurrounding("\"")

        return processFile(fileName)
    }

    private fun processFile(fileName: String): SExpression {
        var isSuccessful = false
        val prefixedFileName = prefixFileNameIfNecessary(fileName)
        val parser = attemptToCreateParserOnFile(prefixedFileName)

        if (parser != null)
            isSuccessful = isSuccessfulEvaluationWithPathPrefix(prefixedFileName, parser)

        return if (isSuccessful) Symbol.T else Nil
    }

    private fun prefixFileNameIfNecessary(fileName: String) =
        if (pathPrefixes.isEmpty())
            RuntimeEnvironment.path!! + fileName
        else
            pathPrefixes.peek() + fileName

    private fun attemptToCreateParserOnFile(fileName: String): LispParser? {
        var parser: LispParser? = null

        try {
            parser = LispParser(FileInputStream(fileName), fileName)
        } catch (e: FileNotFoundException) {
            RuntimeEnvironment.errorManager!!.handle(CouldNotLoadFileWarning(fileName))
        }

        return parser
    }

    private fun isSuccessfulEvaluationWithPathPrefix(prefixedFileName: String, parser: LispParser): Boolean {
        pathPrefixes.push(Path.getPathPrefix(prefixedFileName))
        val isSuccessful = isSuccessfulEvaluation(parser)
        pathPrefixes.pop()

        return isSuccessful
    }

    private fun isSuccessfulEvaluation(parser: LispParser): Boolean {
        while (!parser.isEof()) {
            try {
                eval(parser.nextSExpression())
            } catch (e: LispException) {
                RuntimeEnvironment.errorManager!!.handle(e)
                return false
            }
        }

        return true
    }

    class CouldNotLoadFileWarning(fileName: String) : LispWarning() {

        override val message = "could not load '$fileName'"
    }

    companion object {
        private val pathPrefixes = Stack<String>()
    }
}
