package function.builtin

import environment.RuntimeEnvironment
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("PRINT")
class Print(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return argumentList.first.also {
            RuntimeEnvironment.output!!.println(it)
        }
    }
}
