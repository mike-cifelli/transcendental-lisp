package function.builtin

import error.LispException
import function.ArgumentValidator
import function.builtin.Eval.Companion.eval
import sexpression.AtSignExpression
import sexpression.BackquoteExpression
import sexpression.CommaExpression
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

internal class BackquoteEvaluator(private val backTick: BackquoteExpression) {

    private val listValidator = ArgumentValidator("`|list|")
    private val atSignValidator = ArgumentValidator("@|list|")
    private val resolvedList = Cons(Nil, Nil)
    private var leader = resolvedList
    private var follower = resolvedList

    fun evaluate(): SExpression {
        var expression = backTick.expression

        if (expression.isCons)
            expression = resolveList(expression as Cons)
        else if (expression.isComma)
            expression = eval((expression as CommaExpression).expression)
        else if (expression.isAtSign)
            throw AtSignNotInCommaException()

        return expression
    }

    private fun resolveList(list: Cons): SExpression {
        listValidator.validate(list)
        createResolvedList(list)

        return resolvedList
    }

    private fun createResolvedList(list: Cons) {
        list.forEach { resolveExpression(it.first) }

        follower.rest = Nil
    }

    private fun resolveExpression(expression: SExpression) = when {
        expression.isAtSign -> throw AtSignNotInCommaException()
        expression.isComma  -> resolveCommaExpression(expression)
        expression.isList   -> resolveListExpression(expression)
        else                -> addResolvedExpression(expression)
    }

    private fun resolveCommaExpression(expression: SExpression) {
        val result = evaluateComma(expression as CommaExpression)

        if (result.isAtSign)
            unpackResolvedList(result.result as Cons)
        else
            addResolvedExpression(result.result)
    }

    private fun evaluateComma(comma: CommaExpression): CommaEvaluationResult {
        val expression = comma.expression
        validateCommaExpression(expression)

        return if (expression.isAtSign)
            CommaEvaluationAtSignResult(evaluateAtSign(expression as AtSignExpression))
        else
            CommaEvaluationResult(eval(expression))
    }

    private fun validateCommaExpression(expression: SExpression) {
        if (expression.isComma)
            throw NestedCommaException()
    }

    private fun evaluateAtSign(atSign: AtSignExpression): Cons {
        val expression = atSign.expression
        validateAtSignUnevaluatedExpression(expression)
        val evaluation = eval(expression)

        return getValidatedList(evaluation)
    }

    private fun validateAtSignUnevaluatedExpression(expression: SExpression) {
        if (expression.isComma)
            throw NestedCommaException()
        else if (expression.isAtSign)
            throw NestedAtSignException()
    }

    private fun getValidatedList(evaluation: SExpression): Cons {
        if (!evaluation.isList)
            throw AtSignNotListException()

        val evaluatedList = evaluation as Cons
        atSignValidator.validate(evaluatedList)

        return evaluatedList
    }

    private fun unpackResolvedList(list: Cons) {
        list.forEach { addResolvedExpression(it.first) }
    }

    private fun addResolvedExpression(expression: SExpression) {
        leader.first = expression
        leader.rest = Cons(Nil, Nil)
        follower = leader
        leader = leader.rest as Cons
    }

    private fun resolveListExpression(expression: SExpression) {
        val evaluator = BackquoteEvaluator(BackquoteExpression(expression))
        addResolvedExpression(evaluator.evaluate())
    }

    private open class CommaEvaluationResult(val result: SExpression) {

        open val isAtSign = false
    }

    private class CommaEvaluationAtSignResult(result: SExpression) : CommaEvaluationResult(result) {

        override val isAtSign = true
    }

    class NestedCommaException : LispException() {

        override val message = "nested comma"
    }

    class NestedAtSignException : LispException() {

        override val message = "nested at sign"
    }

    class AtSignNotInCommaException : LispException() {

        override val message = "at sign not in comma"
    }

    class AtSignNotListException : LispException() {

        override val message = "at sign did not evaluate to a list"
    }
}
