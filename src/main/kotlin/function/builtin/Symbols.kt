package function.builtin

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.SExpression
import table.ExecutionContext

@FunctionNames("SYMBOLS")
class Symbols(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(0)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return ExecutionContext.toList()
    }
}
