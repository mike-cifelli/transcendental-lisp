package function.builtin

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.Apply.Companion.apply
import function.builtin.cons.List.Companion.makeList
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("FUNCALL", "CALL")
class Funcall(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(1)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)
        val applyArgs = Cons(argumentList.first, makeList(argumentList.rest))

        return apply(applyArgs)
    }
}
