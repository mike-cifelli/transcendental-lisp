package function.builtin

import error.LispException
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.cons.List.Companion.makeList
import function.builtin.special.Lambda.Lambda.createFunction
import function.builtin.special.Lambda.Lambda.isLambdaExpression
import function.builtin.special.Recur.RecurNotInTailPositionException
import sexpression.BackquoteExpression
import sexpression.Cons
import sexpression.LambdaExpression
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import sexpression.Symbol.Companion.T
import table.ExecutionContext
import table.FunctionTable.lookupFunction

@FunctionNames("EVAL")
class Eval(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
    }

    override fun call(argumentList: Cons): SExpression {
        verifyNotRecurring()
        argumentValidator.validate(argumentList)

        return evaluateExpression(argumentList.first)
    }

    private fun verifyNotRecurring() {
        if (ExecutionContext.isRecur) {
            ExecutionContext.clearRecur()
            throw RecurNotInTailPositionException()
        }
    }

    private fun evaluateExpression(argument: SExpression) = when {
        argument.isList      -> evaluateList(argument)
        argument.isSymbol    -> evaluateSymbol(argument)
        argument.isBackquote -> evaluateBackTick(argument)
        argument.isComma     -> throw UnmatchedCommaException()
        argument.isAtSign    -> throw UnmatchedAtSignException()
        else                 -> argument // NUMBER or STRING
    }

    private fun evaluateList(argument: SExpression) =
        if (argument.isCons)
            evaluateFunction(argument as Cons)
        else
            argument // NIL

    private fun evaluateFunction(list: Cons): SExpression {
        val functionName = list.first
        val arguments = list.rest
        val function = lookupFunctionOrLambda(functionName)
        validateFunctionList(list, functionName)

        return callFunction(function, arguments as Cons)
    }

    private fun validateFunctionList(list: Cons, functionName: SExpression) {
        ArgumentValidator(functionName.toString()).validate(list)
    }

    private fun callFunction(function: LispFunction, argumentList: Cons) =
        if (function.isArgumentListEvaluated)
            applyFunctionWithoutEvaluatingArguments(function, evaluateArgumentList(argumentList))
        else
            applyFunctionWithoutEvaluatingArguments(function, argumentList)

    private fun applyFunctionWithoutEvaluatingArguments(function: LispFunction, argumentList: Cons): SExpression {
        verifyNotRecurring()

        return function.call(argumentList).let {
            if (function.isMacro) eval(it) else it
        }
    }

    private fun evaluateArgumentList(arguments: Cons): Cons {
        if (arguments.isNull)
            return Nil

        val first = eval(arguments.first)
        val rest = arguments.rest as Cons

        return Cons(first, evaluateArgumentList(rest))
    }

    private fun evaluateSymbol(argument: SExpression) =
        lookupSymbol(argument.toString()) ?: throw UndefinedSymbolException(argument)

    private fun evaluateBackTick(argument: SExpression) =
        BackquoteEvaluator(argument as BackquoteExpression).evaluate()

    companion object {

        private fun lookupEval() = lookupFunction("EVAL") as Eval

        fun eval(sExpression: SExpression): SExpression {
            try {
                return lookupEval().call(makeList(sExpression))
            } catch (e: LispException) {
                ExecutionContext.restoreGlobalScope()
                throw e
            }
        }

        fun applyFunction(function: LispFunction, argumentList: Cons) =
            lookupEval().applyFunctionWithoutEvaluatingArguments(function, argumentList)

        fun evaluateFunctionArgumentList(argumentList: Cons) = lookupEval().evaluateArgumentList(argumentList)

        fun lookupSymbol(symbolName: String) = when {
            symbolName == "NIL"        -> Nil
            symbolName == "T"          -> T
            symbolName.startsWith(":") -> Symbol(symbolName)
            else                       -> ExecutionContext.lookupSymbolValue(symbolName)
        }

        fun lookupFunctionOrLambda(functionExpression: SExpression): LispFunction {
            val function = lookupFunction(functionExpression.toString())

            return function ?: createLambdaFunction(functionExpression)
        }

        private fun createLambdaFunction(lambdaExpression: SExpression) = when {
            lambdaExpression.isFunction          -> (lambdaExpression as LambdaExpression).function
            isLambdaExpression(lambdaExpression) -> createFunction(lambdaExpression as Cons)
            else                                 -> throw UndefinedFunctionException(lambdaExpression)
        }
    }

    class UndefinedFunctionException(function: SExpression) : LispException() {

        override val message = "undefined function: $function"
    }

    class UndefinedSymbolException(symbol: SExpression) : LispException() {

        override val message = "symbol $symbol has no value"
    }

    class UnmatchedCommaException : LispException() {

        override val message = "unmatched comma"
    }

    class UnmatchedAtSignException : LispException() {

        override val message = "unmatched at sign"
    }
}
