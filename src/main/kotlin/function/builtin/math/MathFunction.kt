package function.builtin.math

import sexpression.Cons
import sexpression.LispNumber

internal class MathFunction(private val singleValueOperation: (LispNumber) -> LispNumber,
                            private val multipleValueOperation: (LispNumber, LispNumber) -> LispNumber) {

    tailrec fun callTailRecursive(argumentList: Cons): LispNumber {
        val remainingArguments = argumentList.rest as Cons
        val firstArgument = argumentList.first
        val number1 = firstArgument as LispNumber

        if (remainingArguments.isNull)
            return singleValueOperation(number1)

        val secondArgument = remainingArguments.first
        val number2 = secondArgument as LispNumber
        val operationResult = multipleValueOperation(number1, number2)
        val remainingNumbers = remainingArguments.rest

        return if (remainingNumbers.isNull)
            operationResult
        else
            callTailRecursive(Cons(operationResult, remainingNumbers))
    }
}
