package function.builtin.math

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.LispNumber

@FunctionNames("+")
class Plus(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setEveryArgumentExpectedType(LispNumber::class.java)
    }

    private val mathFunction: MathFunction = MathFunction({ it }, this::add)

    override fun call(argumentList: Cons): LispNumber {
        argumentValidator.validate(argumentList)

        return mathFunction.callTailRecursive(Cons(LispNumber.ZERO, argumentList))
    }

    private fun add(number1: LispNumber, number2: LispNumber) =
        LispNumber(number1.value + number2.value)
}
