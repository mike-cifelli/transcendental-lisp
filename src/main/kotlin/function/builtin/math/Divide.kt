package function.builtin.math

import error.LispException
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.LispNumber
import java.math.BigInteger.ONE

@FunctionNames("/")
class Divide(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(1)
        setEveryArgumentExpectedType(LispNumber::class.java)
    }

    private val mathFunction = MathFunction(this::getReciprocal, this::divide)

    override fun call(argumentList: Cons): LispNumber {
        argumentValidator.validate(argumentList)

        try {
            return mathFunction.callTailRecursive(argumentList)
        } catch (e: ArithmeticException) {
            throw DivideByZeroException()
        }
    }

    private fun getReciprocal(number: LispNumber) =
        LispNumber(ONE / number.value)

    private fun divide(number1: LispNumber, number2: LispNumber) =
        LispNumber(number1.value / number2.value)

    class DivideByZeroException : LispException() {

        override val message = "divide by zero"
    }
}
