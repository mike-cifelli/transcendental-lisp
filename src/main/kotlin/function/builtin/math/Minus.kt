package function.builtin.math

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.LispNumber

import java.math.BigInteger.ZERO

@FunctionNames("-")
class Minus(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(1)
        setEveryArgumentExpectedType(LispNumber::class.java)
    }

    private val mathFunction: MathFunction = MathFunction(this::additiveInverse, this::subtract)

    override fun call(argumentList: Cons): LispNumber {
        argumentValidator.validate(argumentList)

        return mathFunction.callTailRecursive(argumentList)
    }

    private fun additiveInverse(number: LispNumber) =
        LispNumber(ZERO - number.value)

    private fun subtract(number1: LispNumber, number2: LispNumber) =
        LispNumber(number1.value - number2.value)
}
