package function.builtin.math

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.math.Divide.DivideByZeroException
import sexpression.Cons
import sexpression.LispNumber
import sexpression.SExpression

@FunctionNames("REM", "REMAINDER")
class Remainder(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
        setEveryArgumentExpectedType(LispNumber::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val dividend = argumentList.first as LispNumber
        val divisor = (argumentList.rest as Cons).first as LispNumber

        try {
            return LispNumber(dividend.value % divisor.value)
        } catch (e: ArithmeticException) {
            throw DivideByZeroException()
        }
    }
}
