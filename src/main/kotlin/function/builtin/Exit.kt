package function.builtin

import environment.RuntimeEnvironment
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

@FunctionNames("EXIT")
class Exit(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMaximumNumberOfArguments(0)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)
        RuntimeEnvironment.terminateSuccessfully()

        return Nil
    }
}
