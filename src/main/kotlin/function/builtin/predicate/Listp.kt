package function.builtin.predicate

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol.Companion.T

@FunctionNames("LISTP", "LIST?")
class Listp(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return if (argumentList.first.isList) T else Nil
    }
}
