package function.builtin.predicate

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol.Companion.T

@FunctionNames("EQUAL", "EQUAL?")
class Equal(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val rest = argumentList.rest as Cons
        val firstArgument = argumentList.first
        val secondArgument = rest.first

        return equal(firstArgument, secondArgument)
    }

    private fun equal(firstArgument: SExpression, secondArgument: SExpression) =
        if (isEqual(firstArgument, secondArgument)) T else Nil

    companion object {

        fun isEqual(firstArgument: SExpression, secondArgument: SExpression) =
            firstArgument.toString() == secondArgument.toString()
    }
}
