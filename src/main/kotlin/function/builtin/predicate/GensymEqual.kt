package function.builtin.predicate

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.builtin.Gensym.Companion.GENSYM_PREFIX
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import java.util.regex.Pattern

@FunctionNames("GENSYM-EQUAL", "GENSYM-EQUAL?")
class GensymEqual(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val rest = argumentList.rest as Cons
        val firstArgument = argumentList.first
        val secondArgument = rest.first

        return gensymEqual(firstArgument, secondArgument)
    }

    private fun gensymEqual(firstArgument: SExpression, secondArgument: SExpression): SExpression {
        val firstEqualized = equalizeGensyms(firstArgument)
        val secondEqualized = equalizeGensyms(secondArgument)

        return if (firstEqualized == secondEqualized) Symbol.T else Nil
    }

    private fun equalizeGensyms(expression: SExpression) =
        GensymEqualizer(expression.toString()).equalize()

    private class GensymEqualizer(internal var expression: String) {

        internal val gensymAliases = mutableMapOf<String, String>()
        internal var matcher = Pattern.compile(GENSYM_REGEX).matcher(expression)
        internal var counter: Int = 0

        fun equalize(): String {
            createGensymAliases()

            return equalizeGensyms()
        }

        private fun createGensymAliases() {
            while (matcher.find())
                createAliasForGensym()
        }

        private fun createAliasForGensym() {
            val gensym = matcher.group()

            if (isNewGensym(gensym))
                gensymAliases[gensym] = GENSYM_PREFIX + counter++
        }

        private fun isNewGensym(gensym: String): Boolean {
            return !gensymAliases.containsKey(gensym)
        }

        private fun equalizeGensyms(): String {
            var equalizedExpression = expression

            for ((key, value) in gensymAliases)
                equalizedExpression = equalizedExpression.replace(key, value)

            return equalizedExpression
        }

        companion object {

            private val GENSYM_REGEX = Pattern.quote(GENSYM_PREFIX) + "[0-9]+"
        }
    }
}
