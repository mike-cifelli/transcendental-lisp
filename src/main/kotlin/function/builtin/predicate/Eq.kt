package function.builtin.predicate

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol.Companion.T

@FunctionNames("EQ", "EQ?")
class Eq(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val rest = argumentList.rest as Cons
        val firstArgument = argumentList.first
        val secondArgument = rest.first

        return eq(firstArgument, secondArgument)
    }

    private fun eq(firstArgument: SExpression, secondArgument: SExpression) =
        if (isAtomPair(firstArgument, secondArgument))
            atomEq(firstArgument, secondArgument)
        else
            listEq(firstArgument, secondArgument)

    private fun isAtomPair(firstArgument: SExpression, secondArgument: SExpression) =
        firstArgument.isAtom && secondArgument.isAtom

    private fun atomEq(firstArgument: SExpression, secondArgument: SExpression) =
        if (isEq(firstArgument, secondArgument)) T else Nil

    private fun isEq(firstArgument: SExpression, secondArgument: SExpression) =
        firstArgument.toString() == secondArgument.toString()

    private fun listEq(firstArgument: SExpression, secondArgument: SExpression) =
        if (firstArgument === secondArgument) T else Nil
}
