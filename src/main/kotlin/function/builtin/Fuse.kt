package function.builtin

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Atom
import sexpression.Cons
import sexpression.LispString
import sexpression.SExpression
import sexpression.Symbol

@FunctionNames("FUSE")
class Fuse(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
        setFirstArgumentExpectedType(Symbol::class.java)
        setTrailingArgumentExpectedType(Atom::class.java)
        setTrailingArgumentExcludedType(LispString::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val left = argumentList.first as Symbol
        val right = (argumentList.rest as Cons).first as Atom

        return Symbol("$left$SEPARATOR$right")
    }

    companion object {
        private const val SEPARATOR = "-"
    }
}
