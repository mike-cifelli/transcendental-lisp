package function.builtin

import error.LispException
import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import function.LispSpecialFunction
import function.UserDefinedFunction
import sexpression.Cons
import sexpression.SExpression
import sexpression.Symbol
import table.FunctionTable.lookupFunction

@FunctionNames("SYMBOL-FUNCTION")
class SymbolFunction(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
        setEveryArgumentExpectedType(Symbol::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val symbol = argumentList.first
        val function = lookupFunction(symbol.toString())

        if (function != null)
            return createRepresentation(symbol, function)

        throw UndefinedSymbolFunctionException(symbol)
    }

    private fun createRepresentation(symbol: SExpression, function: LispFunction): SExpression {
        if (function is UserDefinedFunction)
            return function.lambdaExpression

        val typeIndicator = if (function is LispSpecialFunction) "SPECIAL-FUNCTION" else "FUNCTION"

        return Symbol("#<$typeIndicator $symbol>")
    }

    class UndefinedSymbolFunctionException(function: SExpression) : LispException() {

        override val message = "SYMBOL-FUNCTION: undefined function: $function"
    }
}
