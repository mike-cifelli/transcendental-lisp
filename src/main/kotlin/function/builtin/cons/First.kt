package function.builtin.cons

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("FIRST", "CAR")
class First(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
        setEveryArgumentExpectedType(Cons::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)
        val argument = argumentList.first as Cons

        return argument.first
    }
}
