package function.builtin.cons

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons

@FunctionNames("CONS")
class Construct(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
    }

    override fun call(argumentList: Cons): Cons {
        argumentValidator.validate(argumentList)

        val rest = argumentList.rest as Cons
        val firstArgument = argumentList.first
        val secondArgument = rest.first

        return Cons(firstArgument, secondArgument)
    }
}
