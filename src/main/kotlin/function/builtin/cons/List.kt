package function.builtin.cons

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

@FunctionNames("LIST")
class List(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name)

    override fun call(argumentList: Cons): Cons {
        argumentValidator.validate(argumentList)

        return argumentList
    }

    companion object {

        fun makeList(sexpr: SExpression): Cons {
            return Cons(sexpr, Nil)
        }
    }
}
