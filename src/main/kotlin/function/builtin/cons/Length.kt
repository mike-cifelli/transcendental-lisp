package function.builtin.cons

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.LispNumber
import table.FunctionTable.lookupFunction
import java.math.BigInteger.ZERO

@FunctionNames("LENGTH")
class Length(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
        setEveryArgumentExpectedType(Cons::class.java)
    }

    private val properListValidator = ArgumentValidator("$name|list|")

    override fun call(argumentList: Cons): LispNumber {
        argumentValidator.validate(argumentList)

        val arguments = argumentList.first as Cons
        properListValidator.validate(arguments)

        return getLength(arguments)
    }

    private fun getLength(arguments: Cons) =
        LispNumber(arguments.fold(ZERO) { count, _ -> count.inc() })

    companion object {

        fun getLength(list: Cons) = lookupLength().getLength(list).value
        private fun lookupLength() = lookupFunction("LENGTH") as Length
    }
}
