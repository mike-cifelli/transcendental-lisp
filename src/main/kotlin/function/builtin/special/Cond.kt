package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

@FunctionNames("COND")
class Cond(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setEveryArgumentExpectedType(Cons::class.java)
        setEveryArgumentExcludedType(Nil::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return callTailRecursive(argumentList)
    }

    private tailrec fun callTailRecursive(argumentList: Cons): SExpression {
        if (argumentList.isNull)
            return Nil

        val clause = argumentList.first as Cons
        val remainingClauses = argumentList.rest as Cons
        val test = eval(clause.first)

        return if (!test.isNull)
            evaluateConsequents(clause, test)
        else
            callTailRecursive(remainingClauses)
    }

    private fun evaluateConsequents(clause: Cons, test: SExpression) =
        clause.drop(1).fold(test) { _, cons -> eval(cons.first) }
}
