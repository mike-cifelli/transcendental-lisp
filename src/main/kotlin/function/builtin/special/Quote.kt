package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("QUOTE")
class Quote(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(1)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return argumentList.first
    }
}
