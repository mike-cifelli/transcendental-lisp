package function.builtin.special

import environment.RuntimeEnvironment
import error.LispWarning
import function.ArgumentValidator
import function.LispSpecialFunction
import function.UserDefinedFunction
import function.builtin.cons.List.Companion.makeList
import sexpression.Cons
import sexpression.SExpression
import sexpression.Symbol
import table.FunctionTable

abstract class Define(functionName: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(functionName).apply {
        setMinimumNumberOfArguments(2)
        setFirstArgumentExpectedType(Symbol::class.java)
    }

    private val lambdaListIsListValidator = ArgumentValidator("$functionName|lambda-list|").apply {
        setEveryArgumentExpectedType(Cons::class.java)
    }

    private val lambdaListValidator = ArgumentValidator("$functionName|parameter|").apply {
        setEveryArgumentExpectedType(Symbol::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val remainingArguments = argumentList.rest as Cons
        val functionName = argumentList.first
        val secondArgument = remainingArguments.first
        lambdaListIsListValidator.validate(makeList(secondArgument))

        val lambdaList = secondArgument as Cons
        lambdaListValidator.validate(lambdaList)

        val functionBody = remainingArguments.rest as Cons
        val function = createFunction(functionName, lambdaList, functionBody)

        if (FunctionTable.isAlreadyDefined(functionName.toString()))
            RuntimeEnvironment.errorManager!!.handle(RedefiningFunctionWarning(functionName.toString()))

        FunctionTable.defineFunction(functionName.toString(), function)

        return functionName
    }

    protected abstract fun createFunction(functionName: SExpression,
                                          lambdaList: Cons,
                                          functionBody: Cons): UserDefinedFunction

    inner class RedefiningFunctionWarning(functionName: String) : LispWarning() {

        override val message = "redefining function $functionName"
    }
}
