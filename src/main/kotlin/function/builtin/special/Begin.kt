package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression

@FunctionNames("PROGN", "BEGIN")
class Begin(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name)

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return callTailRecursive(argumentList, Nil)
    }

    private tailrec fun callTailRecursive(argumentList: Cons, lastValue: SExpression): SExpression {
        if (argumentList.isNull)
            return lastValue

        val currentValue = eval(argumentList.first)
        val remainingValues = argumentList.rest as Cons

        return callTailRecursive(remainingValues, currentValue)
    }
}
