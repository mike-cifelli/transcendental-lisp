package function.builtin.special

import function.FunctionNames
import function.UserDefinedSpecialFunction
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("DEFINE-SPECIAL")
class DefineSpecial(name: String) : Define(name) {

    override fun createFunction(functionName: SExpression, lambdaList: Cons, functionBody: Cons) =
        UserDefinedSpecialFunction(functionName.toString(), lambdaList, functionBody)
}
