package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import function.builtin.Set.Companion.set
import function.builtin.cons.List.Companion.makeList
import sexpression.Cons
import sexpression.SExpression
import sexpression.Symbol

@FunctionNames("SETQ")
class Setq(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setExactNumberOfArguments(2)
        setFirstArgumentExpectedType(Symbol::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return set(evaluateValueOnly(argumentList))
    }

    private fun evaluateValueOnly(argumentList: Cons): Cons {
        val rest = argumentList.rest as Cons
        val value = eval(rest.first)

        return Cons(argumentList.first, makeList(value))
    }
}
