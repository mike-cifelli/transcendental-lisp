package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.UserDefinedFunction
import function.builtin.cons.List.Companion.makeList
import sexpression.Cons
import sexpression.LambdaExpression
import sexpression.SExpression
import sexpression.Symbol

@FunctionNames("LAMBDA", "Λ")
class Lambda(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setFirstArgumentExpectedType(Cons::class.java)
        setMinimumNumberOfArguments(1)
    }

    private val lambdaListValidator = ArgumentValidator("$name|lambda-list|").apply {
        setEveryArgumentExpectedType(Symbol::class.java)
    }

    override fun call(argumentList: Cons): LambdaExpression {
        argumentValidator.validate(argumentList)

        val first = argumentList.first
        val lambdaList = first as Cons
        val body = argumentList.rest as Cons

        lambdaListValidator.validate(lambdaList)

        val function = UserDefinedFunction(":LAMBDA", lambdaList, body)

        return LambdaExpression(makeOriginalLambdaExpression(argumentList), function)
    }

    private fun makeOriginalLambdaExpression(argumentList: Cons): Cons {
        return Cons(Symbol("LAMBDA"), argumentList)
    }

    companion object Lambda {

        fun isLambdaExpression(sexpr: SExpression): Boolean {
            if (sexpr.isCons) {
                val first = (sexpr as Cons).first.toString()

                return "LAMBDA" == first || "Λ" == first
            }

            return false
        }

        fun createFunction(lambdaExpression: Cons): UserDefinedFunction {
            val rest = lambdaExpression.rest

            ArgumentValidator("LAMBDA|create|").run {
                setEveryArgumentExpectedType(Cons::class.java)
                validate(makeList(rest))
            }

            val lambda = Lambda("LAMBDA").call(rest as Cons)

            return lambda.function
        }
    }
}
