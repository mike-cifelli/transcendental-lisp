package function.builtin.special

import function.FunctionNames
import sexpression.Cons
import table.ExecutionContext
import table.SymbolTable

@FunctionNames("LET*")
class LetStar(name: String) : Let(name) {

    override fun createLocalScope(variableDefinitions: Cons) =
        SymbolTable(ExecutionContext.scope).also {
            ExecutionContext.scope = it
            addVariablesToScope(it, variableDefinitions)
        }
}
