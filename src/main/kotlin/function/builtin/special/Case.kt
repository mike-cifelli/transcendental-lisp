package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import function.builtin.predicate.Equal.Companion.isEqual
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol.Companion.T

@FunctionNames("CASE")
class Case(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(1)
        setTrailingArgumentExpectedType(Cons::class.java)
        setTrailingArgumentExcludedType(Nil::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)
        val key = eval(argumentList.first)

        return callTailRecursive(key, argumentList.rest as Cons)
    }

    private tailrec fun callTailRecursive(key: SExpression, argumentList: Cons): SExpression {
        if (argumentList.isNull)
            return Nil

        val clause = argumentList.first as Cons
        val remainingClauses = argumentList.rest as Cons
        val keyList = clause.first

        return if (isMatch(key, keyList))
            evaluateConsequents(clause)
        else
            callTailRecursive(key, remainingClauses)
    }

    private fun isMatch(key: SExpression, keyList: SExpression) = when {
        keyList.isNull -> false
        keyList.isCons -> containsMatch(key, keyList as Cons)
        else           -> isEqual(key, keyList) || isEqual(T, keyList)
    }

    private fun containsMatch(key: SExpression, keyList: Cons): Boolean {
        keyList.forEach {
            if (isEqual(key, it.first)) {
                return true
            }
        }

        return false
    }

    private fun evaluateConsequents(clause: Cons) =
        clause.drop(1).fold(Nil as SExpression) { _, cons -> eval(cons.first) }
}
