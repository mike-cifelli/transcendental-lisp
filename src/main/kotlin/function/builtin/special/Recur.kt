package function.builtin.special

import error.LispException
import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.evaluateFunctionArgumentList
import sexpression.Cons
import sexpression.SExpression
import table.ExecutionContext

@FunctionNames("RECUR")
class Recur(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name)

    private fun isRecurArgumentListEvaluated() =
        ExecutionContext.getCurrentFunction().isArgumentListEvaluated

    override fun call(argumentList: Cons): SExpression {
        verifyValidRecurCall()
        argumentValidator.validate(argumentList)
        val recurArguments = getRecurArguments(argumentList)
        ExecutionContext.setRecur()

        return recurArguments
    }

    private fun verifyValidRecurCall() {
        if (!ExecutionContext.isInFunctionCall())
            throw RecurOutsideOfFunctionException()

        if (ExecutionContext.isRecurInitializing())
            throw NestedRecurException()
    }

    private fun getRecurArguments(argumentList: Cons): Cons {
        var recurArguments = argumentList

        try {
            ExecutionContext.setRecurInitializing()

            if (isRecurArgumentListEvaluated())
                recurArguments = evaluateFunctionArgumentList(argumentList)
        } finally {
            ExecutionContext.clearRecurInitializing()
        }

        return recurArguments
    }

    class RecurOutsideOfFunctionException : LispException() {

        override val message = "recur called outside of function"
    }

    class NestedRecurException : LispException() {

        override val message = "nested call to recur"
    }

    class RecurNotInTailPositionException : LispException() {

        override val message = "recur not in tail position"
    }
}
