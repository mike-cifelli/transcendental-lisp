package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import table.ExecutionContext
import table.SymbolTable

@FunctionNames("LET")
open class Let(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(1)
        setFirstArgumentExpectedType(Cons::class.java)

    }

    private val variableDefinitionListValidator = ArgumentValidator("$name|pair-list|").apply {
        setEveryArgumentExpectedType(Cons::class.java)
    }

    private val pairValidator: ArgumentValidator = ArgumentValidator("$name|pair|").apply {
        setMinimumNumberOfArguments(1)
        setMaximumNumberOfArguments(2)
        setFirstArgumentExpectedType(Symbol::class.java)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val variableDefinitions = argumentList.first as Cons
        val body = argumentList.rest as Cons

        return evaluateInScope(variableDefinitions, body)
    }

    private fun evaluateInScope(variableDefinitions: Cons, body: Cons): SExpression {
        val localScope = createLocalScope(variableDefinitions)
        val lastEvaluation = evaluateBody(body)
        restorePreviousScope(localScope)

        return lastEvaluation
    }

    protected open fun createLocalScope(variableDefinitions: Cons) =
        SymbolTable(ExecutionContext.scope).also {
            addVariablesToScope(it, variableDefinitions)
            ExecutionContext.scope = it
        }

    protected fun addVariablesToScope(scope: SymbolTable, variableDefinitions: Cons) {
        variableDefinitionListValidator.validate(variableDefinitions)
        variableDefinitions.forEach { addPairToScope(it.first as Cons, scope) }
    }

    private fun addPairToScope(symbolValuePair: Cons, scope: SymbolTable) {
        pairValidator.validate(symbolValuePair)

        val restOfPair = symbolValuePair.rest as Cons
        val symbol = symbolValuePair.first.toString()
        val value = eval(restOfPair.first)

        scope[symbol] = value
    }

    private fun evaluateBody(body: Cons) =
        body.fold(Nil as SExpression) { _, cons -> eval(cons.first) }

    private fun restorePreviousScope(localScope: SymbolTable) {
        ExecutionContext.scope = localScope.parent!!
    }
}
