package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import sexpression.Cons
import sexpression.SExpression
import sexpression.Symbol.Companion.T

@FunctionNames("AND")
class And(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name)

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return callTailRecursive(argumentList, T)
    }

    private tailrec fun callTailRecursive(argumentList: Cons, lastValue: SExpression): SExpression {
        val currentValue = eval(argumentList.first)
        val remainingValues = argumentList.rest as Cons

        return when {
            argumentList.isNull -> lastValue
            currentValue.isNull -> currentValue
            else                -> callTailRecursive(remainingValues, currentValue)
        }
    }
}
