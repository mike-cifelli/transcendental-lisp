package function.builtin.special

import function.ArgumentValidator
import function.FunctionNames
import function.LispSpecialFunction
import function.builtin.Eval.Companion.eval
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("IF")
class If(name: String) : LispSpecialFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMinimumNumberOfArguments(2)
        setMaximumNumberOfArguments(3)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        val test = eval(argumentList.first)
        val thenForm = getThenForm(argumentList)
        val elseForm = getElseForm(argumentList)

        return if (test.isNull) eval(elseForm) else eval(thenForm)
    }

    private fun getRestOfList(argumentList: Cons) = argumentList.rest as Cons
    private fun getThenForm(argumentList: Cons) = getRestOfList(argumentList).first
    private fun getElseForm(argumentList: Cons) = getRestOfList(argumentList).let { getRestOfList(it).first }
}
