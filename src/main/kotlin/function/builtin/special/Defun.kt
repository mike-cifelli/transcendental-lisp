package function.builtin.special

import function.FunctionNames
import function.UserDefinedFunction
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("DEFUN")
class Defun(name: String) : Define(name) {

    override fun createFunction(functionName: SExpression, lambdaList: Cons, functionBody: Cons) =
        UserDefinedFunction(functionName.toString(), lambdaList, functionBody)
}
