package function.builtin.special

import function.FunctionNames
import function.UserDefinedMacro
import sexpression.Cons
import sexpression.SExpression

@FunctionNames("DEFMACRO")
class Defmacro(name: String) : Define(name) {

    override fun createFunction(functionName: SExpression, lambdaList: Cons, functionBody: Cons) =
        UserDefinedMacro(functionName.toString(), lambdaList, functionBody)
}
