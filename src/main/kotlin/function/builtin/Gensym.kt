package function.builtin

import function.ArgumentValidator
import function.FunctionNames
import function.LispFunction
import sexpression.Cons
import sexpression.SExpression
import sexpression.Symbol
import java.math.BigInteger.ONE
import java.math.BigInteger.ZERO

@FunctionNames("GENSYM")
class Gensym(name: String) : LispFunction() {

    private val argumentValidator = ArgumentValidator(name).apply {
        setMaximumNumberOfArguments(0)
    }

    override fun call(argumentList: Cons): SExpression {
        argumentValidator.validate(argumentList)

        return generateSymbol()
    }

    companion object {

        const val GENSYM_PREFIX = "#G"

        private var counter = ZERO

        private fun generateSymbol(): Symbol {
            incrementCounter()

            return Symbol(GENSYM_PREFIX + counter)
        }

        private fun incrementCounter() {
            counter = counter.add(ONE)
        }
    }
}
