package recursion

interface MutualTailCall<T> {

    fun isTerminal() = false
    fun apply(): MutualTailCall<T>
    fun result(): T = throw UnsupportedOperationException()

    operator fun invoke() = generateSequence(this) { it.apply() }
        .first { it.isTerminal() }
        .result()
}
