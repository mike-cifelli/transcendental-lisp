package recursion

object MutualTailCalls {

    fun <T> recursiveCall(nextCall: () -> MutualTailCall<T>) = object : MutualTailCall<T> {
        override fun apply() = nextCall()
    }

    fun <T> terminalValue(value: T) = object : MutualTailCall<T> {
        override fun isTerminal() = true
        override fun result() = value
        override fun apply() = throw UnsupportedOperationException()
    }
}