)(defun extend-null (the-list)
  (cond
   ((equal (length the-list) 0) t)
   (t nil)
  )
)

(defun mapcar (function-name the-list)
  (cond
   ((null the-list) nil)
   (t (cons (funcall function-name (first the-list))
	    (mapcar function-name (rest the-list))))
  )
)

(defun maplist (function-name the-list)
  (cond
   ((null the-list) nil)
   (t (cons (funcall function-name the-list)
	    (maplist function-name (rest the-list))))
  )
)

(defun extend-apply (function-name param-list)
  (eval (cons function-name param-list)))

(defun append (listA listB)
  (cond
   ((null listA) listB)
   (t (cons (first listA) (append (rest listA) listB)))
  )
)

(defun second (listA) (first (rest listA)))
(defun third (listA) (first (rest (rest listA))))
(defun fourth (listA) (first (rest (rest (rest listA)))))
(defun fifth (listA) (first (rest (rest (rest (rest listA))))))
(defun sixth (listA) (first (rest (rest (rest (rest (rest listA)))))))
(defun seventh (listA) (first (rest (rest (rest (rest (rest (rest listA))))))))
(defun eighth (listA) (first (rest (rest (rest (rest (rest (rest (rest listA)))))))))
(defun ninth (listA) (first (rest (rest (rest (rest (rest (rest (rest (rest listA))))))))))
(defun tenth (listA) (first (rest (rest (rest (rest (rest (rest (rest (rest (rest listA)))))))))))

(defun nth (n listA)
  (cond
   ((equal 0 n) (first listA))
   (t (nth (- n 1) (rest listA)))
  )
)

