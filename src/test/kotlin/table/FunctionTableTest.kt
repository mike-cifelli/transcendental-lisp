package table

import error.Severity
import function.FunctionNames
import function.LispFunction
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.LispString
import sexpression.Nil
import sexpression.Symbol.Companion.T
import table.FunctionTable.LispFunctionInstantiationException
import table.FunctionTable.defineFunction
import table.FunctionTable.isAlreadyDefined
import table.FunctionTable.lookupFunction
import table.FunctionTable.resetFunctionTable
import testutil.LispTestInstance

@LispTestInstance
class FunctionTableTest {

    @FunctionNames("GOOD")
    class GoodFunction(private val name: String) : LispFunction() {
        override fun call(argumentList: Cons): LispString = LispString(name)
    }

    @FunctionNames("BAD")
    class BadFunction : LispFunction() {
        override fun call(argumentList: Cons): Nil = Nil
    }

    class UglyFunction : LispFunction() {
        override fun call(argumentList: Cons): Nil = Nil
    }

    private fun createLispFunction() = object : LispFunction() {
        override fun call(argumentList: Cons) = T
    }

    @BeforeEach
    fun setUp() {
        resetFunctionTable()
    }

    @AfterEach
    fun tearDown() {
        resetFunctionTable()
    }

    @Test
    fun `built-in function is defined`() {
        assertThat(isAlreadyDefined("CONS")).isTrue()
    }

    @Test
    fun `undefined function is not defined`() {
        assertThat(isAlreadyDefined("undefined")).isFalse()
    }

    @Test
    fun `lookup a built-in function`() {
        assertThat(lookupFunction("CONS")).isNotNull()
    }

    @Test
    fun `look up an undefined function`() {
        assertThat(lookupFunction("undefined")).isNull()
    }

    @Test
    fun `function is defined`() {
        val functionName = "testFunction"
        val testFunction = createLispFunction()

        assertThat(lookupFunction(functionName)).isNull()
        assertThat(isAlreadyDefined(functionName)).isFalse()

        defineFunction(functionName, testFunction)

        assertThat(isAlreadyDefined(functionName)).isTrue()
        assertThat(lookupFunction(functionName)).isEqualTo(testFunction)
    }

    @Test
    fun `function table is reset`() {
        val functionName = "testFunction"
        val testFunction = createLispFunction()
        defineFunction(functionName, testFunction)

        resetFunctionTable()

        assertThat(lookupFunction(functionName)).isNull()
        assertThat(isAlreadyDefined(functionName)).isFalse()
    }

    @Test
    fun `reset function table with custom built-ins`() {
        val goodBuiltIns = setOf(GoodFunction::class.java)

        resetFunctionTable(goodBuiltIns)

        assertThat(isAlreadyDefined("GOOD")).isTrue()
        assertThat(lookupFunction("GOOD")).isNotNull()
    }

    @Test
    fun `unable to initialize a built-in function`() {
        val badBuiltIns = setOf(BadFunction::class.java)

        assertThrows(LispFunctionInstantiationException::class.java) { resetFunctionTable(badBuiltIns) }
    }

    @Test
    fun `LispFunctionInstantiationException is cool`() {
        val e = LispFunctionInstantiationException("Bad")

        assertThat(e.message).isNotEmpty()
        assertThat(e.severity).isEqualTo(Severity.CRITICAL)
    }

    @Test
    fun `built-in without a name doesn't throw an exception`() {
        val namelessBuiltins = setOf(UglyFunction::class.java)

        resetFunctionTable(namelessBuiltins)

        assertThat(isAlreadyDefined("UGLY")).isFalse()
        assertThat(lookupFunction("UGLY")).isNull()
    }
}
