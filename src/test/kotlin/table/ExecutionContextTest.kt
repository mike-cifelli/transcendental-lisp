package table

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import sexpression.Nil
import sexpression.Symbol.Companion.T
import table.SymbolTable.NullSymbolTable
import testutil.LispTestInstance

@LispTestInstance
class ExecutionContextTest {

    @BeforeEach
    fun setUp() {
        ExecutionContext.clearContext()
    }

    @AfterEach
    fun tearDown() {
        ExecutionContext.clearContext()
    }

    @Test
    fun `assign a new scope`() {
        val scope = SymbolTable()
        ExecutionContext.scope = scope

        assertThat(ExecutionContext.scope).isEqualTo(scope)
    }

    @Test
    fun `clear the context`() {
        val scope = SymbolTable()
        ExecutionContext.scope = scope

        assertThat(ExecutionContext.scope).isEqualTo(scope)
        ExecutionContext.clearContext()
        assertThat(ExecutionContext.scope).isNotEqualTo(scope)
        assertThat(ExecutionContext.scope.parent).isEqualTo(NullSymbolTable)
    }

    @Test
    fun `lookup a variable`() {
        ExecutionContext.scope["test"] = T

        assertThat(ExecutionContext.lookupSymbolValue("test")).isEqualTo(T)
    }

    @Test
    fun `lookup a local variable`() {
        val scope = SymbolTable(ExecutionContext.scope)

        scope["local"] = T
        ExecutionContext.scope = scope

        assertThat(ExecutionContext.lookupSymbolValue("local")).isEqualTo(T)
    }

    @Test
    fun `lookup a global variable`() {
        val global = ExecutionContext.scope
        val scope1 = SymbolTable(global)
        val scope2 = SymbolTable(scope1)
        val scope3 = SymbolTable(scope2)
        ExecutionContext.scope["global"] = T
        ExecutionContext.scope = scope3

        assertThat(ExecutionContext.lookupSymbolValue("global")).isEqualTo(T)
    }

    @Test
    fun `lookup a shadowed variable`() {
        val scope = SymbolTable(ExecutionContext.scope)

        scope["shadowed"] = Nil
        ExecutionContext.scope["shadowed"] = T
        ExecutionContext.scope = scope

        assertThat(ExecutionContext.lookupSymbolValue("shadowed")).isEqualTo(Nil)
    }

    @Test
    fun `restore the global context`() {
        val global = ExecutionContext.scope
        val scope1 = SymbolTable(global)
        val scope2 = SymbolTable(scope1)
        val scope3 = SymbolTable(scope2)
        ExecutionContext.scope = scope3

        assertThat(ExecutionContext.scope.isGlobal()).isFalse()
        ExecutionContext.restoreGlobalScope()
        assertThat(ExecutionContext.scope.isGlobal()).isTrue()
        assertThat(ExecutionContext.scope).isEqualTo(global)
    }
}
