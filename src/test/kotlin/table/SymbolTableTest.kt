package table

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import sexpression.Nil
import sexpression.Symbol.Companion.T
import testutil.LispTestInstance

@LispTestInstance
class SymbolTableTest {

    private lateinit var symbolTable: SymbolTable

    @BeforeEach
    fun setUp() {
        symbolTable = SymbolTable()
    }

    @Test
    fun `lookup a symbol that does not exist`() {
        assertThat(symbolTable.contains("symbol")).isFalse()
    }

    @Test
    fun `lookup a symbol that exists`() {
        symbolTable["symbol"] = T

        assertThat(symbolTable.contains("symbol")).isTrue()
    }

    @Test
    fun `get the value of a symbol`() {
        symbolTable["symbol"] = T

        assertThat(symbolTable["symbol"]).isEqualTo(T)
    }

    @Test
    fun `redefine the value of a symbol`() {
        symbolTable["symbol"] = T
        symbolTable["symbol"] = Nil

        assertThat(symbolTable["symbol"]).isEqualTo(Nil)
    }

    @Test
    fun `check the value of a parent table`() {
        val childTable = SymbolTable(symbolTable)

        assertThat(childTable.parent).isEqualTo(symbolTable)
    }

    @Test
    fun `lookup a symbol in a parent table`() {
        symbolTable["symbol"] = T
        val childTable = SymbolTable(symbolTable)
        val parentTable = childTable.parent

        assertThat(parentTable?.get("symbol")).isEqualTo(T)
    }
}
