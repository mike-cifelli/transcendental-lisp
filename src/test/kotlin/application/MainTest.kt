package application

import application.LispMain.Companion.ANSI_GREEN
import application.LispMain.Companion.ANSI_PURPLE
import application.LispMain.Companion.ANSI_RESET
import com.googlecode.lanterna.input.KeyType.Enter
import com.googlecode.lanterna.terminal.virtual.DefaultVirtualTerminal
import environment.RuntimeEnvironment
import interpreter.LispInterpreterBuilder
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.contrib.java.lang.system.ExpectedSystemExit
import org.junit.contrib.java.lang.system.SystemErrRule
import org.junit.contrib.java.lang.system.SystemOutRule
import terminal.TerminalConfiguration
import terminal.VirtualTerminalInteractor
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.text.MessageFormat.format
import java.util.concurrent.CountDownLatch

@LispTestInstance
class MainTest : SymbolAndFunctionCleaner() {

    companion object {
        private val FILE = MainTest::class.java.getResource("file.lisp").file
    }

    private lateinit var latch: CountDownLatch

    private fun systemOutLog() = systemOutRule.logWithNormalizedLineSeparator
    private fun systemErrLog() = systemErrRule.logWithNormalizedLineSeparator
    private fun expectedGreeting() = format(LispMain.GREETING, javaClass.`package`.implementationVersion)

    @Rule
    @JvmField
    val exit: ExpectedSystemExit = ExpectedSystemExit.none()

    @Rule
    @JvmField
    val systemErrRule: SystemErrRule = SystemErrRule().enableLog().mute()

    @Rule
    @JvmField
    val systemOutRule: SystemOutRule = SystemOutRule().enableLog().mute()

    private fun runInterpreterWithFile(fileName: String) {
        val configuration = TerminalConfiguration()
        configuration.setInputPair(PipedOutputStream(), PipedInputStream())
        configuration.setOutputPair(PipedOutputStream(), PipedInputStream())
        configuration.terminal = DefaultVirtualTerminal()
        val main = LispMain(configuration)

        main.runWithFile(fileName)
    }

    private fun runInterpreterAndGetInteractor(): VirtualTerminalInteractor {
        val terminal = VirtualTerminalInteractor()
        latch = CountDownLatch(1)

        Thread {
            try {
                val main = LispMain(terminal.configuration)
                main.runInteractive()
            } finally {
                latch.countDown()
            }
        }.start()

        return terminal
    }

    private fun waitForInterpreterToShutdown() {
        try {
            latch.await()
        } catch (ignored: InterruptedException) {
        }
    }

    override fun additionalSetUp() {
        RuntimeEnvironment.reset()
        LispInterpreterBuilder.reset()
    }

    override fun additionalTearDown() {
        RuntimeEnvironment.reset()
        LispInterpreterBuilder.reset()
    }

    @Test
    fun `bad file displays correct error message`() {
        val expectedMessage = "[critical] bad.lisp (No such file or directory)"

        exit.expectSystemExitWithStatus(1)
        exit.checkAssertionAfterwards {
            assertEquals("$ANSI_PURPLE$expectedMessage$ANSI_RESET\n", systemErrLog())
            assertEquals("", systemOutLog())
        }

        runInterpreterWithFile("bad.lisp")
    }

    @Test
    fun `interpret file prints the decorated last value only`() {
        runInterpreterWithFile(FILE)

        assertEquals("", systemErrLog())
        assertEquals("${ANSI_GREEN}RADISH$ANSI_RESET\n\n", systemOutLog())
    }

    @Test
    fun `run interactive interpreter`() {
        val terminal = runInterpreterAndGetInteractor()

        terminal.waitForPrompt()
        terminal.enterCharacters("'hi")
        terminal.pressKey(Enter)
        terminal.waitForPrompt()
        terminal.assertCursorPosition(2, 5)
        terminal.assertScreenText(expectedGreeting(), " ", "~ 'hi ", " ", "HI ", "~  ")
        terminal.enterControlCharacter('d')
        terminal.assertInputStreamClosed()

        waitForInterpreterToShutdown()
    }
}
