package sexpression

import function.UserDefinedFunction
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber.InvalidNumberException
import testutil.LispTestInstance
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.makeList
import java.math.BigInteger
import java.util.Locale

@LispTestInstance
class SExpressionTest {

    private fun assertSExpressionMatchesString(expected: String, sExpression: SExpression) {
        assertThat(sExpression.toString()).isEqualTo(expected)
    }

    @Test
    fun `nil to string`() {
        val input = "NIL"

        assertSExpressionMatchesString(input, Nil)
    }

    @Test
    fun `number to string`() {
        val input = "12"

        assertSExpressionMatchesString(input, LispNumber(input))
    }

    @Test
    fun `string to string`() {
        val input = "\"hi\""

        assertSExpressionMatchesString(input, LispString(input))
    }

    @Test
    fun `symbol to string`() {
        val input = "symbol"

        assertSExpressionMatchesString(input.toUpperCase(Locale.ROOT), Symbol(input))
    }

    @Test
    fun `simple cons to string`() {
        val expected = "(1)"
        val cons = makeList(LispNumber("1"))

        assertSExpressionMatchesString(expected, cons)
    }

    @Test
    fun `complex cons to string`() {
        val expected = "(1 A \"string\")"
        val list = makeList(LispNumber("1"), Symbol("a"), LispString("\"string\""))

        assertSExpressionMatchesString(expected, list)
    }

    @Test
    fun `improper list to string`() {
        val expected = "(A . B)"
        val list = Cons(Symbol("A"), Symbol("B"))

        assertSExpressionMatchesString(expected, list)
    }

    @Test
    fun `lambda expression to string`() {
        val expected = "(LAMBDA)"
        val lambda = LambdaExpression(makeList(Symbol("lambda")),
                                      UserDefinedFunction("", Nil, Nil))

        assertSExpressionMatchesString(expected, lambda)
    }

    @Test
    fun `get lambda expression from lambda`() {
        val expected = "(LAMBDA)"
        val lambda = LambdaExpression(makeList(Symbol("lambda")),
                                      UserDefinedFunction("", Nil, Nil))

        assertSExpressionMatchesString(expected, lambda.lambdaExpression)
    }

    @Test
    fun `get function from lambda`() {
        val expected = "(LAMBDA)"
        val function = UserDefinedFunction(expected, Nil, Nil)
        val lambda = LambdaExpression(makeList(Symbol("lambda")), function)

        assertThat(lambda.function).isEqualTo(function)
    }

    @Test
    fun `first of nil is nil`() {
        assertThat(Nil.first).isEqualTo(Nil)
    }

    @Test
    fun `rest of nil is nil`() {
        assertThat(Nil.rest).isEqualTo(Nil)
    }

    @Test
    fun `cannot change first of nil`() {
        val nil = Nil
        nil.first = LispNumber("2")

        assertThat(nil.first).isEqualTo(Nil)
    }

    @Test
    fun `cannot change rest of nil`() {
        val nil = Nil
        nil.rest = LispNumber("2")

        assertThat(nil.rest).isEqualTo(Nil)
    }

    @Test
    fun `number value`() {
        val value = BigInteger("12")
        val number = LispNumber(value.toString())

        assertThat(number.value).isEqualTo(value)
    }

    @Test
    fun `invalid number text throws an exception`() {
        assertThrows(InvalidNumberException::class.java) { LispNumber("a") }
    }

    @Test
    fun `InvalidNumberException is cool`() {
        try {
            LispNumber("a")
        } catch (e: InvalidNumberException) {
            assertIsErrorWithMessage(e)
        }
    }

    @Test
    fun `lisp number constants are accurate`() {
        assertThat(LispNumber.ZERO.value).isEqualTo(BigInteger.ZERO)
        assertThat(LispNumber.ONE.value).isEqualTo(BigInteger.ONE)
    }

    @Test
    fun `back tick expression to string`() {
        val expected = "`(TEST)"
        val backTick = BackquoteExpression(makeList(Symbol("TEST")))

        assertSExpressionMatchesString(expected, backTick)
    }

    @Test
    fun `comma expression to string`() {
        val expected = ",A"
        val comma = CommaExpression(Symbol("A"))

        assertSExpressionMatchesString(expected, comma)
    }

    @Test
    fun `at sign expression to string`() {
        val expected = "@A"
        val atSign = AtSignExpression(Symbol("A"))

        assertSExpressionMatchesString(expected, atSign)
    }

    @Test
    fun `complex back tick expression to string`() {
        val expected = "`(LIST ,A ,@B)"
        val backTick = BackquoteExpression(makeList(Symbol("LIST"),
                                                    CommaExpression(Symbol("A")),
                                                    CommaExpression(AtSignExpression(Symbol("B")))))

        assertSExpressionMatchesString(expected, backTick)
    }

    @Test
    fun `get expression from back tick expression`() {
        val expression = makeList(Symbol("TEST"))
        val backTick = BackquoteExpression(expression)

        assertSExpressionsMatch(expression, backTick.expression)
    }

    @Test
    fun `get expression from comma expression`() {
        val expression = Symbol("A")
        val comma = CommaExpression(expression)

        assertSExpressionsMatch(expression, comma.expression)
    }

    @Test
    fun `get expression from at sign expression`() {
        val expression = Symbol("A")
        val atSign = AtSignExpression(expression)

        assertSExpressionsMatch(expression, atSign.expression)
    }
}
