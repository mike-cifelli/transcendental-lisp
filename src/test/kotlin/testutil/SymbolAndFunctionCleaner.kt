package testutil

import org.junit.After
import org.junit.Before
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import table.ExecutionContext
import table.FunctionTable

abstract class SymbolAndFunctionCleaner {

    // TODO - clean up
    protected var executionContext: ExecutionContext = ExecutionContext

    @Before
    @BeforeEach
    fun setUp() {
        ExecutionContext.clearContext()
        FunctionTable.resetFunctionTable()
        additionalSetUp()
    }

    @After
    @AfterEach
    fun tearDown() {
        ExecutionContext.clearContext()
        FunctionTable.resetFunctionTable()
        additionalTearDown()
    }

    open fun additionalSetUp() {}
    open fun additionalTearDown() {}
}
