package testutil

import error.LispException
import error.Severity.ERROR
import function.builtin.Eval.Companion.eval
import org.assertj.core.api.Assertions.assertThat
import parser.LispParser
import sexpression.Cons
import sexpression.Nil
import sexpression.SExpression
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.Arrays

object TestUtilities {

    fun createInputStreamFromString(string: String) = ByteArrayInputStream(string.toByteArray())

    fun createIOExceptionThrowingInputStream() = object : InputStream() {

        override fun read(): Int {
            throw IOException("read()")
        }

        override fun close() {
            throw IOException("close()")
        }
    }

    fun createIOExceptionThrowingOutputStream() = object : OutputStream() {

        override fun write(b: ByteArray) {
            throw IOException("write(byte[])")
        }

        override fun flush() {
            throw IOException("flush()")
        }

        override fun close() {
            throw IOException("close()")
        }

        override fun write(arg0: Int) {
            throw IOException("write(int)")
        }
    }

    fun evaluateString(input: String): SExpression = eval(parseString(input))

    fun parseString(input: String): SExpression {
        val stringInputStream = TestUtilities.createInputStreamFromString(input)

        return LispParser(stringInputStream, "testFile").nextSExpression()
    }

    fun makeList(vararg expressionList: SExpression): Cons {
        if (expressionList.isEmpty())
            return Nil

        val rest = makeList(*Arrays.copyOfRange(expressionList, 1, expressionList.size))

        return Cons(expressionList[0], rest)
    }

    fun assertSExpressionsMatch(expected: SExpression, actual: SExpression) {
        assertThat(actual.toString()).isEqualTo(expected.toString())
    }

    fun assertSExpressionsDoNotMatch(unexpected: SExpression, actual: SExpression) {
        assertThat(actual.toString()).isNotEqualTo(unexpected.toString())
    }

    fun assertIsErrorWithMessage(e: LispException) {
        assertThat(e.severity).isEqualTo(ERROR)
        assertThat(e.message).isNotEmpty()
    }
}
