package testutil

import org.assertj.core.api.Assertions.assertThat
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol.Companion.T

object TypeAssertions {

    fun assertList(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isFalse()
        assertThat(sExpression.isCons).isTrue()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isTrue()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertNil(sExpression: SExpression) {
        assertThat(sExpression).isEqualTo(Nil)

        assertThat(sExpression.isAtom).isTrue()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isTrue()
        assertThat(sExpression.isNull).isTrue()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isTrue()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertNumber(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isTrue()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isTrue()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertString(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isTrue()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isTrue()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertSymbol(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isTrue()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isTrue()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertT(sExpression: SExpression) {
        assertThat(sExpression).isEqualTo(T)
    }

    fun assertBackTickExpression(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isFalse()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isTrue()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertCommaExpression(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isFalse()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isTrue()
        assertThat(sExpression.isAtSign).isFalse()
    }

    fun assertAtSignExpression(sExpression: SExpression) {
        assertThat(sExpression.isAtom).isFalse()
        assertThat(sExpression.isCons).isFalse()
        assertThat(sExpression.isFunction).isFalse()
        assertThat(sExpression.isList).isFalse()
        assertThat(sExpression.isNull).isFalse()
        assertThat(sExpression.isNumber).isFalse()
        assertThat(sExpression.isString).isFalse()
        assertThat(sExpression.isSymbol).isFalse()
        assertThat(sExpression.isBackquote).isFalse()
        assertThat(sExpression.isComma).isFalse()
        assertThat(sExpression.isAtSign).isTrue()
    }
}
