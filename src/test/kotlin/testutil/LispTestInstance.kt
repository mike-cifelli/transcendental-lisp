package testutil

import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import kotlin.annotation.AnnotationTarget.CLASS

@Target(CLASS)
@TestInstance(PER_CLASS)
annotation class LispTestInstance
