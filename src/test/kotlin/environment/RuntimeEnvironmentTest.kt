package environment

import error.ErrorManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

@LispTestInstance
class RuntimeEnvironmentTest {

    companion object {
        private const val TERMINATED_SUCCESSFULLY = "TERMINATED_SUCCESSFULLY"
        private const val TERMINATED_EXCEPTIONALLY = "TERMINATED_EXCEPTIONALLY"
    }

    private val indicatorSet = mutableSetOf<String>()

    @BeforeEach
    fun setUp() {
        indicatorSet.clear()
        RuntimeEnvironment.reset()
    }

    @AfterEach
    fun tearDown() {
        RuntimeEnvironment.reset()
    }

    @Test
    fun `assign an input name`() {
        RuntimeEnvironment.inputName = "test"

        assertThat(RuntimeEnvironment.inputName).isEqualTo("test")
    }

    @Test
    fun `assign input`() {
        RuntimeEnvironment.input = System.`in`

        assertThat(RuntimeEnvironment.input).isEqualTo(System.`in`)
    }

    @Test
    fun `assign output`() {
        RuntimeEnvironment.output = System.out

        assertThat(RuntimeEnvironment.output).isEqualTo(System.out)
    }

    @Test
    fun `assign error output`() {
        RuntimeEnvironment.errorOutput = System.err

        assertThat(RuntimeEnvironment.errorOutput).isEqualTo(System.err)
    }

    @Test
    fun `assign an error manager`() {
        val errorManager = ErrorManager()
        RuntimeEnvironment.errorManager = errorManager

        assertThat(RuntimeEnvironment.errorManager).isEqualTo(errorManager)
    }

    @Test
    fun `assign a path`() {
        RuntimeEnvironment.path = "testpath/"

        assertThat(RuntimeEnvironment.path).isEqualTo("testpath/")
    }

    @Test
    fun `assign a termination function`() {
        RuntimeEnvironment.terminationFunction = { indicatorSet.add(TERMINATED_SUCCESSFULLY) }
        RuntimeEnvironment.terminateSuccessfully()

        assertThat(indicatorSet).contains(TERMINATED_SUCCESSFULLY)
    }

    @Test
    fun `assign an error termination function`() {
        RuntimeEnvironment.errorTerminationFunction = { indicatorSet.add(TERMINATED_EXCEPTIONALLY) }
        RuntimeEnvironment.terminateExceptionally()

        assertThat(indicatorSet).contains(TERMINATED_EXCEPTIONALLY)
    }

    @Test
    fun `assign a prompt decorator`() {
        RuntimeEnvironment.promptDecorator = { "[$it]" }

        assertThat(RuntimeEnvironment.decoratePrompt("test")).isEqualTo("[test]")
    }

    @Test
    fun `assign a value output decorator`() {
        RuntimeEnvironment.valueOutputDecorator = { "($it)" }

        assertThat(RuntimeEnvironment.decorateValueOutput("test")).isEqualTo("(test)")
    }

    @Test
    fun `assign a warning output decorator`() {
        RuntimeEnvironment.warningOutputDecorator = { "|$it|" }

        assertThat(RuntimeEnvironment.decorateWarningOutput("test")).isEqualTo("|test|")
    }

    @Test
    fun `assign an error output decorator`() {
        RuntimeEnvironment.errorOutputDecorator = { "{$it}" }

        assertThat(RuntimeEnvironment.decorateErrorOutput("test")).isEqualTo("{test}")
    }

    @Test
    fun `assign a critical output decorator`() {
        RuntimeEnvironment.criticalOutputDecorator = { "/$it/" }

        assertThat(RuntimeEnvironment.decorateCriticalOutput("test")).isEqualTo("/test/")
    }

    @Test
    fun `reset works`() {
        RuntimeEnvironment.inputName = "test"
        RuntimeEnvironment.input = System.`in`
        RuntimeEnvironment.output = System.out
        RuntimeEnvironment.errorOutput = System.err
        RuntimeEnvironment.errorManager = ErrorManager()
        RuntimeEnvironment.path = "testpath/"
        RuntimeEnvironment.terminationFunction = { indicatorSet.add(TERMINATED_SUCCESSFULLY) }
        RuntimeEnvironment.errorTerminationFunction = { indicatorSet.add(TERMINATED_EXCEPTIONALLY) }
        RuntimeEnvironment.promptDecorator = { "[$it]" }
        RuntimeEnvironment.valueOutputDecorator = { "($it)" }
        RuntimeEnvironment.warningOutputDecorator = { "|$it|" }
        RuntimeEnvironment.errorOutputDecorator = { "{$it}" }
        RuntimeEnvironment.criticalOutputDecorator = { "/$it/" }
        RuntimeEnvironment.reset()

        assertThat(RuntimeEnvironment.inputName).isNull()
        assertThat(RuntimeEnvironment.input).isNull()
        assertThat(RuntimeEnvironment.output).isNull()
        assertThat(RuntimeEnvironment.errorOutput).isNull()
        assertThat(RuntimeEnvironment.errorManager).isNull()
        assertThat(RuntimeEnvironment.path).isNull()

        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.terminateSuccessfully() }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.terminateExceptionally() }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.decoratePrompt("") }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.decorateValueOutput("") }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.decorateWarningOutput("") }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.decorateErrorOutput("") }
        //        assertThrows(NullPointerException::class.java) { RuntimeEnvironment.decorateCriticalOutput("") }
    }
}
