package terminal

import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import stream.LispIOException
import testutil.LispTestInstance
import java.io.IOException
import java.io.PipedInputStream
import java.io.PipedOutputStream

@LispTestInstance
class TerminalConfigurationTest {

    private lateinit var configuration: TerminalConfiguration

    private fun createIOExceptionThrowingPipedOutputStream() = object : PipedOutputStream() {
        override fun connect(inputStream: PipedInputStream) = throw IOException()
    }

    @BeforeEach
    fun setUp() {
        configuration = TerminalConfiguration()
    }

    @Test
    fun setInputPairThrowsUncheckedException() {
        assertThrows(LispIOException::class.java) {
            configuration.setInputPair(createIOExceptionThrowingPipedOutputStream(), PipedInputStream())
        }
    }

    @Test
    fun setOutputPairThrowsUncheckedException() {
        assertThrows(LispIOException::class.java) {
            configuration.setOutputPair(createIOExceptionThrowingPipedOutputStream(), PipedInputStream())
        }
    }
}
