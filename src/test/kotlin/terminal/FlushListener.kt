package terminal

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.terminal.Terminal
import com.googlecode.lanterna.terminal.virtual.VirtualTerminalListener

class FlushListener : VirtualTerminalListener {

    val lock = Object()

    var flushCount = 0
        get () = synchronized(lock) { field }
        private set

    fun reduceFlushCount(reduction: Int) {
        synchronized(lock) {
            flushCount -= reduction
        }
    }

    override fun onFlush() {
        synchronized(lock) {
            flushCount++
            lock.notify()
        }
    }

    override fun onResized(terminal: Terminal, newSize: TerminalSize) {}
    override fun onBell() {}
    override fun onClose() {}
}