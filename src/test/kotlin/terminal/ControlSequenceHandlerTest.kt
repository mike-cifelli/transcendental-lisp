package terminal

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import stream.SafeInputStream
import terminal.ControlSequence.NullControlSequence
import terminal.ControlSequenceHandler.Companion.isEscape
import terminal.SelectGraphicRendition.GREEN
import terminal.SelectGraphicRendition.PURPLE
import terminal.SelectGraphicRendition.RED
import terminal.SelectGraphicRendition.RESET
import terminal.SelectGraphicRendition.YELLOW
import testutil.LispTestInstance
import testutil.TestUtilities
import util.Characters.EOF

@LispTestInstance
class ControlSequenceHandlerTest {

    private lateinit var handler: ControlSequenceHandler

    private fun readRemaining(input: SafeInputStream): Any {
        var remaining = ""
        var c = input.read()

        while (c != EOF) {
            remaining += c.toChar()
            c = input.read()
        }

        return remaining
    }

    private fun createSafeInputStream(data: String) =
        SafeInputStream(TestUtilities.createInputStreamFromString(data))

    @BeforeEach
    fun setUp() {
        handler = ControlSequenceHandler()
    }

    @Test
    fun isEscapeDetectsNonEscapeCharacter() {
        assertThat(isEscape('x')).isFalse()
    }

    @Test
    fun isEscapeDetectsEscapeCharacter() {
        assertThat(isEscape('\u001b')).isTrue()
    }

    @Test
    fun correctlyParsesControlSequence_LeavesRestOfStreamIntact() {
        val input = createSafeInputStream("[32mdata")
        handler.parse(input)
        assertThat(readRemaining(input)).isEqualTo("data")
    }

    @Test
    fun unterminatedControlSequence_OnlyConsumesFirstNonSequenceCharacter() {
        val input = createSafeInputStream("[32data")
        handler.parse(input)
        assertThat(readRemaining(input)).isEqualTo("ata")
    }

    @Test
    fun malformedControlSequence_OnlyConsumesOneCharacter() {
        val input = createSafeInputStream("32mdata")
        handler.parse(input)
        assertThat(readRemaining(input)).isEqualTo("2mdata")
    }

    @Test
    fun parsedControlSequenceIsCorrectType_EOF() {
        val input = createSafeInputStream("")
        assertThat(handler.parse(input)).isInstanceOf(NullControlSequence::class.java)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_EOF_AfterFirstCharacter() {
        val input = createSafeInputStream("[")
        assertThat(handler.parse(input)).isInstanceOf(NullControlSequence::class.java)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_UnterminatedControlSequence() {
        val input = createSafeInputStream("[data")
        assertThat(handler.parse(input)).isInstanceOf(NullControlSequence::class.java)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_MalformedControlSequence() {
        val input = createSafeInputStream("32mdata")
        assertThat(handler.parse(input)).isInstanceOf(NullControlSequence::class.java)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_SGR_Reset() {
        val input = createSafeInputStream("[0m")
        assertThat(handler.parse(input)).isEqualTo(RESET)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_SGR_Red() {
        val input = createSafeInputStream("[31m")
        assertThat(handler.parse(input)).isEqualTo(RED)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_SGR_Green() {
        val input = createSafeInputStream("[32m")
        assertThat(handler.parse(input)).isEqualTo(GREEN)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_SGR_Yellow() {
        val input = createSafeInputStream("[33m")
        assertThat(handler.parse(input)).isEqualTo(YELLOW)
    }

    @Test
    fun parsedControlSequenceIsCorrectType_SGR_Purple() {
        val input = createSafeInputStream("[35m")
        assertThat(handler.parse(input)).isEqualTo(PURPLE)
    }

    @Test
    fun parseMultipleControlSequences() {
        val input = createSafeInputStream("[35m[32m[0m")
        assertThat(handler.parse(input)).isEqualTo(PURPLE)
        assertThat(handler.parse(input)).isEqualTo(GREEN)
        assertThat(handler.parse(input)).isEqualTo(RESET)
    }
}
