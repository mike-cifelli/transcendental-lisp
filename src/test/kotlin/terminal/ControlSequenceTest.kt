package terminal

import com.googlecode.lanterna.TextColor
import com.googlecode.lanterna.terminal.virtual.DefaultVirtualTerminal
import com.googlecode.lanterna.terminal.virtual.VirtualTerminal
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import terminal.ControlSequence.NullControlSequence
import terminal.SelectGraphicRendition.GREEN
import terminal.SelectGraphicRendition.PURPLE
import terminal.SelectGraphicRendition.RED
import terminal.SelectGraphicRendition.RESET
import terminal.SelectGraphicRendition.YELLOW
import testutil.LispTestInstance
import java.util.HashSet

@LispTestInstance
class ControlSequenceTest {

    private lateinit var indicatorSet: MutableSet<String>

    private fun createTerminalWithIndicators(): VirtualTerminal {
        return object : DefaultVirtualTerminal() {

            override fun resetColorAndSGR() {
                indicatorSet.add("RESET")
            }

            override fun setForegroundColor(color: TextColor) {
                indicatorSet.add(color.toString())
            }
        }
    }

    @BeforeEach
    fun setUp() {
        indicatorSet = HashSet()
    }

    @Test
    fun nullControlSequenceDoesNothing() {
        val nullControlSequence = NullControlSequence()
        val terminal = createTerminalWithIndicators()
        nullControlSequence.applyTo(terminal)
        assertThat(indicatorSet).isEmpty()
    }

    @Test
    fun controlSequenceUpdatesTerminal_SGR_Reset() {
        val terminal = createTerminalWithIndicators()
        RESET.applyTo(terminal)
        assertThat(indicatorSet).contains("RESET")
    }

    @Test
    fun controlSequenceUpdatesTerminal_SGR_Red() {
        val terminal = createTerminalWithIndicators()
        RED.applyTo(terminal)
        assertThat(indicatorSet).contains("RED")
    }

    @Test
    fun controlSequenceUpdatesTerminal_SGR_Green() {
        val terminal = createTerminalWithIndicators()
        GREEN.applyTo(terminal)
        assertThat(indicatorSet).contains("GREEN")
    }

    @Test
    fun controlSequenceUpdatesTerminal_SGR_Yellow() {
        val terminal = createTerminalWithIndicators()
        YELLOW.applyTo(terminal)
        assertThat(indicatorSet).contains("YELLOW")
    }

    @Test
    fun controlSequenceUpdatesTerminal_SGR_Purple() {
        val terminal = createTerminalWithIndicators()
        PURPLE.applyTo(terminal)
        assertThat(indicatorSet).contains("MAGENTA")
    }

    @Test
    fun nullControlSequenceHasCorrectCode() {
        val nullControlSequence = NullControlSequence()
        assertThat(nullControlSequence.code).isEmpty()
    }
}
