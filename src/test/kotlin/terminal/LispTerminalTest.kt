package terminal

import com.googlecode.lanterna.input.KeyType.ArrowDown
import com.googlecode.lanterna.input.KeyType.ArrowLeft
import com.googlecode.lanterna.input.KeyType.ArrowRight
import com.googlecode.lanterna.input.KeyType.ArrowUp
import com.googlecode.lanterna.input.KeyType.Backspace
import com.googlecode.lanterna.input.KeyType.Delete
import com.googlecode.lanterna.input.KeyType.Enter
import com.googlecode.lanterna.input.KeyType.Escape
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import terminal.LispTerminal.Companion.END_OF_SEGMENT
import testutil.LispTestInstance

@LispTestInstance
class LispTerminalTest {

    private lateinit var terminal: VirtualTerminalInteractor

    @BeforeEach
    fun setUp() {
        terminal = VirtualTerminalInteractor()
        terminal.start()
    }

    @AfterEach
    fun tearDown() {
        terminal.stop()
    }

    @Test
    fun leftArrowDoesNotMovePastOrigin() {
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(0, 0)
    }

    @Test
    fun leftArrowWorksAfterEnteringCharacters() {
        terminal.enterCharacters("abc")
        terminal.assertCursorPosition(3, 0)
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(2, 0)
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(1, 0)
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(0, 0)
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(0, 0)
    }

    @Test
    fun leftArrowWorksAcrossRows() {
        terminal.setColumns(5)
        terminal.enterCharacters("123451")
        terminal.assertCursorPosition(1, 1)
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.assertCursorPosition(4, 0)
    }

    @Test
    fun rightArrowDoesNotMovePastEndOfInput() {
        terminal.pressKey(ArrowRight)
        terminal.assertCursorPosition(0, 0)
    }

    @Test
    fun rightArrowWorksAfterMovingLeft() {
        terminal.enterCharacters("12")
        terminal.assertCursorPosition(2, 0)
        terminal.pressKey(ArrowLeft)
        terminal.assertCursorPosition(1, 0)
        terminal.pressKey(ArrowRight)
        terminal.assertCursorPosition(2, 0)
        terminal.pressKey(ArrowRight)
        terminal.assertCursorPosition(2, 0)
    }

    @Test
    fun rightArrowWorksAcrossRow() {
        terminal.setColumns(5)
        terminal.enterCharacters("123451")
        terminal.assertCursorPosition(1, 1)
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.assertCursorPosition(3, 0)
        terminal.pressKeyTimes(ArrowRight, 3)
        terminal.assertCursorPosition(1, 1)
    }

    @Test
    fun characterKeyIsEchoed() {
        terminal.enterCharacter('a')
        terminal.assertCursorPosition(1, 0)
        terminal.assertScreenText("a")
    }

    @Test
    fun characterIsInserted() {
        terminal.enterCharacters("abcd")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.enterCharacter('x')
        terminal.assertScreenText("abxcd")
    }

    @Test
    fun characterIsInserted_PushesInputToNextRow() {
        terminal.setColumns(4)
        terminal.enterCharacters("abcd")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.enterCharacter('x')
        terminal.assertScreenText("abxc", "d   ")
    }

    @Test
    fun backspaceDoesNothingAtOrigin() {
        terminal.pressKey(Backspace)
        terminal.assertCursorPosition(0, 0)
    }

    @Test
    fun backspaceWorksAfterInput() {
        terminal.enterCharacters("12345")
        terminal.pressKeyTimes(Backspace, 2)
        terminal.assertCursorPosition(3, 0)
        terminal.assertScreenText("123   ")
    }

    @Test
    fun backspaceWorksAcrossRow() {
        terminal.setColumns(4)
        terminal.enterCharacters("1234567")
        terminal.pressKeyTimes(Backspace, 5)
        terminal.assertCursorPosition(2, 0)
        terminal.assertScreenText("12  ", "    ")
    }

    @Test
    fun backspaceWorksInMiddleOfInput() {
        terminal.enterCharacters("12345")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.pressKey(Backspace)
        terminal.assertCursorPosition(2, 0)
        terminal.assertScreenText("1245")
    }

    @Test
    fun deleteDoesNothingAtOrigin() {
        terminal.pressKey(Delete)
        terminal.assertCursorPosition(0, 0)
    }

    @Test
    fun deleteDoesNothingAtEndOfInput() {
        terminal.enterCharacters("del")
        terminal.pressKey(Delete)
        terminal.assertCursorPosition(3, 0)
        terminal.assertScreenText("del")
    }

    @Test
    fun deleteWorksAtStartOfInput() {
        terminal.enterCharacters("del")
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.pressKeyTimes(Delete, 3)
        terminal.assertCursorPosition(0, 0)
        terminal.assertScreenText("   ")
    }

    @Test
    fun deleteWorksAcrossRow() {
        terminal.setColumns(4)
        terminal.enterCharacters("delete")
        terminal.pressKeyTimes(ArrowLeft, 5)
        terminal.pressKey(Delete)
        terminal.assertCursorPosition(1, 0)
        terminal.assertScreenText("dlet", "e   ")
    }

    @Test
    fun enterMovesToNextLine() {
        terminal.pressKey(Enter)
        terminal.assertCursorPosition(0, 1)
    }

    @Test
    fun enterWritesLineToPipedStream() {
        terminal.enterCharacters("enter")
        terminal.pressKey(Enter)
        terminal.assertInputWritten("enter\n")
    }

    @Test
    fun enterPressedInMiddleOfInput_WritesEntireLineToPipedStream() {
        terminal.enterCharacters("enter")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.pressKey(Enter)
        terminal.assertInputWritten("enter\n")
    }

    @Test
    fun enterAfterInsertedText_WritesLineToPipedStream() {
        terminal.enterCharacters("enter")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.enterCharacters("||")
        terminal.pressKey(Enter)
        terminal.assertInputWritten("ent||er\n")
    }

    @Test
    fun enterAfterBackspace_WritesLineToPipedStream() {
        terminal.enterCharacters("enter")
        terminal.pressKeyTimes(Backspace, 2)
        terminal.pressKey(Enter)
        terminal.assertInputWritten("ent\n")
    }

    @Test
    fun enterAfterDelete_WritesLineToPipedStream() {
        terminal.enterCharacters("enter")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.pressKeyTimes(Delete, 2)
        terminal.pressKey(Enter)
        terminal.assertInputWritten("ent\n")
    }

    @Test
    fun controlDWorks() {
        terminal.enterCharacters("control-d")
        terminal.enterControlCharacter('d')
        terminal.assertInputStreamClosed()
        terminal.assertInputWritten("control-d\n")
    }

    @Test
    fun controlDWorksInMiddleOfInput() {
        terminal.enterCharacters("control-d")
        terminal.pressKeyTimes(ArrowLeft, 2)
        terminal.enterControlCharacter('d')
        terminal.assertInputStreamClosed()
        terminal.assertInputWritten("control-d\n")
    }

    @Test
    fun escapeDoesNothing() {
        terminal.pressKey(Escape)
        terminal.assertCursorPosition(0, 0)
        terminal.assertInputWritten("")
    }

    @Test
    fun controlQDoesNothing() {
        terminal.enterControlCharacter('q')
        terminal.assertCursorPosition(0, 0)
        terminal.assertInputWritten("")
    }

    @Test
    fun controlEnterDoesNothing() {
        terminal.pressControlKey(Enter)
        terminal.assertCursorPosition(0, 0)
        terminal.assertInputWritten("")
    }

    @Test
    fun outputIsWritten() {
        terminal.produceOutput("output")
        terminal.assertCursorPosition(6, 0)
        terminal.assertScreenText("output")
    }

    @Test
    fun endOfSegmentCharacterIsNotPrinted() {
        terminal.produceOutput("> $END_OF_SEGMENT")
        terminal.assertCursorPosition(2, 0)
        terminal.assertScreenText(">  ")
    }

    @Test
    fun enterTextPastLastLineOfBuffer() {
        terminal.setColumns(3)
        terminal.setRows(2)
        terminal.enterCharacters("01201201")
        terminal.assertCursorPosition(2, 1)
        terminal.assertScreenText("012", "01 ")
    }

    @Test
    fun insertingTextPushesInputPastEndOfBuffer() {
        terminal.setColumns(3)
        terminal.setRows(4)
        terminal.pressKey(Enter)
        terminal.enterCharacters("00011122")
        terminal.pressKeyTimes(ArrowLeft, 4)
        terminal.assertCursorPosition(1, 2)
        terminal.enterCharacters("zz")
        terminal.assertCursorPosition(0, 2)
        terminal.assertScreenText("000", "1zz", "112", "2  ")
    }

    @Test
    fun insertingTextDoesNothingWhenBufferFilled() {
        terminal.setColumns(3)
        terminal.setRows(3)
        terminal.enterCharacters("00011122")
        terminal.pressKeyTimes(ArrowLeft, 4)
        terminal.assertCursorPosition(1, 1)
        terminal.enterCharacters("zz")
        terminal.assertCursorPosition(1, 1)
        terminal.assertScreenText("000", "111", "22 ")
    }

    @Test
    fun appendingTextDoesNothingWhenBufferFilled() {
        terminal.setColumns(3)
        terminal.setRows(3)
        terminal.enterCharacters("000111222333444")
        terminal.assertCursorPosition(2, 2)
        terminal.assertScreenText("000", "111", "22 ")
    }

    @Test
    fun printedOutputToEndOfRow_MovesCursorToNextRow() {
        terminal.setColumns(3)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(0, 1)
    }

    @Test
    fun printedOutputToEndOfBuffer_MovesCursorToNewRow() {
        terminal.setColumns(3)
        terminal.setRows(2)
        terminal.produceOutput("output")
        terminal.assertCursorPosition(0, 1)
        terminal.assertScreenText("put", "   ")
    }

    @Test
    fun outputDoesNotOverwriteInput_AndRedisplaysInput() {
        terminal.setColumns(3)
        terminal.enterCharacters("0123")
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(2, 3)
        terminal.assertScreenText("012", "3ou", "t01", "23 ", "   ")
    }

    @Test
    fun outputEndsOnSecondToLastColumn_MovesToNewRow() {
        terminal.setColumns(3)
        terminal.enterCharacters("01234")
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(2, 4)
        terminal.assertScreenText("012", "34o", "ut ", "012", "34 ")
    }

    @Test
    fun outputEndsOnLastColumn_MovesToNewRow() {
        terminal.setColumns(3)
        terminal.enterCharacters("012345")
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(0, 5)
        terminal.assertScreenText("012", "345", "out", "012", "345", "   ")
    }

    @Test
    fun outputRedisplaysInputAtEndOfBuffer() {
        terminal.setColumns(3)
        terminal.setRows(4)
        terminal.enterCharacters("01234")
        terminal.pressKeyTimes(ArrowLeft, 3)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(2, 3)
        terminal.assertScreenText("34o", "ut ", "012")
    }

    @Test
    fun outputDoesNotOverwriteInput_AfterEnter() {
        terminal.setColumns(3)
        terminal.enterCharacters("01201201")
        terminal.pressKeyTimes(ArrowLeft, 5)
        terminal.pressKey(Enter)
        terminal.produceOutput("out")
        terminal.assertCursorPosition(0, 4)
        terminal.assertScreenText("012", "012", "01 ", "out", "   ")
    }

    @Test
    fun resizeIsHandledGracefully() {
        terminal.enterCharacters("resize")
        terminal.pressKey(Enter)
        terminal.enterCharacters("test")
        terminal.setColumns(3)
        terminal.assertCursorPosition(1, 1)
        terminal.assertScreenText("tes", "t  ")
    }

    @Test
    fun backspaceWorksAfterResize() {
        terminal.enterCharacters("resize")
        terminal.pressKey(Enter)
        terminal.enterCharacters("test")
        terminal.setColumns(3)
        terminal.pressKeyTimes(Backspace, 20)
        terminal.assertCursorPosition(0, 0)
        terminal.assertScreenText("   ", "   ")
    }

    @Test
    fun deleteWorksAfterResize() {
        terminal.enterCharacters("resize")
        terminal.pressKey(Enter)
        terminal.enterCharacters("test")
        terminal.setColumns(3)
        terminal.pressKeyTimes(ArrowLeft, 20)
        terminal.pressKeyTimes(Delete, 20)
        terminal.pressKeyTimes(ArrowRight, 20)
        terminal.assertCursorPosition(0, 0)
        terminal.assertScreenText("   ", "   ")
    }

    @Test
    fun controlSequencesAreNotPrinted() {
        terminal.produceOutput("\u001B[32mcontrol\u001B[0mseq")
        terminal.assertScreenText("controlseq")
    }

    @Test
    fun upArrowWorks() {
        terminal.enterCharacters("(one)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(two)")
        terminal.pressKey(Enter)
        terminal.pressKey(ArrowUp)
        terminal.assertScreenText("(one)", "(two)", "(two)")
        terminal.assertCursorPosition(5, 2)
    }

    @Test
    fun upArrowErasesCurrentLine() {
        terminal.enterCharacters("(one)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(two)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(three)")
        terminal.pressKey(ArrowUp)
        terminal.assertScreenText("(one)", "(two)", "(two)     ")
        terminal.assertCursorPosition(5, 2)
    }

    @Test
    fun upArrowStopsAtFirstLine() {
        terminal.enterCharacters("(one)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(two)")
        terminal.pressKey(Enter)
        terminal.pressKeyTimes(ArrowUp, 5)
        terminal.assertScreenText("(one)", "(two)", "(one)")
        terminal.assertCursorPosition(5, 2)
    }

    @Test
    fun originIsUpdatedWhenPreviousLineMovesPastEndOfBuffer() {
        terminal.setRows(3)
        terminal.setColumns(3)
        terminal.enterCharacters("12345")
        terminal.pressKey(Enter)
        terminal.pressKey(ArrowUp)
        terminal.assertScreenText("45 ", "123", "45 ")
        terminal.pressKeyTimes(ArrowLeft, 10)
        terminal.assertCursorPosition(0, 1)
    }

    @Test
    fun downArrowWorks() {
        terminal.enterCharacters("(one)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(two)")
        terminal.pressKey(Enter)
        terminal.enterCharacters("(three)")
        terminal.pressKey(ArrowUp)
        terminal.pressKey(ArrowDown)
        terminal.assertScreenText("(one)", "(two)", "(three)")
        terminal.assertCursorPosition(7, 2)
    }

    @Test
    fun downArrowStopsAtLastLine() {
        terminal.pressKeyTimes(ArrowDown, 5)
        terminal.assertScreenText(" ")
        terminal.assertCursorPosition(0, 0)
    }
}
