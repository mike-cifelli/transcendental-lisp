package token

import error.Severity.CRITICAL
import file.FilePosition
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

import token.TokenFactory.BadCharacterException
import token.TokenFactory.EmptyTokenTextException

@LispTestInstance
class TokenFactoryTest {

    private lateinit var tokenFactory: TokenFactory
    private lateinit var testPosition: FilePosition

    private fun createToken(text: String) = tokenFactory.createToken(text, testPosition)

    @BeforeEach
    fun setUp() {
        tokenFactory = TokenFactoryImpl()
        testPosition = FilePosition("testFile", 0, 0)
    }

    @Test
    fun `create EOF token`() {
        assertThat(tokenFactory.createEofToken(testPosition)).isInstanceOf(Eof::class.java)
    }

    @Test
    fun `create left parenthesis`() {
        assertThat(createToken("(")).isInstanceOf(LeftParenthesis::class.java)
    }

    @Test
    fun `create right parenthesis`() {
        assertThat(createToken(")")).isInstanceOf(RightParenthesis::class.java)
    }

    @Test
    fun `create quote mark`() {
        assertThat(createToken("'")).isInstanceOf(QuoteMark::class.java)
    }

    @Test
    fun `create number`() {
        assertThat(createToken("987")).isInstanceOf(Number::class.java)
    }

    @Test
    fun `create prefixed number`() {
        assertThat(createToken("-987")).isInstanceOf(Number::class.java)
    }

    @Test
    fun `create identifier`() {
        assertThat(createToken("identifier")).isInstanceOf(Identifier::class.java)
    }

    @Test
    fun `create prefixed identifier`() {
        assertThat(createToken("-identifier")).isInstanceOf(Identifier::class.java)
    }

    @Test
    fun `create string`() {
        assertThat(createToken("\"string\"")).isInstanceOf(QuotedString::class.java)
    }

    @Test
    fun `empty string throws exception`() {
        assertThrows(EmptyTokenTextException::class.java) { createToken("") }
    }

    @Test
    fun `EmptyTokenTextException is cool`() {
        try {
            createToken("")
        } catch (e: EmptyTokenTextException) {
            assertThat(e.message).isNotEmpty()
            assertThat(e.severity).isEqualTo(CRITICAL)
        }
    }

    @Test
    fun `bad character throws exception`() {
        assertThrows(BadCharacterException::class.java) { createToken("[abc]") }
    }

    @Test
    fun `create back tick`() {
        assertThat(createToken("`")).isInstanceOf(Backquote::class.java)
    }

    @Test
    fun `create comma`() {
        assertThat(createToken(",")).isInstanceOf(Comma::class.java)
    }

    @Test
    fun `create at sign`() {
        assertThat(createToken("@")).isInstanceOf(AtSign::class.java)
    }
}
