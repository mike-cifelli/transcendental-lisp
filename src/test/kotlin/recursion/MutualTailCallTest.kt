package recursion

import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import recursion.MutualTailCalls.terminalValue
import testutil.LispTestInstance

@LispTestInstance
class MutualTailCallTest {

    @Test
    fun `tailCall does not support result`() {
        val tailCall = object : MutualTailCall<Nothing?> {
            override fun apply() = terminalValue(null)
        }

        assertThrows(UnsupportedOperationException::class.java) { tailCall.result() }
    }

    @Test
    fun `done does not support apply`() {
        assertThrows(UnsupportedOperationException::class.java) { terminalValue(null).apply() }
    }
}
