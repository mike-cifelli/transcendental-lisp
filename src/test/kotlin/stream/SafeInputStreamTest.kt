package stream

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.TestUtilities.createIOExceptionThrowingInputStream
import testutil.TestUtilities.createInputStreamFromString
import util.Characters.EOF

@LispTestInstance
class SafeInputStreamTest {

    private lateinit var safe: SafeInputStream
    private lateinit var safeWithException: SafeInputStream

    @BeforeEach
    fun setUp() {
        safe = SafeInputStream(createInputStreamFromString("a"))
        safeWithException = SafeInputStream(createIOExceptionThrowingInputStream())
    }

    @Test
    fun `read works`() {
        assertThat(safe.read().toChar()).isEqualTo('a')
        assertThat(safe.read()).isEqualTo(EOF)
    }

    @Test
    fun `close works`() {
        safe.close()
    }

    @Test
    fun `read throws correct exception`() {
        assertThrows(LispIOException::class.java) { safeWithException.read() }
    }

    @Test
    fun `close throws correct exception`() {
        assertThrows(LispIOException::class.java) { safeWithException.close() }
    }
}
