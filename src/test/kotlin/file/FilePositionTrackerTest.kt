package file

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

@LispTestInstance
class FilePositionTrackerTest {

    companion object {
        const val FILE_NAME = "testFile"
    }

    private lateinit var trackerUnderTest: FilePositionTracker

    private fun createFilePosition(lineNumber: Int, columnNumber: Int) =
        FilePosition(FILE_NAME, lineNumber, columnNumber)

    private fun assertTrackerPositionEquals(expectedPosition: FilePosition) {
        assertThat(trackerUnderTest.currentPosition()).isEqualTo(expectedPosition)
    }

    @BeforeEach
    fun setUp() {
        trackerUnderTest = FilePositionTracker(FILE_NAME)
    }

    @Test
    fun `no movement returns the initial position`() {
        assertTrackerPositionEquals(createFilePosition(1, 0))
    }

    @Test
    fun `advancing one column returns correct position`() {
        trackerUnderTest.incrementColumn()

        assertTrackerPositionEquals(createFilePosition(1, 1))
    }

    @Test
    fun `advancing one line returns correct position`() {
        trackerUnderTest.incrementLine()

        assertTrackerPositionEquals(createFilePosition(2, 0))
    }

    @Test
    fun `advancing one line resets column number`() {
        trackerUnderTest.incrementColumn()
        trackerUnderTest.incrementLine()

        assertTrackerPositionEquals(createFilePosition(2, 0))
    }
}
