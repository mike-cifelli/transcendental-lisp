package function

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.UserDefinedFunction.IllegalKeywordRestPositionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.LispNumber
import sexpression.Nil
import sexpression.Symbol
import testutil.LispTestInstance
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch

@LispTestInstance
class UserDefinedFunctionTest {

    companion object {
        private const val FUNCTION_NAME = "TEST"
    }

    private fun createNoArgumentFunctionThatReturnsNil() =
        UserDefinedFunction(FUNCTION_NAME, Nil, Cons(Nil, Nil))

    private fun createOneArgumentFunctionThatReturnsArgument() =
        UserDefinedFunction(FUNCTION_NAME, Cons(Symbol("X"), Nil), Cons(Symbol("X"), Nil))

    @Test
    fun `call function that returns nil`() {
        val function = createNoArgumentFunctionThatReturnsNil()

        assertThat(function.call(Nil)).isEqualTo(Nil)
    }

    @Test
    fun `function has correct lambda expression`() {
        val function = createNoArgumentFunctionThatReturnsNil()
        val expected = Cons(Symbol(FUNCTION_NAME), Cons(Nil, Cons(Nil, Nil)))

        assertSExpressionsMatch(expected, function.lambdaExpression)
    }

    @Test
    fun `function returns correct value`() {
        val function = createOneArgumentFunctionThatReturnsArgument()
        val argument = LispNumber("23")
        val argumentList = Cons(argument, Nil)

        assertSExpressionsMatch(argument, function.call(argumentList))
    }

    @Test
    fun `function throws exception with too many arguments`() {
        val function = createOneArgumentFunctionThatReturnsArgument()
        val argument = LispNumber("23")
        val argumentList = Cons(argument, Cons(argument, Nil))

        assertThrows(TooManyArgumentsException::class.java) {
            function.call(argumentList)
        }
    }

    @Test
    fun `function throws exception with too few arguments`() {
        val function = createOneArgumentFunctionThatReturnsArgument()

        assertThrows(TooFewArgumentsException::class.java) {
            function.call(Nil)
        }
    }

    @Test
    fun `IllegalKeywordRestPositionException is cool`() {
        assertIsErrorWithMessage(IllegalKeywordRestPositionException(FUNCTION_NAME, Nil))
    }
}
