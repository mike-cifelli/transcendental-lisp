package function

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.Nil
import testutil.LispTestInstance

@LispTestInstance
class LispSpecialFunctionTest {

    @Test
    fun `arguments are not evaluated`() {
        val lispFunction = object : LispSpecialFunction() {
            override fun call(argumentList: Cons) = Nil
        }

        assertThat(lispFunction.isArgumentListEvaluated).isFalse()
    }
}
