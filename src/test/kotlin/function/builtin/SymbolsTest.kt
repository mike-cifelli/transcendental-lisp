package function.builtin

import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class SymbolsTest : SymbolAndFunctionCleaner() {

    @Test
    fun noSymbols() {
        assertSExpressionsMatch(evaluateString("'(nil)"), evaluateString("(symbols)"))
    }

    @Test
    fun globalSymbol() {
        evaluateString("(setq x 10)")
        assertSExpressionsMatch(evaluateString("'(((x 10)))"), evaluateString("(symbols)"))
    }

    @Test
    fun multipleSymbolsSorted() {
        evaluateString("(setq x 10)")
        evaluateString("(setq a 20)")
        evaluateString("(setq y 30)")
        evaluateString("(setq w 40)")
        evaluateString("(setq e 50)")

        assertSExpressionsMatch(evaluateString("'(((a 20) (e 50) (w 40) (x 10) (y 30)))"), evaluateString("(symbols)"))
    }

    @Test
    fun fullExecutionContext() {
        evaluateString("(setq x 10)")
        evaluateString("(setq y 30)")

        assertSExpressionsMatch(evaluateString("'(((x 10) (y 30)) ((a 100) (q 99) (z nil)))"),
                                evaluateString("(let ((q 99) (a 100) (z)) (symbols))"))
    }

    @Test
    fun updateSymbolInLet() {
        evaluateString("(setq x 10)")
        evaluateString("(setq y 30)")

        assertSExpressionsMatch(evaluateString("'(((x 10) (y 30)) ((q 99) (x 1) (z 2)))"),
                                evaluateString("(let ((q 99) (x 100) (z 2)) (setq x 1) (symbols))"))
    }

    @Test
    fun symbolsWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(symbols 1)")
        }
    }
}
