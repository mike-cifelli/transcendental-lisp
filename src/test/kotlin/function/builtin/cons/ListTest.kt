package function.builtin.cons

import function.builtin.cons.List.Companion.makeList
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class ListTest : SymbolAndFunctionCleaner() {

    @Test
    fun listWithNoArguments() {
        val input = "(list)"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun listWithOneArgument() {
        val input = "(list 1)"

        assertSExpressionsMatch(parseString("(1)"), evaluateString(input))
    }

    @Test
    fun listWithTwoArguments() {
        val input = "(list 2 3)"

        assertSExpressionsMatch(parseString("(2 3)"), evaluateString(input))
    }

    @Test
    fun listWithManyArguments() {
        val input = "(list 'm 'a 'n 'y 'a 'r 'g 's)"

        assertSExpressionsMatch(parseString("(m a n y a r g s)"), evaluateString(input))
    }

    @Test
    fun listWithOneListArgument() {
        val input = "(list '(1))"

        assertSExpressionsMatch(parseString("((1))"), evaluateString(input))
    }

    @Test
    fun listWithManyListArguments() {
        val input = "(list '(1) '(2 3) ())"

        assertSExpressionsMatch(parseString("((1) (2 3) ())"), evaluateString(input))
    }

    @Test
    fun staticMakeList() {
        assertSExpressionsMatch(parseString("(22)"), makeList(parseString("22")))
    }
}
