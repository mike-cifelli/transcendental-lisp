package function.builtin.cons

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.Symbol
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class ConstructTest : SymbolAndFunctionCleaner() {

    @Test
    fun consWithNilValues() {
        val input = "(cons () nil)"

        assertSExpressionsMatch(parseString("(())"), evaluateString(input))
    }

    @Test
    fun consWithTwoSymbols() {
        val input = "(cons 'a 'b)"

        assertSExpressionsMatch(Cons(Symbol("A"), Symbol("B")), evaluateString(input))
    }

    @Test
    fun consWithListAsRest() {
        val input = "(cons 1 '(2 3))"

        assertSExpressionsMatch(parseString("(1 2 3)"), evaluateString(input))
    }

    @Test
    fun consWithTwoLists() {
        val input = "(cons '(1 2) '(3 4))"

        assertSExpressionsMatch(parseString("((1 2) 3 4)"), evaluateString(input))
    }

    @Test
    fun consWithList() {
        val input = "(cons nil '(2 3))"

        assertSExpressionsMatch(parseString("(nil 2 3)"), evaluateString(input))
    }

    @Test
    fun consWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(cons 1 2 3)")
        }
    }

    @Test
    fun consWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(cons 1)")
        }
    }
}
