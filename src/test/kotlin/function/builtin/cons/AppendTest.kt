package function.builtin.cons

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class AppendTest : SymbolAndFunctionCleaner() {

    @Test
    fun appendNil() {
        val input = "(append () ())"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun appendNilToList() {
        val input = "(append () '(1 2 3))"

        assertSExpressionsMatch(parseString("(1 2 3)"), evaluateString(input))
    }

    @Test
    fun appendListToNil() {
        val input = "(append '(1 2 3) ())"

        assertSExpressionsMatch(parseString("(1 2 3)"), evaluateString(input))
    }

    @Test
    fun appendTwoLists() {
        val input = "(append '(1 2 3) '(4 5 6))"

        assertSExpressionsMatch(parseString("(1 2 3 4 5 6)"), evaluateString(input))
    }

    @Test
    fun appendMakesCopyOfFirstList() {
        evaluateString("(setq x '(1 2 3))")
        evaluateString("(setq y '(4 5 6))")
        evaluateString("(append x y)")

        assertSExpressionsMatch(parseString("(1 2 3)"), evaluateString("x"))
    }

    @Test
    fun appendAllowsDottedSecondList() {
        val input = "(append '() (cons 3 4))"
        assertSExpressionsMatch(evaluateString("(cons 3 4)"), evaluateString(input))
    }

    @Test
    fun appendWithDottedFirstList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(append (cons 1 2) '(3 4))")
        }
    }

    @Test
    fun appendWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(append () () ())")
        }
    }

    @Test
    fun appendWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(append ())")
        }
    }

    @Test
    fun appendWithBadArgumentType() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(append 1 '(2))")
        }
    }
}
