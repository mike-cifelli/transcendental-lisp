package function.builtin.cons

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class LengthTest : SymbolAndFunctionCleaner() {

    @Test
    fun lengthOfNil() {
        val input = "(length '())"

        assertSExpressionsMatch(parseString("0"), evaluateString(input))
    }

    @Test
    fun lengthOfListOfOneElement() {
        val input = "(length '(1))"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun lengthOfListOfManyElements() {
        val input = "(length '(1 2 3 4 5))"

        assertSExpressionsMatch(parseString("5"), evaluateString(input))
    }

    @Test
    fun lengthWithNonList() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(length 'x)")
        }
    }

    @Test
    fun lengthWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(length '(1 2) '(1 2))")
        }
    }

    @Test
    fun lengthWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(length)")
        }
    }

    @Test
    fun lengthWithDottedList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(length (cons 1 2))")
        }
    }
}
