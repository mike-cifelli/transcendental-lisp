package function.builtin.cons

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class FirstTest : SymbolAndFunctionCleaner() {

    @Test
    fun firstOfNil() {
        val input = "(first nil)"

        assertSExpressionsMatch(parseString("()"), evaluateString(input))
    }

    @Test
    fun firstOfList() {
        val input = "(first '(1 2 3))"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun carOfList() {
        val input = "(car '(1 2 3))"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun nestedFirstOfList() {
        val input = "(first (first '((1 2) 3)))"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun firstOfSymbol() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(first 'x)")
        }
    }

    @Test
    fun firstWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(first '(1 2) '(1 2) \"oh\")")
        }
    }

    @Test
    fun firstWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(first)")
        }
    }
}
