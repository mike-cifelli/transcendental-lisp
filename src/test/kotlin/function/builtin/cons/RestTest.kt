package function.builtin.cons

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class RestTest : SymbolAndFunctionCleaner() {

    @Test
    fun restOfNil() {
        val input = "(rest nil)"

        assertSExpressionsMatch(parseString("()"), evaluateString(input))
    }

    @Test
    fun restOfList() {
        val input = "(rest '(1 2 3))"

        assertSExpressionsMatch(parseString("(2 3)"), evaluateString(input))
    }

    @Test
    fun cdrOfList() {
        val input = "(cdr '(1 2 3))"

        assertSExpressionsMatch(parseString("(2 3)"), evaluateString(input))
    }

    @Test
    fun nestedRestOfList() {
        val input = "(rest (rest '(1 2 3)))"

        assertSExpressionsMatch(parseString("(3)"), evaluateString(input))
    }

    @Test
    fun restOfSymbol() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(rest 'x)")
        }
    }

    @Test
    fun restWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(rest '(1 2) '(1 2) \"oh\")")
        }
    }

    @Test
    fun restWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(rest)")
        }
    }
}
