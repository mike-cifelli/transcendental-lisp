package function.builtin

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Symbol
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class FuseTest : SymbolAndFunctionCleaner() {

    @Test
    fun fuseSymbolAndNumber() {
        assertSExpressionsMatch(Symbol("A-1"), evaluateString("(fuse 'a 1)"))
    }

    @Test
    fun fuseTwoSymbols_NeitherQuoted() {
        evaluateString("(setq a 'aaa)")
        evaluateString("(setq b 'bbb)")
        assertSExpressionsMatch(Symbol("AAA-BBB"), evaluateString("(fuse a b)"))
    }

    @Test
    fun fuseTwoSymbols_OneQuoted() {
        evaluateString("(setq b 'bbb)")
        assertSExpressionsMatch(Symbol("A-BBB"), evaluateString("(fuse 'a b)"))
    }

    @Test
    fun fuseTwoSymbols_BothQuoted() {
        assertSExpressionsMatch(Symbol("A-B"), evaluateString("(fuse 'a 'b)"))
    }

    @Test
    fun fuseWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(fuse 'a)")
        }
    }

    @Test
    fun fuseWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(fuse 'a 'b 'c)")
        }
    }

    @Test
    fun fuseWithBadFirstArgumentType() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(fuse 1 'b)")
        }
    }

    @Test
    fun fuseWithBadSecondArgumentType() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(fuse 'a \"b\")")
        }
    }
}
