package function.builtin

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.SymbolFunction.UndefinedSymbolFunctionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Nil
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.evaluateString

@LispTestInstance
class SymbolFunctionTest : SymbolAndFunctionCleaner() {

    @Test
    fun symbolFunction_BuiltInFunction() {
        val input = "(symbol-function '+)"

        assertThat(evaluateString(input).toString()).isEqualTo("#<FUNCTION +>")
    }

    @Test
    fun symbolFunction_BuiltInSpecialFunction() {
        val input = "(symbol-function 'if)"

        assertThat(evaluateString(input).toString()).isEqualTo("#<SPECIAL-FUNCTION IF>")
    }

    @Test
    fun symbolFunction_UserDefinedFunction() {
        val defineUserFunction = "(defun y (n m) (+ n m))"
        val input = "(symbol-function 'y)"

        evaluateString(defineUserFunction)
        assertThat(evaluateString(input).toString()).isEqualTo("(Y (N M) (+ N M))")
    }

    @Test
    fun symbolFunction_NonFunction() {
        val input = "(symbol-function 'a)"

        assertThrows(UndefinedSymbolFunctionException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun symbolFunctionWithBadArgumentType() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(symbol-function 2)")
        }
    }

    @Test
    fun symbolFunctionWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(symbol-function 'a 'b)")
        }
    }

    @Test
    fun symbolFunctionWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(symbol-function)")
        }
    }

    @Test
    fun undefinedSymbolFunctionException_HasCorrectAttributes() {
        assertIsErrorWithMessage(UndefinedSymbolFunctionException(Nil))
    }
}
