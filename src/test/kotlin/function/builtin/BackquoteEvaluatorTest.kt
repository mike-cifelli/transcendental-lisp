package function.builtin

import function.ArgumentValidator.DottedArgumentListException
import function.builtin.BackquoteEvaluator.AtSignNotInCommaException
import function.builtin.BackquoteEvaluator.AtSignNotListException
import function.builtin.BackquoteEvaluator.NestedAtSignException
import function.builtin.BackquoteEvaluator.NestedCommaException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.AtSignExpression
import sexpression.BackquoteExpression
import sexpression.CommaExpression
import sexpression.Cons
import sexpression.LispNumber
import sexpression.LispString
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import sexpression.Symbol.Companion.T
import testutil.LispTestInstance
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.makeList

@LispTestInstance
class BackquoteEvaluatorTest {

    private fun createBackquoteEvaluator(expression: SExpression): BackquoteEvaluator {
        return BackquoteEvaluator(BackquoteExpression(expression))
    }

    @Test
    fun evaluateNil() {
        val evaluator = createBackquoteEvaluator(Nil)

        assertSExpressionsMatch(Nil, evaluator.evaluate())
    }

    @Test
    fun evaluateNumber() {
        val input = LispNumber("99")
        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(input, evaluator.evaluate())
    }

    @Test
    fun evaluateList() {
        val input = makeList(LispNumber("1"), LispNumber("99"))
        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(input, evaluator.evaluate())
    }

    @Test
    fun evaluateComma() {
        val input = CommaExpression(makeList(Symbol("+"), LispNumber("1"), LispNumber("9")))
        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(LispNumber("10"), evaluator.evaluate())
    }

    @Test
    fun evaluateListWithComma() {
        val input = makeList(CommaExpression(makeList(Symbol("+"), LispNumber("1"), LispNumber("9"))))
        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(makeList(LispNumber("10")), evaluator.evaluate())
    }

    @Test
    fun evaluateListWithNestedComma() {
        val input = makeList(CommaExpression(CommaExpression(Symbol("+"))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(NestedCommaException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateListWithNoCommaPrecedingAtSign() {
        val input = makeList(AtSignExpression(makeList(Symbol("+"))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(AtSignNotInCommaException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateAtSign() {
        val input = AtSignExpression(makeList(Symbol("+")))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(AtSignNotInCommaException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateListWithNestedAtSigns() {
        val input = makeList(CommaExpression(AtSignExpression(AtSignExpression(makeList(Symbol("+"))))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(NestedAtSignException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateListWithCommaAfterAtSign() {
        val input = makeList(CommaExpression(AtSignExpression(CommaExpression(makeList(Symbol("+"))))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(NestedCommaException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateListWithAtSign() {
        val input = makeList(CommaExpression(AtSignExpression(makeList(Symbol("LIST"),
                                                                       LispNumber("1"),
                                                                       LispNumber("9")))))
        val expected = makeList(LispNumber("1"), LispNumber("9"))
        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(expected, evaluator.evaluate())
    }

    @Test
    fun atSignDoesNotEvaluateToList() {
        val input = makeList(CommaExpression(AtSignExpression(LispNumber("1"))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(AtSignNotListException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun evaluateListWithCommasAndAtSign() {
        val list1 = makeList(Symbol("LIST"), LispNumber("1"), LispNumber("9"))
        val list2 = makeList(Symbol("+"), LispNumber("20"), LispNumber("5"))
        val list3 = makeList(Symbol("LIST"), LispNumber("7"), LispNumber("6"))

        val input = makeList(LispNumber("78"),
                             CommaExpression(AtSignExpression(list1)),
                             CommaExpression(list2),
                             CommaExpression(list3),
                             LispString("\"sky\""))

        val expected = makeList(LispNumber("78"),
                                LispNumber("1"),
                                LispNumber("9"),
                                LispNumber("25"),
                                makeList(LispNumber("7"), LispNumber("6")),
                                LispString("\"sky\""))

        val evaluator = createBackquoteEvaluator(input)

        assertSExpressionsMatch(expected, evaluator.evaluate())
    }

    @Test
    fun evaluateDottedList() {
        val evaluator = createBackquoteEvaluator(Cons(T, T))

        assertThrows(DottedArgumentListException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun atSignWithDottedList() {
        val input = makeList(CommaExpression(AtSignExpression(makeList(Symbol("CONS"), T, T))))
        val evaluator = createBackquoteEvaluator(input)

        assertThrows(DottedArgumentListException::class.java) {
            evaluator.evaluate()
        }
    }

    @Test
    fun backquoteExceptionsHaveCorrectAttributes() {
        assertIsErrorWithMessage(NestedCommaException())
        assertIsErrorWithMessage(NestedAtSignException())
        assertIsErrorWithMessage(AtSignNotInCommaException())
        assertIsErrorWithMessage(AtSignNotListException())
    }
}
