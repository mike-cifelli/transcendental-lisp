package function.builtin

import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class FuncallTest : SymbolAndFunctionCleaner() {

    @Test
    fun funcallWithNumbers() {
        val input = "(funcall '+ 1 2 3)"

        assertSExpressionsMatch(parseString("6"), evaluateString(input))
    }

    @Test
    fun callWithNumbers() {
        val input = "(call '+ 1 2 3)"

        assertSExpressionsMatch(parseString("6"), evaluateString(input))
    }

    @Test
    fun funcallWithUserDefinedFunction() {
        val defineUserFunction = "(defun x (n m) (+ n m))"
        val input = "(funcall 'x 2 30)"

        evaluateString(defineUserFunction)
        assertSExpressionsMatch(parseString("32"), evaluateString(input))
    }

    @Test
    fun funcallWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(funcall)")
        }
    }
}
