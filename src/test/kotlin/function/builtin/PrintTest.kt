package function.builtin

import environment.RuntimeEnvironment
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.text.MessageFormat.format

@LispTestInstance
class PrintTest : SymbolAndFunctionCleaner() {

    private var outputStream: ByteArrayOutputStream? = null

    private fun assertPrinted(expected: String) {
        assertThat(outputStream!!.toString()).isEqualTo(expected)
    }

    override fun additionalSetUp() {
        outputStream = ByteArrayOutputStream()
        RuntimeEnvironment.reset()
        RuntimeEnvironment.output = PrintStream(outputStream!!)
    }

    override fun additionalTearDown() {
        RuntimeEnvironment.reset()
    }

    @Test
    fun printStringWorks() {
        val output = "\"Hello, world!\""

        evaluateString(format("(print {0})", output))
        assertPrinted(format("{0}\n", output))
    }

    @Test
    fun printSymbolWorks() {
        val output = "A"

        evaluateString(format("(print ''{0})", output))
        assertPrinted(format("{0}\n", output))
    }

    @Test
    fun printWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(print '1 '2)")
        }
    }

    @Test
    fun printWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(print)")
        }
    }
}
