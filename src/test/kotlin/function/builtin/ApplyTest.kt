package function.builtin

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Apply.Companion.apply
import function.builtin.Eval.UndefinedFunctionException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Cons
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class ApplyTest : SymbolAndFunctionCleaner() {

    @Test
    fun applyWithSymbol() {
        val input = "(apply '+ '(1 2 3))"

        assertSExpressionsMatch(parseString("6"), evaluateString(input))
    }

    @Test
    fun applyWithLambdaExpression() {
        val input = "(apply (lambda (x) (+ x 1)) '(25))"

        assertSExpressionsMatch(parseString("26"), evaluateString(input))
    }

    @Test
    fun applyWithQuotedLambdaExpression() {
        val input = "(apply '(lambda (x) (+ x 1)) '(25))"

        assertSExpressionsMatch(parseString("26"), evaluateString(input))
    }

    @Test
    fun staticApplyCall() {
        val argumentList = "(+ (25 10))"
        val parsedArgumentList = parseString(argumentList) as Cons

        assertSExpressionsMatch(parseString("35"), apply(parsedArgumentList))
    }

    @Test
    fun applyWithUndefinedFunction() {
        assertThrows(UndefinedFunctionException::class.java) {
            evaluateString("(apply 'f '(1 2 3))")
        }
    }

    @Test
    fun applyWithNonListSecondArgument() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(apply '+ '2)")
        }
    }

    @Test
    fun applyWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(apply '1 '2 '3)")
        }
    }

    @Test
    fun applyWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(apply '1)")
        }
    }

    @Test
    fun applyWithDottedArgumentList_ThrowsException() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(apply 'apply (cons 'T 'T))")
        }
    }

    @Test
    fun applyWithMacro() {
        evaluateString("(defmacro m (x) `(+ 2 ,x))")
        assertSExpressionsMatch(parseString("27"), evaluateString("(apply 'm '(25))"))
    }
}
