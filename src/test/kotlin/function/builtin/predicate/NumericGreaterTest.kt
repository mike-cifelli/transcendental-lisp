package function.builtin.predicate

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class NumericGreaterTest : SymbolAndFunctionCleaner() {

    @Test
    fun greaterThanWithOneNumber_ReturnsT() {
        val input = "(> 1)"

        assertT(evaluateString(input))
    }

    @Test
    fun greaterThanWithTwoNumbers_ReturnsNil() {
        val input = "(> 1 2)"

        assertNil(evaluateString(input))
    }

    @Test
    fun greaterThanWithTwoNumbers_ReturnsT() {
        val input = "(> 3 2)"

        assertT(evaluateString(input))
    }

    @Test
    fun greaterThanWithManyNumbers_ReturnsNil() {
        val input = "(> 4 3 2 5 1)"

        assertNil(evaluateString(input))
    }

    @Test
    fun greaterThanWithManyNumbers() {
        val input = "(> 4 3 2 1 0)"

        assertT(evaluateString(input))
    }

    @Test
    fun greaterThanWithNonNumbers() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(> 'x 'x)")
        }
    }

    @Test
    fun greaterThanWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(>)")
        }
    }
}
