package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class AtomTest : SymbolAndFunctionCleaner() {

    @Test
    fun atomIsAtom() {
        assertT(evaluateString("(atom 'a)"))
    }

    @Test
    fun atomIsAtomWithAlias() {
        assertT(evaluateString("(atom? 'a)"))
    }

    @Test
    fun listIsNotAtom() {
        assertNil(evaluateString("(atom '(1 2 3))"))
    }

    @Test
    fun testApplyWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(atom '1 '2)")
        }
    }

    @Test
    fun testApplyWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(atom)")
        }
    }
}
