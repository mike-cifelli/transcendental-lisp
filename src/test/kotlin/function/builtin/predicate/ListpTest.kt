package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class ListpTest : SymbolAndFunctionCleaner() {

    @Test
    fun listpWithList() {
        assertT(evaluateString("(listp '(1))"))
    }

    @Test
    fun listpWithListAndAlias() {
        assertT(evaluateString("(list? '(1))"))
    }

    @Test
    fun listpWithNonList() {
        assertNil(evaluateString("(listp 1)"))
    }

    @Test
    fun listpWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(listp)")
        }
    }

    @Test
    fun listpWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(listp '() '())")
        }
    }
}
