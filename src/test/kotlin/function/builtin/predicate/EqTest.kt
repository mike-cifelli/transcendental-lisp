package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class EqTest : SymbolAndFunctionCleaner() {

    @Test
    fun eqWithEqualAtoms() {
        val input = "(eq 1 1)"

        assertT(evaluateString(input))
    }

    @Test
    fun eqWithEqualAtomsAndAlias() {
        val input = "(eq? 1 1)"

        assertT(evaluateString(input))
    }

    @Test
    fun eqWithUnequalAtoms() {
        val input = "(eq 1 2)"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithTwoEqualNumbers() {
        val input = "(eq -4 -4)"

        assertT(evaluateString(input))
    }

    @Test
    fun eqWithTwoUnequalNumbers() {
        val input = "(eq +5 +7)"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithTwoEqualStrings() {
        val input = "(eq \"potato\" \"potato\")"

        assertT(evaluateString(input))
    }

    @Test
    fun eqWithTwoUnequalStrings() {
        val input = "(eq \"tomato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithTwoDifferentCasedStrings() {
        val input = "(eq \"Potato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithAtomAndList() {
        val input = "(eq 1 '(2))"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithSameList() {
        val initializeL1 = "(setq l1 '(1 2 3))"
        val initializeL2 = "(setq l2 l1)"
        val input = "(eq l1 l2)"

        evaluateString(initializeL1)
        evaluateString(initializeL2)

        assertT(evaluateString(input))
    }

    @Test
    fun eqWithEqualLists() {
        val input = "(eq '(1 2) '(1 2))"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithUnequalLists() {
        val input = "(eq '(1 2) '(3 4))"

        assertNil(evaluateString(input))
    }

    @Test
    fun eqWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(eq 'one 'two 'three)")
        }
    }

    @Test
    fun eqWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(eq 'one)")
        }
    }
}
