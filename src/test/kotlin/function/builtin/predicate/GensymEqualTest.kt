package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class GensymEqualTest : SymbolAndFunctionCleaner() {

    @Test
    fun gensymEqualWithTwoEqualAtoms() {
        val input = "(gensym-equal 'a 'a)"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoEqualAtomsAndAlias() {
        val input = "(gensym-equal? 'a 'a)"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoUnequalAtoms() {
        val input = "(gensym-equal 'a 'b)"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoEqualNumbers() {
        val input = "(gensym-equal -4 -4)"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoUnequalNumbers() {
        val input = "(gensym-equal +5 +7)"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoEqualStrings() {
        val input = "(gensym-equal \"potato\" \"potato\")"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoUnequalStrings() {
        val input = "(gensym-equal \"tomato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoDifferentCasedStrings() {
        val input = "(gensym-equal \"Potato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithAtomAndList() {
        val input = "(gensym-equal \"string\" '(m i k e))"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithListAndAtom() {
        val input = "(gensym-equal '(m i k e) \"string\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoEqualLists() {
        val input = "(gensym-equal '(1 2 3) '(1 2 3))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoUnequalLists() {
        val input = "(gensym-equal '(1 2 3) '(1 3 3))"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTwoEqualNestedLists() {
        val input = "(gensym-equal '(1 ((2) 3)) '(1 ((2) 3)))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(gensym-equal 1 2 3)")
        }
    }

    @Test
    fun gensymEqualWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(gensym-equal 1)")
        }
    }

    @Test
    fun gensymEqualWithGensyms() {
        val input = "(gensym-equal (gensym) (gensym))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithNestedGensyms() {
        val input = "(gensym-equal `(1 ,(gensym) (2 ,(gensym))) `(1 ,(gensym) (2 ,(gensym))))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithUnmatchedGensymPositions() {
        val input = "(gensym-equal (let ((one (gensym))) `(,one ,one))" +
                " (let ((one (gensym)) (two (gensym))) `(,one ,two)))"

        assertNil(evaluateString(input))
    }

    @Test
    fun gensymEqualWithMatchedGensymPositions() {
        val input = "(gensym-equal (let ((one (gensym))) `(,one ,one))" +
                " (let ((one (gensym)) (two (gensym))) `(,one ,one)))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithComplexMatchedGensymPositions() {
        val input = "(gensym-equal (let ((x (gensym)) (y (gensym)) (z (gensym))) `(,x (,y ,z)))" +
                " (let ((a (gensym)) (b (gensym)) (c (gensym))) `(,c (,a ,b))))"

        assertT(evaluateString(input))
    }

    @Test
    fun gensymEqualWithComplexUnmatchedGensymPositions() {
        val input = "(gensym-equal (let ((x (gensym)) (y (gensym)) (z (gensym))) `(,x , y (,z ,z)))" +
                " (let ((a (gensym)) (b (gensym)) (c (gensym))) `(,a ,c (,b ,c))))"

        assertNil(evaluateString(input))
    }
}
