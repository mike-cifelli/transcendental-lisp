package function.builtin.predicate

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class NumericLessTest : SymbolAndFunctionCleaner() {

    @Test
    fun lessThanWithOneNumber_ReturnsT() {
        val input = "(< 1)"

        assertT(evaluateString(input))
    }

    @Test
    fun lessThanWithTwoNumbers_ReturnsNil() {
        val input = "(< 2 1)"

        assertNil(evaluateString(input))
    }

    @Test
    fun lessThanWithTwoNumbers_ReturnsT() {
        val input = "(< 2 3)"

        assertT(evaluateString(input))
    }

    @Test
    fun lessThanWithManyNumbers_ReturnsNil() {
        val input = "(< 4 3 2 5 1)"

        assertNil(evaluateString(input))
    }

    @Test
    fun lessThanWithManyNumbers_ReturnsT() {
        val input = "(< 0 1 2 3 4)"

        assertT(evaluateString(input))
    }

    @Test
    fun lessThanWithNonNumbers() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(< '(1) '(2))")
        }
    }

    @Test
    fun lessThanWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(<)")
        }
    }
}
