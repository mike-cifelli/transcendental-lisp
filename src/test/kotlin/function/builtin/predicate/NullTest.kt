package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class NullTest : SymbolAndFunctionCleaner() {

    @Test
    fun nilIsNull() {
        assertT(evaluateString("(null ())"))
    }

    @Test
    fun nilIsNullWithAlias() {
        assertT(evaluateString("(null? ())"))
    }

    @Test
    fun listIsNotNull() {
        assertNil(evaluateString("(null '(1))"))
    }

    @Test
    fun testNullWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(null)")
        }
    }

    @Test
    fun testNullWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(null 1 2)")
        }
    }
}
