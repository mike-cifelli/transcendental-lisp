package function.builtin.predicate

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class NumericEqualTest : SymbolAndFunctionCleaner() {

    @Test
    fun `one number`() {
        assertT(evaluateString("(= 1)"))
    }

    @Test
    fun `equal numbers`() {
        assertT(evaluateString("(= 1 1)"))
    }

    @Test
    fun `unequal numbers`() {
        assertNil(evaluateString("(= 1 2)"))
    }

    @Test
    fun `many equal numbers`() {
        assertT(evaluateString("(= 4 4 4 4 4 4 4 4 4 4)"))
    }

    @Test
    fun `many unequal numbers`() {
        assertNil(evaluateString("(= 4 4 4 4 5 4 4 4 4 4)"))
    }

    @Test
    fun `bad argument types`() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(= 'x 'x)")
        }
    }

    @Test
    fun `too few arguments`() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(=)")
        }
    }
}
