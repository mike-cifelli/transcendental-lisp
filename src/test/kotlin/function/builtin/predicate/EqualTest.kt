package function.builtin.predicate

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class EqualTest : SymbolAndFunctionCleaner() {

    @Test
    fun equalWithTwoEqualAtoms() {
        val input = "(equal 'a 'a)"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTwoEqualAtomsAndAlias() {
        val input = "(equal? 'a 'a)"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTwoUnequalAtoms() {
        val input = "(equal 'a 'b)"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithTwoEqualNumbers() {
        val input = "(equal -4 -4)"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTwoUnequalNumbers() {
        val input = "(equal +5 +7)"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithTwoEqualStrings() {
        val input = "(equal \"potato\" \"potato\")"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTwoUnequalStrings() {
        val input = "(equal \"tomato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithTwoDifferentCasedStrings() {
        val input = "(equal \"Potato\" \"potato\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithAtomAndList() {
        val input = "(equal \"string\" '(m i k e))"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithListAndAtom() {
        val input = "(equal '(m i k e) \"string\")"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithTwoEqualLists() {
        val input = "(equal '(1 2 3) '(1 2 3))"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTwoUnequalLists() {
        val input = "(equal '(1 2 3) '(1 3 3))"

        assertNil(evaluateString(input))
    }

    @Test
    fun equalWithTwoEqualNestedLists() {
        val input = "(equal '(1 ((2) 3)) '(1 ((2) 3)))"

        assertT(evaluateString(input))
    }

    @Test
    fun equalWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(equal 1 2 3)")
        }
    }

    @Test
    fun equalWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(equal 1)")
        }
    }
}
