package function.builtin

import environment.RuntimeEnvironment
import function.ArgumentValidator.TooManyArgumentsException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString

@LispTestInstance
class ExitTest : SymbolAndFunctionCleaner() {

    companion object {
        private const val TERMINATED = "terminated"
    }

    private val indicatorSet = mutableSetOf<String>()

    private fun assertTerminated() {
        assertThat(indicatorSet).contains(TERMINATED)
    }

    private fun assertNotTerminated() {
        assertThat(indicatorSet).doesNotContain(TERMINATED)
    }

    override fun additionalSetUp() {
        indicatorSet.clear()
        RuntimeEnvironment.reset()
        RuntimeEnvironment.terminationFunction = { indicatorSet.add(TERMINATED) }
    }

    override fun additionalTearDown() {
        RuntimeEnvironment.reset()
    }

    @Test
    fun `exit works`() {
        evaluateString("(exit)")
        assertTerminated()
    }

    @Test
    fun `no termination when exit not called`() {
        assertNotTerminated()
    }

    @Test
    fun `too many arguments`() {
        assertThrows(TooManyArgumentsException::class.java) { evaluateString("(exit 1)") }
    }
}
