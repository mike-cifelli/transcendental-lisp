package function.builtin

import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Gensym.Companion.GENSYM_PREFIX
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsDoNotMatch
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertSymbol
import token.TokenFactory.BadCharacterException

@LispTestInstance
class GensymTest : SymbolAndFunctionCleaner() {

    @Test
    fun gensymCreatesSymbol() {
        assertSymbol(evaluateString("(gensym)"))
    }

    @Test
    fun gensymCreatesUniqueSymbol() {
        assertSExpressionsDoNotMatch(evaluateString("(gensym)"), evaluateString("(gensym)"))
    }

    @Test
    fun simpleGensymUsage() {
        val input = "(let ((x (gensym))) (set x 23) (eval x))"
        assertSExpressionsMatch(evaluateString("23"), evaluateString(input))
    }

    @Test
    fun cannotUseGensymValueManually() {
        val variableName = GENSYM_PREFIX + 1

        assertThrows(BadCharacterException::class.java) {
            evaluateString("(setq $variableName 100)")
        }
    }

    @Test
    fun gensymWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(gensym 1)")
        }
    }
}
