package function.builtin.special

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner

import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class CondTest : SymbolAndFunctionCleaner() {

    @Test
    fun condWithNoArguments() {
        val input = "(cond)"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun condWithTrue() {
        val input = "(cond (T))"

        assertSExpressionsMatch(parseString("T"), evaluateString(input))
    }

    @Test
    fun condWithNumber() {
        val input = "(cond ((+ 1 2)))"

        assertSExpressionsMatch(parseString("3"), evaluateString(input))
    }

    @Test
    fun condWithSingleClause() {
        val input = "(cond (T \"true\"))"

        assertSExpressionsMatch(parseString("\"true\""), evaluateString(input))
    }

    @Test
    fun condWithMultipleClauses() {
        val input = "(cond ((= 1 2) 2) ((= 1 2) 2) ((= 1 1) 3))"

        assertSExpressionsMatch(parseString("3"), evaluateString(input))
    }

    @Test
    fun condWithMultipleTrueTests_ReturnsFirstOne() {
        val input = "(cond ((= 1 1) 2) ((= 1 1) 3))"

        assertSExpressionsMatch(parseString("2"), evaluateString(input))
    }

    @Test
    fun condWithMultipleTrueTests_OnlyEvaluatesFirstOne() {
        val input = "(cond ((= 1 1) 2) ((= 1 1) x))"

        assertSExpressionsMatch(parseString("2"), evaluateString(input))
    }

    @Test
    fun condWithMultipleValuesInConsequent_OnlyReturnsLast() {
        val input = "(cond ((= 1 1) 2 3 4))"

        assertSExpressionsMatch(parseString("4"), evaluateString(input))
    }

    @Test
    fun condWithNoTrueTest_ReturnsNil() {
        val input = "(cond ((= 1 2) T) ((= 1 3) T))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun condWithEmptyListArgument_ThrowsException() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(cond ())")
        }
    }

    @Test
    fun condWithNilArgument_ThrowsException() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(cond nil)")
        }
    }

    @Test
    fun condWithNonListArgument_ThrowsException() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(cond o)")
        }
    }

    @Test
    fun condWithDottedArgumentList_ThrowsException() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(apply 'cond (cons '(nil T) 'b))")
        }
    }

    @Test
    fun condWithDottedClause() {
        val input = "(eval `(cond ,(cons T 'pear)))"

        assertSExpressionsMatch(parseString("T"), evaluateString(input))
    }
}
