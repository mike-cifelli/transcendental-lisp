package function.builtin.special

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class CaseTest : SymbolAndFunctionCleaner() {

    @Test
    fun caseWithKeyOnly() {
        val input = "(case t)"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithEmptyConsequent() {
        val input = "(case :a ((:a)))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithOneClause_Match() {
        val input = "(case :a ((:a) 'banana))"

        assertSExpressionsMatch(parseString("banana"), evaluateString(input))
    }

    @Test
    fun caseWithOneClause_NoMatch() {
        val input = "(case :a ((:b) 'banana))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithSeveralClauses_Match() {
        val input = "(case :a ((:b) 'orange) ((:a) 'banana))"

        assertSExpressionsMatch(parseString("banana"), evaluateString(input))
    }

    @Test
    fun caseWithSeveralClauses_NoMatch() {
        val input = "(case :a ((:b) 'orange) ((:c) 'banana))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithSeveralItemsInKeyList_Match() {
        val input = "(case :a ((:b :a) 'orange) ((:c :d) 'banana))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithSeveralItemsInKeyList_NoMatch() {
        val input = "(case :a ((:b :f) 'orange) ((:c :d) 'banana))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithSymbolicKeyList_Match() {
        val input = "(case :a (:a 'orange))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithSymbolicKeyList_NoMatch() {
        val input = "(case :a (:b 'orange))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseDoesNotEvaluateKeyList() {
        val input = "(case 'x ((x) t))"

        assertSExpressionsMatch(parseString("t"), evaluateString(input))
    }

    @Test
    fun caseWithEmptyKeyList() {
        val input = "(case nil (() 'orange))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }

    @Test
    fun caseWithNil() {
        val input = "(case nil ((nil) 'orange))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithEmptyList() {
        val input = "(case () ((()) 'orange))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithList() {
        val input = "(case '(5 4 3) (((1 2) (5 4 3)) 'orange))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithDefaultClause() {
        val input = "(case nil (() 'banana) (t 'orange))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithOutOfOrderDefaultClause() {
        val input = "(case :a (t 'orange) (:a 'banana))"

        assertSExpressionsMatch(parseString("orange"), evaluateString(input))
    }

    @Test
    fun caseWithKeyListContainingT() {
        val input = "(case t ((t) 'banana))"

        assertSExpressionsMatch(parseString("banana"), evaluateString(input))
    }

    @Test
    fun caseWithMultipleMatches_ReturnsFirst() {
        val input = "(case 2 ((0) 'banana) ((1) 'apple) ((2) 'avocado) ((2) 'greenbean))"

        assertSExpressionsMatch(parseString("avocado"), evaluateString(input))
    }

    @Test
    fun caseEvaluatesMultipleConsequents() {
        val input = "(case 2 (1 1) (2 (setq x 'x) (setq y 'y)) (3 3)))"

        evaluateString(input)

        assertSExpressionsMatch(parseString("x"), evaluateString("x"))
        assertSExpressionsMatch(parseString("y"), evaluateString("y"))
    }

    @Test
    fun caseReturnsValueOfLastConsequent() {
        val input = "(case 2 (1 1) (2 3 4 5) (3 3))"

        assertSExpressionsMatch(parseString("5"), evaluateString(input))
    }

    @Test
    fun caseOnlyEvaluatesConsequentInFirstMatchingClause() {
        val input = "(case 2 ((0) (setq zero 0)) ((1) (setq one 1)) ((2) (setq two '2)) ((2) (setq two 'two)))"

        evaluateString("(setq zero nil)")
        evaluateString("(setq one nil)")
        evaluateString(input)

        assertSExpressionsMatch(parseString("nil"), evaluateString("zero"))
        assertSExpressionsMatch(parseString("nil"), evaluateString("one"))
        assertSExpressionsMatch(parseString("2"), evaluateString("two"))
    }

    @Test
    fun caseWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(case)")
        }
    }

    @Test
    fun caseWithNonListClause() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(case :a t)")
        }
    }

    @Test
    fun caseWithEmptyClause() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(case :a ())")
        }
    }

    @Test
    fun caseWithNilClause() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(case :a nil)")
        }
    }

    @Test
    fun caseWithDottedClause() {
        val input = "(eval `(case :a ,(cons :a 'banana)))"

        assertSExpressionsMatch(parseString("nil"), evaluateString(input))
    }
}
