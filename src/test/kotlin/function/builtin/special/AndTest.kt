package function.builtin.special

import function.builtin.Eval.UndefinedSymbolException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class AndTest : SymbolAndFunctionCleaner() {

    @Test
    fun andByItself() {
        val input = "(and)"

        assertT(evaluateString(input))
    }

    @Test
    fun andWithNil() {
        val input = "(and nil)"

        assertNil(evaluateString(input))
    }

    @Test
    fun andWithT() {
        val input = "(and t)"

        assertT(evaluateString(input))
    }

    @Test
    fun andWithNumber() {
        val input = "(and 7)"

        assertSExpressionsMatch(LispNumber("7"), evaluateString(input))
    }

    @Test
    fun andWithSeveralValues() {
        val input = "(and t t nil t t)"

        assertNil(evaluateString(input))
    }

    @Test
    fun andWithSeveralNumbers() {
        val input = "(and 1 2 3)"

        assertSExpressionsMatch(LispNumber("3"), evaluateString(input))
    }

    @Test
    fun andShortCircuits() {
        val input = "(and nil (setq x 22))"

        assertNil(evaluateString(input))

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("x")
        }
    }
}
