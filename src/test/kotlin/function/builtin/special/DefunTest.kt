package function.builtin.special

import environment.RuntimeEnvironment
import error.ErrorManager
import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.UserDefinedFunction.IllegalKeywordRestPositionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString
import java.io.ByteArrayOutputStream
import java.io.PrintStream

@LispTestInstance
class DefunTest : SymbolAndFunctionCleaner() {

    private var outputStream = ByteArrayOutputStream()

    private fun assertSomethingPrinted() {
        assertThat(outputStream.toByteArray()).isNotEmpty()
    }

    override fun additionalSetUp() {
        outputStream.reset()
        RuntimeEnvironment.reset()
        RuntimeEnvironment.output = PrintStream(outputStream)
        RuntimeEnvironment.errorManager = ErrorManager()
        RuntimeEnvironment.warningOutputDecorator = { it }
    }

    override fun additionalTearDown() {
        RuntimeEnvironment.reset()
    }

    @Test
    fun defun() {
        val input = "(defun f () t)"

        assertSExpressionsMatch(parseString("f"), evaluateString(input))
        assertSExpressionsMatch(parseString("t"), evaluateString("(f)"))
    }

    @Test
    fun defunWithEmptyBody() {
        val input = "(defun f ())"

        assertSExpressionsMatch(parseString("f"), evaluateString(input))
        assertSExpressionsMatch(parseString("()"), evaluateString("(f)"))
    }

    @Test
    fun defunEvaluatesArguments() {
        evaluateString("(defun f (x) (car x))")
        assertSExpressionsMatch(parseString("1"), evaluateString("(f '(1 2 3))"))
    }

    @Test
    fun defunRecursiveFunction() {
        evaluateString("(defun fact (x) (if (< x 2) 1 (* x (fact (- x 1)))))")
        assertSExpressionsMatch(parseString("120"), evaluateString("(fact 5)"))
    }

    @Test
    fun defunTailRecursiveFunction() {
        evaluateString("(defun fact-tail (x acc) (if (< x 2) acc (fact-tail (- x 1) (* x acc))))")
        assertSExpressionsMatch(parseString("120"), evaluateString("(fact-tail 5 1)"))
    }

    @Test
    fun defunSimpleClass() {
        evaluateString("(defun counter-class () (let ((counter 0)) (lambda () (setq counter (+ 1 counter)))))")
        evaluateString("(setq my-counter (counter-class))")

        assertSExpressionsMatch(parseString("1"), evaluateString("(funcall my-counter)"))
        assertSExpressionsMatch(parseString("2"), evaluateString("(funcall my-counter)"))
        assertSExpressionsMatch(parseString("3"), evaluateString("(funcall my-counter)"))
        assertSExpressionsMatch(parseString("4"), evaluateString("(funcall my-counter)"))
    }

    @Test
    fun redefineFunction_DisplaysWarning() {
        val input = "(defun myFunction () nil)"
        evaluateString(input)
        evaluateString(input)

        assertSomethingPrinted()
    }

    @Test
    fun redefineFunction_ActuallyRedefinesFunction() {
        evaluateString("(defun myFunction () nil)")
        evaluateString("(defun myFunction () T)")

        assertSomethingPrinted()
        assertSExpressionsMatch(parseString("t"), evaluateString("(myFunction)"))
    }

    @Test
    fun defunWithDottedLambdaList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(funcall 'defun 'f (cons 'a 'b) ())")
        }
    }

    @Test
    fun defunWithNonSymbolName() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(defun 1 () ())")
        }
    }

    @Test
    fun defunWithBadLambdaList() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(defun f a ())")
        }
    }

    @Test
    fun defunWithNonSymbolInLambdaList() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(defun f (1) ())")
        }
    }

    @Test
    fun defunWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(defun f)")
        }
    }

    @Test
    fun defunFunctionAndCallWithTooFewArguments() {
        evaluateString("(defun f (a b))")

        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(f 'a)")
        }
    }

    @Test
    fun defunFunctionAndCallWithTooManyArguments() {
        evaluateString("(defun f (a b))")

        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(f 'a 'b 'c)")
        }
    }

    @Test
    fun defunWithKeywordRestParameter() {
        evaluateString("(defun f (&rest x) (car x))")
        assertSExpressionsMatch(parseString("1"), evaluateString("(f 1 2 3 4 5)"))
    }

    @Test
    fun defunWithNormalAndKeywordRestParameter() {
        evaluateString("(defun f (a &rest b) (cons a b))")
        assertSExpressionsMatch(parseString("(1 2 3 4 5)"), evaluateString("(f 1 2 3 4 5)"))
    }

    @Test
    fun defunWithParametersFollowingKeywordRest() {
        assertThrows(IllegalKeywordRestPositionException::class.java) {
            evaluateString("(defun f (a &rest b c) (cons a b))")
        }
    }

    @Test
    fun defunWithKeywordRest_CallWithNoArguments() {
        evaluateString("(defun f (&rest a) (car a))")
        assertSExpressionsMatch(parseString("nil"), evaluateString("(f)"))
    }

    @Test
    fun defunWithNormalAndKeywordRest_CallWithNoArguments() {
        evaluateString("(defun f (a &rest b) a)")

        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(f)")
        }
    }

    @Test
    fun resultOfFunctionIsNotEvaluated() {
        evaluateString("(setq x 'grains)")
        evaluateString("(define-special f (x) 'x)")
        evaluateString("(f (setq x 'sprouts))")

        assertSExpressionsMatch(parseString("grains"), evaluateString("x"))
    }
}
