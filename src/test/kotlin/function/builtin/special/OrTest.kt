package function.builtin.special

import function.builtin.Eval.UndefinedSymbolException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class OrTest : SymbolAndFunctionCleaner() {

    @Test
    fun orByItself() {
        val input = "(or)"

        assertNil(evaluateString(input))
    }

    @Test
    fun orWithNil() {
        val input = "(or nil)"

        assertNil(evaluateString(input))
    }

    @Test
    fun orWithT() {
        val input = "(or t)"

        assertT(evaluateString(input))
    }

    @Test
    fun orWithNumber() {
        val input = "(or 7)"

        assertSExpressionsMatch(LispNumber("7"), evaluateString(input))
    }

    @Test
    fun orWithSeveralValues() {
        val input = "(or nil nil nil t nil)"

        assertT(evaluateString(input))
    }

    @Test
    fun orWithSeveralNumbers() {
        val input = "(or 1 2 3)"

        assertSExpressionsMatch(LispNumber("1"), evaluateString(input))
    }

    @Test
    fun orShortCircuits() {
        val input = "(or t (setq x 22))"

        assertT(evaluateString(input))

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("x")
        }
    }
}
