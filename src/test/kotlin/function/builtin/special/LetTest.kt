package function.builtin.special

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Eval.UndefinedSymbolException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.LispNumber
import sexpression.Nil
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class LetTest : SymbolAndFunctionCleaner() {

    @Test
    fun simpleLet() {
        val input = "(let ((x 1)) x)"

        assertSExpressionsMatch(LispNumber("1"), evaluateString(input))
    }

    @Test
    fun emptyLet_ReturnsNil() {
        val input = "(let ())"

        assertSExpressionsMatch(Nil, evaluateString(input))
    }

    @Test
    fun letWithSymbolsOnly_SetsValuesToNil() {
        val input = "(let ((x) (y)) (list x y))"

        assertSExpressionsMatch(Cons(Nil, Cons(Nil, Nil)), evaluateString(input))
    }

    @Test
    fun letWithSetq_DoesNotAlterGlobalVariable() {
        val before = "(setq x 22)"
        val input = "(let ((x 1)) x)"
        val after = "x"

        assertSExpressionsMatch(LispNumber("22"), evaluateString(before))
        assertSExpressionsMatch(LispNumber("1"), evaluateString(input))
        assertSExpressionsMatch(LispNumber("22"), evaluateString(after))
    }

    @Test
    fun letWithNestedSetq_DoesNotAlterGlobalVariable() {
        val before = "(setq x 22)"
        val input = "(let ((x 33)) (setq x 44) x)"
        val after = "x"

        assertSExpressionsMatch(LispNumber("22"), evaluateString(before))
        assertSExpressionsMatch(LispNumber("44"), evaluateString(input))
        assertSExpressionsMatch(LispNumber("22"), evaluateString(after))
    }

    @Test
    fun nestedLet() {
        val input = "(let ((x 1)) (let ((y (+ 1 x))) y))"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun nestedLetWithGlobals() {
        val before = "(setq x 92)"
        val input = "(let ((x 1)) (let ((y (+ 1 x))) y))"
        val after = "x"

        assertSExpressionsMatch(LispNumber("92"), evaluateString(before))
        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
        assertSExpressionsMatch(LispNumber("92"), evaluateString(after))
    }

    @Test
    fun alterGlobalVariableFromLet() {
        val before = "(setq x 1)"
        val input = "(let ((y 1)) (setq x 2))"
        val after = "x"

        assertSExpressionsMatch(LispNumber("1"), evaluateString(before))
        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
        assertSExpressionsMatch(LispNumber("2"), evaluateString(after))
    }

    @Test
    fun accessGlobalVariableFromLet() {
        val before = "(setq x 1)"
        val input = "(let () x)"

        assertSExpressionsMatch(LispNumber("1"), evaluateString(before))
        assertSExpressionsMatch(LispNumber("1"), evaluateString(input))
    }

    @Test
    fun letDoesNotSetGlobalVariable() {
        val input = "(let ((x 1)) nil)"

        evaluateString(input)

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("x")
        }
    }

    @Test
    fun letWithNonList() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(let a)")
        }
    }

    @Test
    fun letWithNoPairs() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(let (a))")
        }
    }

    @Test
    fun letWithTooFewItemsInPair() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(let (()))")
        }
    }

    @Test
    fun letWithTooManyItemsInPair() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(let ((a b c)))")
        }
    }

    @Test
    fun letWithNonSymbolInPair() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(let ((1 b)))")
        }
    }

    @Test
    fun letWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(let)")
        }
    }

    @Test
    fun letWithDottedArgumentList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(apply 'let (cons 'a 'b))")
        }
    }

    @Test
    fun letWithDottedPairList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(apply 'let (cons (cons 'a 'b) nil))")
        }
    }

    @Test
    fun letWithDottedPair() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(apply 'let (cons (cons (cons 'a 'b) nil) nil))")
        }
    }

    @Test
    fun letEvaluatesSymbolsInParallel() {
        val input = "(let ((x 1) (y (+ x 1))) (+ x y))"

        assertThrows(UndefinedSymbolException::class.java) {
            assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
        }
    }
}
