package function.builtin.special

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class QuoteTest : SymbolAndFunctionCleaner() {

    @Test
    fun quoteSymbol() {
        val input = "'a"

        assertSExpressionsMatch(parseString("a"), evaluateString(input))
    }

    @Test
    fun quoteList() {
        val input = "'(l i s t)"

        assertSExpressionsMatch(parseString("(l i s t)"), evaluateString(input))
    }

    @Test
    fun quoteWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(quote)")
        }
    }

    @Test
    fun quoteWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(quote a b)")
        }
    }
}
