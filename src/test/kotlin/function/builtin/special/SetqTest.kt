package function.builtin.special

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Eval.UndefinedSymbolException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import table.SymbolTable
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class SetqTest : SymbolAndFunctionCleaner() {

    @Test
    fun setq() {
        evaluateString("(setq a 23)")
        assertSExpressionsMatch(LispNumber("23"), evaluateString("a"))
    }

    @Test
    fun lookupDefinedSymbol() {
        evaluateString("(setq a 23)")
        assertSExpressionsMatch(LispNumber("23"), executionContext.lookupSymbolValue("A")!!)
    }

    @Test
    fun lookupUndefinedSymbol() {
        assertThat(executionContext.lookupSymbolValue("A")).isNull()
    }

    @Test
    fun setqGlobalVariable() {
        evaluateString("(setq a 23)")
        val global = executionContext.scope
        executionContext.scope = SymbolTable(global)

        evaluateString("(setq a 94)")
        executionContext.scope = global
        assertSExpressionsMatch(LispNumber("94"), evaluateString("a"))
    }

    @Test
    fun setqLocalVariable() {
        val global = executionContext.scope
        val local = SymbolTable(global)
        local["A"] = LispNumber("99")
        executionContext.scope = local

        evaluateString("(setq a 94)")
        assertSExpressionsMatch(LispNumber("94"), evaluateString("a"))
    }

    @Test
    fun setqLocalVariableDefined_DoesNotSetGlobal() {
        val global = executionContext.scope
        val local = SymbolTable(global)
        local["A"] = LispNumber("99")
        executionContext.scope = local

        evaluateString("(setq a 94)")
        executionContext.scope = global

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("a")
        }
    }

    @Test
    fun setqLocalVariableUndefined_SetsGlobal() {
        val global = executionContext.scope
        val local = SymbolTable(global)
        executionContext.scope = local

        evaluateString("(setq a 94)")
        executionContext.scope = global
        assertSExpressionsMatch(LispNumber("94"), evaluateString("a"))
    }

    @Test
    fun setqWithNonSymbol() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(setq 1 2)")
        }
    }

    @Test
    fun setqWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(setq x)")
        }
    }

    @Test
    fun setqWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(setq a b c)")
        }
    }
}
