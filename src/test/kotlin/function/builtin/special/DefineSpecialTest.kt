package function.builtin.special

import environment.RuntimeEnvironment
import error.ErrorManager
import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.UserDefinedFunction.IllegalKeywordRestPositionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString
import java.io.ByteArrayOutputStream
import java.io.PrintStream

@LispTestInstance
class DefineSpecialTest : SymbolAndFunctionCleaner() {

    private var outputStream = ByteArrayOutputStream()

    private fun assertSomethingPrinted() {
        assertThat(outputStream.toByteArray()).isNotEmpty()
    }

    override fun additionalSetUp() {
        outputStream.reset()
        RuntimeEnvironment.reset()
        RuntimeEnvironment.output = PrintStream(outputStream)
        RuntimeEnvironment.errorManager = ErrorManager()
        RuntimeEnvironment.warningOutputDecorator = { it }
    }

    override fun additionalTearDown() {
        RuntimeEnvironment.reset()
    }

    @Test
    fun defineSpecial() {
        val input = "(define-special f () t)"

        assertSExpressionsMatch(parseString("f"), evaluateString(input))
        assertSExpressionsMatch(parseString("t"), evaluateString("(f)"))
    }

    @Test
    fun defineSpecialWithEmptyBody() {
        val input = "(define-special f ())"

        assertSExpressionsMatch(parseString("f"), evaluateString(input))
        assertSExpressionsMatch(parseString("()"), evaluateString("(f)"))
    }

    @Test
    fun defineSpecialDoesNotEvaluateArguments() {
        evaluateString("(define-special f (x) (car x))")
        assertSExpressionsMatch(parseString("quote"), evaluateString("(f '(1 2 3))"))
    }

    @Test
    fun defineSpecialAdd() {
        evaluateString("(define-special f (x) (+ (eval x) 23))")
        assertSExpressionsMatch(parseString("27"), evaluateString("(f (+ 2 2))"))
    }

    @Test
    fun defineSpecialSetVariable() {
        evaluateString("(define-special f (x) (set x 23))")
        evaluateString("(f y)")
        assertSExpressionsMatch(parseString("23"), evaluateString("y"))
    }

    @Test
    fun defineSpecialVariableCapture() {
        evaluateString("(setq x 0)")
        evaluateString("(define-special f (x) (set x 23))")
        evaluateString("(f x)")
        assertSExpressionsMatch(parseString("0"), evaluateString("x"))
    }

    @Test
    fun defineSpecialAvoidVariableCaptureConvention() {
        evaluateString("(setq x 0)")
        evaluateString("(define-special f (-x-) (set -x- 23))")
        evaluateString("(f x)")
        assertSExpressionsMatch(parseString("23"), evaluateString("x"))
    }

    @Test
    fun redefineSpecial_DisplaysWarning() {
        val input = "(define-special myFunction () nil)"
        evaluateString(input)
        evaluateString(input)

        assertSomethingPrinted()
    }

    @Test
    fun redefineSpecial_ActuallyRedefinesSpecialFunction() {
        evaluateString("(define-special mySpecialFunction () nil)")
        evaluateString("(define-special mySpecialFunction () T)")

        assertSomethingPrinted()
        assertSExpressionsMatch(parseString("t"), evaluateString("(mySpecialFunction)"))
    }

    @Test
    fun defineSpecialWithDottedLambdaList() {
        assertThrows(DottedArgumentListException::class.java) {
            evaluateString("(funcall 'define-special 'x (cons 'a 'b) ())")
        }
    }

    @Test
    fun defineSpecialWithNonSymbolName() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(define-special 1 () ())")
        }
    }

    @Test
    fun defineSpecialWithBadLambdaList() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(define-special x a ())")
        }
    }

    @Test
    fun defineSpecialWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(define-special x)")
        }
    }

    @Test
    fun defineSpecialAndCallWithTooFewArguments() {
        evaluateString("(define-special x (a b))")

        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(x a)")
        }
    }

    @Test
    fun defineSpecialAndCallWithTooManyArguments() {
        evaluateString("(define-special x (a b))")

        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(x a b c)")
        }
    }

    @Test
    fun defineSpecialWithKeywordRestParameter() {
        evaluateString("(define-special f (&rest x) (car x))")
        assertSExpressionsMatch(parseString("1"), evaluateString("(f 1 2 3 4 5)"))
    }

    @Test
    fun defineSpecialWithNormalAndKeywordRestParameter() {
        evaluateString("(define-special f (a &rest b) (cons a b))")
        assertSExpressionsMatch(parseString("(1 2 3 4 5)"), evaluateString("(f 1 2 3 4 5)"))
    }

    @Test
    fun defineSpecialWithParametersFollowingKeywordRest() {
        assertThrows(IllegalKeywordRestPositionException::class.java) {
            evaluateString("(define-special f (a &rest b c) (cons a b))")
        }
    }

    @Test
    fun defineSpecialWithKeywordRest_CallWithNoArguments() {
        evaluateString("(define-special f (&rest a) (car a))")
        assertSExpressionsMatch(parseString("nil"), evaluateString("(f)"))
    }

    @Test
    fun defineSpecialWithNormalAndKeywordRest_CallWithNoArguments() {
        evaluateString("(define-special f (a &rest b) a)")

        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(f)")
        }
    }

    @Test
    fun resultOfSpecialFunctionIsNotEvaluated() {
        evaluateString("(setq x 'grains)")
        evaluateString("(define-special f (x) x)")
        evaluateString("(f (setq x 'sprouts))")

        assertSExpressionsMatch(parseString("grains"), evaluateString("x"))
    }
}
