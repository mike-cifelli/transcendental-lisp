package function.builtin.special

import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Eval.UndefinedSymbolException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.evaluateString
import testutil.TypeAssertions.assertNil
import testutil.TypeAssertions.assertT

@LispTestInstance
class IfTest : SymbolAndFunctionCleaner() {

    @Test
    fun ifWithOneExpression_ReturnsExpression() {
        val input = "(if t t)"

        assertT(evaluateString(input))
    }

    @Test
    fun ifWithOneExpression_ReturnsNil() {
        val input = "(if nil t)"

        assertNil(evaluateString(input))
    }

    @Test
    fun ifWithTwoExpressions_ReturnsFirst() {
        val input = "(if t t nil)"

        assertT(evaluateString(input))
    }

    @Test
    fun ifWithTwoExpressions_ReturnsSecond() {
        val input = "(if nil nil t)"

        assertT(evaluateString(input))
    }

    @Test
    fun ifWithNumericConditional() {
        val input = "(if 23 t nil)"

        assertT(evaluateString(input))
    }

    @Test
    fun ifWithNilCondition_DoesNotEvaluateThenForm() {
        val input = "(if nil (setq x 22))"

        assertNil(evaluateString(input))
        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("x")
        }
    }

    @Test
    fun ifWithTrueCondition_DoesNotEvaluateElseForm() {
        val input = "(if t nil (setq x 22))"

        assertNil(evaluateString(input))
        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("x")
        }
    }

    @Test
    fun ifWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(if t)")
        }
    }

    @Test
    fun ifWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(if t t t t)")
        }
    }
}
