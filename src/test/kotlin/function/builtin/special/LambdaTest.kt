package function.builtin.special

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Eval.UndefinedFunctionException
import function.builtin.special.Lambda.Lambda.createFunction
import function.builtin.special.Lambda.Lambda.isLambdaExpression
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.LispNumber
import sexpression.LispNumber.Companion.ONE
import sexpression.Nil
import sexpression.Symbol
import sexpression.Symbol.Companion.T
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class LambdaTest : SymbolAndFunctionCleaner() {

    @Test
    fun `lambda is evaluated`() {
        val input = "(lambda (x) x)"

        assertSExpressionsMatch(parseString("(LAMBDA (X) X)"), evaluateString(input))
    }

    @Test
    fun `lambda symbol is evaluated`() {
        val input = "(λ (x) x)"

        assertSExpressionsMatch(parseString("(LAMBDA (X) X)"), evaluateString(input))
    }

    @Test
    fun `lambda with no body`() {
        val input = "(lambda ())"

        assertSExpressionsMatch(parseString("(LAMBDA ())"), evaluateString(input))
    }

    @Test
    fun `lambda expression is a lambda expression`() {
        val lambdaExpression = Cons(Symbol("LAMBDA"), Cons(Nil, Cons(Nil, Nil)))

        assertThat(isLambdaExpression(lambdaExpression)).isTrue()
    }

    @Test
    fun `something else is not a lambda expression`() {
        assertThat(isLambdaExpression(T)).isFalse()
    }

    @Test
    fun `create lambda expression`() {
        val lambdaExpression = Cons(Symbol("LAMBDA"), Cons(Nil, Cons(Nil, Nil)))

        assertSExpressionsMatch(parseString("(:LAMBDA () ())"), createFunction(lambdaExpression).lambdaExpression)
    }

    @Test
    fun `dotted argument list`() {
        val input = "(apply 'lambda (cons '(x) 1))"

        assertThrows(DottedArgumentListException::class.java) { evaluateString(input) }
    }

    @Test
    fun `dotted lambda list`() {
        val input = "(funcall 'lambda (cons 'a 'b) ())"

        assertThrows(DottedArgumentListException::class.java) { evaluateString(input) }
    }

    @Test
    fun `create function with dotted argument list`() {
        val lambdaExpression = Cons(Symbol("LAMBDA"), Cons(Nil, ONE))

        assertThrows(DottedArgumentListException::class.java) { createFunction(lambdaExpression) }
    }

    @Test
    fun `create function with non list`() {
        val lambdaExpression = Cons(Symbol("LAMBDA"), ONE)

        assertThrows(BadArgumentTypeException::class.java) { createFunction(lambdaExpression) }
    }

    @Test
    fun `bad argument type`() {
        assertThrows(BadArgumentTypeException::class.java) { evaluateString("(lambda (1) ())") }
    }

    @Test
    fun `too few arguments`() {
        assertThrows(TooFewArgumentsException::class.java) { evaluateString("(lambda)") }
    }

    @Test
    fun `anonymous lambda call`() {
        val input = "((lambda (x) x) 203)"

        assertSExpressionsMatch(LispNumber("203"), evaluateString(input))
    }

    @Test
    fun `anonymous lambda call with multiple arguments`() {
        val input = "((lambda (x y) (+ x y)) 203 2)"

        assertSExpressionsMatch(LispNumber("205"), evaluateString(input))
    }

    @Test
    fun `anonymous lambda call with symbol`() {
        val input = "((λ (x) (+ x 1)) 3)"

        assertSExpressionsMatch(LispNumber("4"), evaluateString(input))
    }

    @Test
    fun `anonymous lambda call with too few arguments`() {
        assertThrows(TooFewArgumentsException::class.java) { evaluateString("((lambda (x) x))") }
    }

    @Test
    fun `anonymous lambda call with too many arguments`() {
        assertThrows(TooManyArgumentsException::class.java) { evaluateString("((lambda (x y) x) 1 2 3)") }
    }

    @Test
    fun `bad anonymous function call`() {
        assertThrows(UndefinedFunctionException::class.java) { evaluateString("((bad-lambda (x y) x) 1 2 3)") }
    }

    @Test
    fun `lexical closure`() {
        evaluateString("(setq increment-count (let ((counter 0)) (lambda () (setq counter (+ 1 counter)))))")

        assertSExpressionsMatch(parseString("1"), evaluateString("(funcall increment-count)"))
        assertSExpressionsMatch(parseString("2"), evaluateString("(funcall increment-count)"))
        assertSExpressionsMatch(parseString("3"), evaluateString("(funcall increment-count)"))
        assertSExpressionsMatch(parseString("4"), evaluateString("(funcall increment-count)"))
    }
}
