package function.builtin.special

import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString
import testutil.TypeAssertions.assertNil

@LispTestInstance
class BeginTest : SymbolAndFunctionCleaner() {

    @Test
    fun prognWithNoArguments() {
        assertNil(evaluateString("(progn)"))
    }

    @Test
    fun prognWithOneArgument() {
        assertSExpressionsMatch(parseString("1"), evaluateString("(progn 1)"))
    }

    @Test
    fun prognWithSeveralArguments() {
        assertSExpressionsMatch(parseString("5"), evaluateString("(progn 1 2 3 4 5)"))
    }

    @Test
    fun beginWithSeveralArguments() {
        assertSExpressionsMatch(parseString("5"), evaluateString("(begin 1 2 3 4 5)"))
    }

    @Test
    fun prognEvaluatesArgument() {
        assertSExpressionsMatch(parseString("1"), evaluateString("(progn (car '(1 2 3)))"))
    }

    @Test
    fun prognWithDifferentArgumentTypes() {
        assertSExpressionsMatch(parseString("pear"), evaluateString("(progn t nil '(1 2) 'pear)"))
    }
}
