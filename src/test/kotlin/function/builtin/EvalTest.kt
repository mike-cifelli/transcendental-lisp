package function.builtin

import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.BackquoteEvaluator.AtSignNotInCommaException
import function.builtin.Eval.Companion.lookupSymbol
import function.builtin.Eval.UndefinedFunctionException
import function.builtin.Eval.UndefinedSymbolException
import function.builtin.Eval.UnmatchedAtSignException
import function.builtin.Eval.UnmatchedCommaException
import function.builtin.special.Recur.RecurNotInTailPositionException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import sexpression.Nil
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class EvalTest : SymbolAndFunctionCleaner() {

    @Test
    fun evalNumber() {
        val input = "(eval 9)"

        assertSExpressionsMatch(parseString("9"), evaluateString(input))
    }

    @Test
    fun evalNil() {
        val input = "(eval ())"

        assertSExpressionsMatch(parseString("()"), evaluateString(input))
    }

    @Test
    fun evalUsesCurrentLexicalEnvironment() {
        val input = "(let ((x 1)) (eval '(+ x 1)))"

        assertSExpressionsMatch(parseString("2"), evaluateString(input))
    }

    @Test
    fun lookupKeywordSymbol() {
        val symbol = ":symbol"

        assertSExpressionsMatch(parseString(symbol), lookupSymbol(symbol)!!)
    }

    @Test
    fun lookupT() {
        val symbol = "T"

        assertSExpressionsMatch(parseString(symbol), lookupSymbol(symbol)!!)
    }

    @Test
    fun lookupNil() {
        val symbol = "NIL"

        assertSExpressionsMatch(parseString(symbol), lookupSymbol(symbol)!!)
    }

    @Test
    fun lookupUndefinedSymbol() {
        assertThat(Eval.lookupSymbol("undefined")).isNull()
    }

    @Test
    fun evalUndefinedFunction() {
        val input = "(funcall 'eval '(undefined))"

        assertThrows(UndefinedFunctionException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun evalUndefinedSymbol() {
        val input = "(eval undefined)"

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun evalWithDottedLambdaList() {
        val input = "(funcall 'eval (cons '+ 1))"

        assertThrows(DottedArgumentListException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun evalWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(eval '1 '2 '3)")
        }
    }

    @Test
    fun evalWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(eval)")
        }
    }

    @Test
    fun undefinedFunctionException_HasCorrectAttributes() {
        assertIsErrorWithMessage(UndefinedFunctionException(Nil))
    }

    @Test
    fun undefinedSymbolException_HasCorrectAttributes() {
        assertIsErrorWithMessage(UndefinedSymbolException(Nil))
    }

    @Test
    fun evalComma() {
        val input = ",a"

        assertThrows(UnmatchedCommaException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun evalAtSign() {
        val input = "@a"

        assertThrows(UnmatchedAtSignException::class.java) {
            evaluateString(input)
        }
    }

    @Test
    fun evalBackTick() {
        val input = "`(a b c)"

        assertSExpressionsMatch(parseString("(a b c)"), evaluateString(input))
    }

    @Test
    fun evalBackTickWithCommasAndAtSigns() {
        val input = "(let ((x '(1 2 3)) (y '(4 5 6)) (z 'apple)) `(start ,x ,@y ,z end))"

        assertSExpressionsMatch(parseString("(start (1 2 3) 4 5 6 apple end)"), evaluateString(input))
    }

    @Test
    fun evalBackTickOnComma() {
        val input = "`,9"

        assertSExpressionsMatch(parseString("9"), evaluateString(input))
    }

    @Test
    fun evalBackTickOnAtSign() {
        assertThrows(AtSignNotInCommaException::class.java) {
            evaluateString("`@9")
        }
    }

    @Test
    fun evalNestedBackquotes() {
        val input = "`,`,`,`,9"

        assertSExpressionsMatch(parseString("9"), evaluateString(input))
    }

    @Test
    fun unmatchedCommaException_HasCorrectAttributes() {
        assertIsErrorWithMessage(UnmatchedCommaException())
    }

    @Test
    fun unmatchedAtSignException_HasCorrectAttributes() {
        assertIsErrorWithMessage(UnmatchedAtSignException())
    }

    @Test
    fun evalQuoteInNestedList() {
        val input = "(let ((g 27)) `((,g)))"

        assertSExpressionsMatch(parseString("((27))"), evaluateString(input))
    }

    @Test
    fun evalAtSignInNestedList() {
        val input = "(let ((g '(1 2 3))) `((,@g)))"

        assertSExpressionsMatch(parseString("((1 2 3))"), evaluateString(input))
    }

    @Test
    fun evalNestedBackquotesInList() {
        val input = "`(,`(1 ,`2 ,@`(3)))"

        assertSExpressionsMatch(parseString("((1 2 3))"), evaluateString(input))
    }

    @Test
    fun scopeRestoredAfterFailure_Let() {
        evaluateString("(setq n 100)")

        try {
            evaluateString("(let ((n 200)) (begin 1 2 3 y))")
            fail("expected exception")
        } catch (e: UndefinedSymbolException) {
        }

        assertSExpressionsMatch(parseString("100"), evaluateString("n"))
    }

    @Test
    fun scopeRestoredAfterFailure_Defun() {
        evaluateString("(setq n 100)")

        try {
            evaluateString("(defun test (n) (begin 1 2 3 y))")
            evaluateString("(test 200)")
            fail("expected exception")
        } catch (e: UndefinedSymbolException) {
        }

        assertSExpressionsMatch(parseString("100"), evaluateString("n"))
    }

    @Test
    fun scopeRestoredAfterFailure_Lambda() {
        evaluateString("(setq n 100)")

        try {
            evaluateString("((lambda (n) (begin 1 2 3 y)) 200)")
            fail("expected exception")
        } catch (e: UndefinedSymbolException) {
        }

        assertSExpressionsMatch(parseString("100"), evaluateString("n"))
    }

    @Test
    fun scopeRestoredAfterFailure_Recur() {
        evaluateString("(setq n 100)")

        try {
            evaluateString("(defun tail-recursive (n) (begin (recur) 2))")
            evaluateString("(tail-recursive 200)")
            fail("expected exception")
        } catch (e: RecurNotInTailPositionException) {
        }

        assertSExpressionsMatch(parseString("100"), evaluateString("n"))
    }

    @Test
    fun scopeRestoredAfterFailure_Apply() {
        evaluateString("(setq n 100)")

        try {
            evaluateString("(defun tail-recursive (n) (begin (recur) 2))")
            evaluateString("(apply 'tail-recursive '(200))")
            fail("expected exception")
        } catch (e: RecurNotInTailPositionException) {
        }

        assertSExpressionsMatch(parseString("100"), evaluateString("n"))
    }
}
