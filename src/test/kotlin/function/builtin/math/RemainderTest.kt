package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.math.Divide.DivideByZeroException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class RemainderTest : SymbolAndFunctionCleaner() {

    @Test
    fun rem() {
        val input = "(rem 5 3)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun remainder() {
        val input = "(remainder 11 7)"

        assertSExpressionsMatch(LispNumber("4"), evaluateString(input))
    }

    @Test
    fun dividendGreaterThanDivisor() {
        val input = "(rem 21 19)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun dividendLessThanDivisor() {
        val input = "(rem 5 239)"

        assertSExpressionsMatch(LispNumber("5"), evaluateString(input))
    }

    @Test
    fun dividendEqualToDivisor() {
        val input = "(rem 5 5)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun dividendMultipleOfDivisor() {
        val input = "(rem 20 5)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun divisorOfOne() {
        val input = "(rem 5 1)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun dividendOfZero() {
        val input = "(rem 0 2309)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun negativeDividend() {
        val input = "(rem -23 25)"

        assertSExpressionsMatch(LispNumber("-23"), evaluateString(input))
    }

    @Test
    fun negativeDivisor() {
        val input = "(rem 5 -11)"

        assertSExpressionsMatch(LispNumber("5"), evaluateString(input))
    }

    @Test
    fun divisorOfZero() {
        assertThrows(DivideByZeroException::class.java) {
            evaluateString("(rem 5 0)")
        }
    }

    @Test
    fun remWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(rem 'a 'b)")
        }
    }

    @Test
    fun remWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(rem 1)")
        }
    }

    @Test
    fun remWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(rem 1 2 3)")
        }
    }
}
