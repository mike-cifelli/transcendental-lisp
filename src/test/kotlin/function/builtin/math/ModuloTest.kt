package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.math.Modulo.ModulusNotPositiveException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class ModuloTest : SymbolAndFunctionCleaner() {

    @Test
    fun mod() {
        val input = "(mod 5 3)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun modulo() {
        val input = "(modulo 11 7)"

        assertSExpressionsMatch(LispNumber("4"), evaluateString(input))
    }

    @Test
    fun moduloSymbol() {
        val input = "(% 8 5)"

        assertSExpressionsMatch(LispNumber("3"), evaluateString(input))
    }

    @Test
    fun dividendGreaterThanDivisor() {
        val input = "(mod 21 19)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun dividendLessThanDivisor() {
        val input = "(mod 5 239)"

        assertSExpressionsMatch(LispNumber("5"), evaluateString(input))
    }

    @Test
    fun dividendEqualToDivisor() {
        val input = "(mod 5 5)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun dividendMultipleOfDivisor() {
        val input = "(mod 20 5)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun divisorOfOne() {
        val input = "(mod 5 1)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun dividendOfZero() {
        val input = "(mod 0 2309)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun negativeDividend() {
        val input = "(mod -23 25)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun negativeDivisor() {
        assertThrows(ModulusNotPositiveException::class.java) {
            evaluateString("(mod 5 -10)")
        }
    }

    @Test
    fun divisorOfZero() {
        assertThrows(ModulusNotPositiveException::class.java) {
            evaluateString("(mod 5 0)")
        }
    }

    @Test
    fun modWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(mod 'a 'b)")
        }
    }

    @Test
    fun modWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(mod 1)")
        }
    }

    @Test
    fun modWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(mod 1 2 3)")
        }
    }

    @Test
    fun moduloNotPositiveException_HasCorrectAttributes() {
        assertIsErrorWithMessage(ModulusNotPositiveException())
    }
}
