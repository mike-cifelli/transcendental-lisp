package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.builtin.math.Divide.DivideByZeroException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString
import testutil.TestUtilities.parseString

@LispTestInstance
class DivideTest : SymbolAndFunctionCleaner() {

    @Test
    fun divideWithOne() {
        val input = "(/ 1)"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun divideWithTwo() {
        val input = "(/ 2)"

        assertSExpressionsMatch(parseString("0"), evaluateString(input))
    }

    @Test
    fun divideTwoNumbers() {
        val input = "(/ 24 3)"

        assertSExpressionsMatch(parseString("8"), evaluateString(input))
    }

    @Test
    fun divideSeveralNumbers() {
        val input = "(/ 256 2 2 8)"

        assertSExpressionsMatch(parseString("8"), evaluateString(input))
    }

    @Test
    fun divideTwoNumbersWithRemainder() {
        val input = "(/ 9 2)"

        assertSExpressionsMatch(parseString("4"), evaluateString(input))
    }

    @Test
    fun divideSeveralNumbersWithRemainder() {
        val input = "(/ 19 2 5)"

        assertSExpressionsMatch(parseString("1"), evaluateString(input))
    }

    @Test
    fun divideWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(/ 'x)")
        }
    }

    @Test
    fun divideWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(/)")
        }
    }

    @Test
    fun divideByZero() {
        assertThrows(DivideByZeroException::class.java) {
            evaluateString("(/ 2 0)")
        }
    }

    @Test
    fun divideByZeroException_HasCorrectAttributes() {
        assertIsErrorWithMessage(DivideByZeroException())
    }
}
