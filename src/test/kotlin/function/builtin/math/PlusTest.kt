package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class PlusTest : SymbolAndFunctionCleaner() {

    @Test
    fun plusWithNoArguments() {
        val input = "(+)"

        assertSExpressionsMatch(LispNumber("0"), evaluateString(input))
    }

    @Test
    fun plusWithOneNumber() {
        val input = "(+ 27)"

        assertSExpressionsMatch(LispNumber("27"), evaluateString(input))
    }

    @Test
    fun plusWithTwoNumbers() {
        val input = "(+ 5 3)"

        assertSExpressionsMatch(LispNumber("8"), evaluateString(input))
    }

    @Test
    fun plusWithManyNumbers_PositiveResult() {
        val input = "(+ 200 100 10 5)"

        assertSExpressionsMatch(LispNumber("315"), evaluateString(input))
    }

    @Test
    fun plusWithManyNumbers_NegativeResult() {
        val input = "(+ 100 (- 200) 20 5)"

        assertSExpressionsMatch(LispNumber("-75"), evaluateString(input))
    }

    @Test
    fun plusWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(+ 'a 'b)")
        }
    }
}
