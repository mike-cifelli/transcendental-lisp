package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class MinusTest : SymbolAndFunctionCleaner() {

    @Test
    fun minusWithOneNumber() {
        val input = "(- 27)"

        assertSExpressionsMatch(LispNumber("-27"), evaluateString(input))
    }

    @Test
    fun minusWithTwoNumbers() {
        val input = "(- 5 3)"

        assertSExpressionsMatch(LispNumber("2"), evaluateString(input))
    }

    @Test
    fun minusWithManyNumbers_PositiveResult() {
        val input = "(- 200 100 10 5)"

        assertSExpressionsMatch(LispNumber("85"), evaluateString(input))
    }

    @Test
    fun minusWithManyNumbers_NegativeResult() {
        val input = "(- 100 200 20 5)"

        assertSExpressionsMatch(LispNumber("-125"), evaluateString(input))
    }

    @Test
    fun minusWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(- 'a 'b)")
        }
    }

    @Test
    fun minusWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(-)")
        }
    }
}
