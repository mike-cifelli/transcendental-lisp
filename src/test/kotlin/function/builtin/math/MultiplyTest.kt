package function.builtin.math

import function.ArgumentValidator.BadArgumentTypeException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class MultiplyTest : SymbolAndFunctionCleaner() {

    @Test
    fun multiplyWithNoArguments() {
        val input = "(*)"

        assertSExpressionsMatch(LispNumber("1"), evaluateString(input))
    }

    @Test
    fun multiplyWithOneNumber() {
        val input = "(* 8)"

        assertSExpressionsMatch(LispNumber("8"), evaluateString(input))
    }

    @Test
    fun multiplyWithTwoNumbers() {
        val input = "(* 5 3)"

        assertSExpressionsMatch(LispNumber("15"), evaluateString(input))
    }

    @Test
    fun multiplyWithManyNumbers_PositiveResult() {
        val input = "(* 2 3 5 1)"

        assertSExpressionsMatch(LispNumber("30"), evaluateString(input))
    }

    @Test
    fun multiplyWithManyNumbers_NegativeResult() {
        val input = "(* 3 (- 2) 10 2)"

        assertSExpressionsMatch(LispNumber("-120"), evaluateString(input))
    }

    @Test
    fun multiplyWithNonNumber() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(* 'a 'b)")
        }
    }
}
