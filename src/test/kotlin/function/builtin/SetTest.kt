package function.builtin

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import function.builtin.Eval.UndefinedSymbolException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sexpression.LispNumber
import table.SymbolTable
import testutil.LispTestInstance
import testutil.SymbolAndFunctionCleaner
import testutil.TestUtilities.assertSExpressionsMatch
import testutil.TestUtilities.evaluateString

@LispTestInstance
class SetTest : SymbolAndFunctionCleaner() {

    @Test
    fun set() {
        evaluateString("(set 'a 23)")
        assertSExpressionsMatch(LispNumber("23"), evaluateString("a"))
    }

    @Test
    fun lookupDefinedSymbol() {
        evaluateString("(set 'a 23)")
        assertSExpressionsMatch(LispNumber("23"), executionContext.lookupSymbolValue("A")!!)
    }

    @Test
    fun lookupUndefinedSymbol() {
        assertThat(executionContext.lookupSymbolValue("A")).isNull()
    }

    @Test
    fun setGlobalVariable() {
        evaluateString("(set 'a 23)")
        val global = executionContext.scope
        executionContext.scope = SymbolTable(global)

        evaluateString("(set 'a 94)")
        executionContext.scope = global
        assertSExpressionsMatch(LispNumber("94"), evaluateString("a"))
    }

    @Test
    fun setLocalVariableDefined_DoesNotSetGlobal() {
        val global = executionContext.scope
        val local = SymbolTable(global)
        local["A"] = LispNumber("99")
        executionContext.scope = local

        evaluateString("(set 'a 94)")
        executionContext.scope = global

        assertThrows(UndefinedSymbolException::class.java) {
            evaluateString("a")
        }
    }

    @Test
    fun setLocalVariableUndefined_SetsGlobal() {
        val global = executionContext.scope
        val local = SymbolTable(global)
        executionContext.scope = local

        evaluateString("(set 'a 94)")
        executionContext.scope = global
        assertSExpressionsMatch(LispNumber("94"), evaluateString("a"))
    }

    @Test
    fun setWithNonSymbol() {
        assertThrows(BadArgumentTypeException::class.java) {
            evaluateString("(set '1 2)")
        }
    }

    @Test
    fun setWithTooFewArguments() {
        assertThrows(TooFewArgumentsException::class.java) {
            evaluateString("(set 'x)")
        }
    }

    @Test
    fun setWithTooManyArguments() {
        assertThrows(TooManyArgumentsException::class.java) {
            evaluateString("(set 'a 'b 'c)")
        }
    }
}
