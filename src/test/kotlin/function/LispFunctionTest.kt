package function

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.Nil
import testutil.LispTestInstance

@LispTestInstance
class LispFunctionTest {

    @Test
    fun `arguments are evaluated`() {
        val lispFunction = object : LispFunction() {
            override fun call(argumentList: Cons) = Nil
        }

        assertThat(lispFunction.isArgumentListEvaluated).isTrue()
    }
}
