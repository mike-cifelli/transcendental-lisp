package function

import function.ArgumentValidator.BadArgumentTypeException
import function.ArgumentValidator.DottedArgumentListException
import function.ArgumentValidator.TooFewArgumentsException
import function.ArgumentValidator.TooManyArgumentsException
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import sexpression.Cons
import sexpression.LispString
import sexpression.Nil
import sexpression.SExpression
import sexpression.Symbol
import sexpression.Symbol.Companion.T
import testutil.LispTestInstance
import testutil.TestUtilities.assertIsErrorWithMessage

@LispTestInstance
class ArgumentValidatorTest {

    companion object {
        private const val FUNCTION_NAME = "TEST"
    }

    private lateinit var validator: ArgumentValidator

    private fun makeArgumentListOfSize(size: Int) =
        (0 until size).fold(Nil as Cons) { list, _ -> Cons(Nil, list) }

    @BeforeEach
    fun setUp() {
        validator = ArgumentValidator(FUNCTION_NAME)
    }

    @Test
    fun noConstraints_DoesNotThrowExceptionWithNoArguments() {
        validator.validate(makeArgumentListOfSize(0))
    }

    @Test
    fun noConstraints_DoesNotThrowExceptionWithOneArgument() {
        validator.validate(makeArgumentListOfSize(1))
    }

    @Test
    fun noConstraints_DoesNotThrowExceptionWithManyArguments() {
        validator.validate(makeArgumentListOfSize(20))
    }

    @Test
    fun tooFewArgumentsWithMinimumSet_ThrowsException() {
        validator.setMinimumNumberOfArguments(1)

        assertThrows(TooFewArgumentsException::class.java) {
            validator.validate(makeArgumentListOfSize(0))
        }
    }

    @Test
    fun tooManyArgumentsWithMaximumSet_ThrowsException() {
        validator.setMaximumNumberOfArguments(1)

        assertThrows(TooManyArgumentsException::class.java) {
            validator.validate(makeArgumentListOfSize(2))
        }
    }

    @Test
    fun exactNumberOfArguments_DoesNotThrowException() {
        validator.setExactNumberOfArguments(5)
        validator.validate(makeArgumentListOfSize(5))
    }

    @Test
    fun tooFewArgumentsWithExactSet_ThrowsException() {
        validator.setExactNumberOfArguments(3)

        assertThrows(TooFewArgumentsException::class.java) {
            validator.validate(makeArgumentListOfSize(2))
        }
    }

    @Test
    fun tooManyArgumentsWithExactSet_ThrowsException() {
        validator.setExactNumberOfArguments(3)

        assertThrows(TooManyArgumentsException::class.java) {
            validator.validate(makeArgumentListOfSize(4))
        }
    }

    @Test
    fun tooManyArgumentsException_HasCorrectAttributes() {
        assertIsErrorWithMessage(TooManyArgumentsException(FUNCTION_NAME, Nil))
    }

    @Test
    fun tooFewArgumentsException_HasCorrectAttributes() {
        assertIsErrorWithMessage(TooFewArgumentsException(FUNCTION_NAME, Nil))
    }

    @Test
    fun badArgumentTypeException_HasCorrectAttributes() {
        assertIsErrorWithMessage(BadArgumentTypeException(FUNCTION_NAME, Nil, SExpression::class.java))
    }

    @Test
    fun correctArgumentType_DoesNotThrowException() {
        validator.setEveryArgumentExpectedType(Nil::class.java)
        validator.validate(makeArgumentListOfSize(1))
    }

    @Test
    fun badArgumentType_ThrowsException() {
        validator.setEveryArgumentExpectedType(LispString::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(makeArgumentListOfSize(1))
        }
    }

    @Test
    fun correctFirstAndRestArgumentTypes_DoesNotThrowException() {
        val argumentList = Cons(T, Cons(Nil, Nil))

        validator.setFirstArgumentExpectedType(Symbol::class.java)
        validator.setTrailingArgumentExpectedType(Cons::class.java)
        validator.validate(argumentList)
    }

    @Test
    fun badFirstArgumentType_ThrowsException() {
        val argumentList = Cons(T, Cons(Nil, Nil))

        validator.setFirstArgumentExpectedType(Cons::class.java)
        validator.setTrailingArgumentExpectedType(Cons::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(argumentList)
        }
    }

    @Test
    fun badTrailingArgumentType_ThrowsException() {
        val argumentList = Cons(T, Cons(Nil, Nil))

        validator.setFirstArgumentExpectedType(Symbol::class.java)
        validator.setTrailingArgumentExpectedType(Symbol::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(argumentList)
        }
    }

    @Test
    fun expectedTypeWithNoDisplayName_DoesNotCauseNPE() {
        val argumentList = Cons(T, Cons(Nil, Nil))
        val withoutDisplayName = object : SExpression() {

        }

        validator.setEveryArgumentExpectedType(withoutDisplayName.javaClass)

        try {
            validator.validate(argumentList)
        } catch (e: BadArgumentTypeException) {
            assertIsErrorWithMessage(e)
        }
    }

    @Test
    fun givenDottedArgumentList_ThrowsException() {
        val argumentList = Cons(T, T)

        assertThrows(DottedArgumentListException::class.java) {
            validator.validate(argumentList)
        }
    }

    @Test
    fun givenLargeDottedArgumentList_ThrowsException() {
        val argumentList = Cons(T, Cons(T, T))

        assertThrows(DottedArgumentListException::class.java) {
            validator.validate(argumentList)
        }
    }

    @Test
    fun dottedArgumentListException_HasCorrectAttributes() {
        assertIsErrorWithMessage(DottedArgumentListException(FUNCTION_NAME, Nil))
    }

    @Test
    fun excludedFirstArgumentType_DoesNotAffectTrailingArguments() {
        validator.setFirstArgumentExcludedType(Nil::class.java)
        validator.validate(Cons(T, Cons(Nil, Nil)))
    }

    @Test
    fun excludedTrailingArgumentType_DoesNotAffectFirstArgument() {
        validator.setTrailingArgumentExcludedType(Nil::class.java)
        validator.validate(Cons(Nil, Cons(T, Nil)))
    }

    @Test
    fun excludedFirstArgumentType_ThrowsException() {
        validator.setFirstArgumentExcludedType(Nil::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(Cons(Nil, Cons(T, Nil)))
        }
    }

    @Test
    fun excludedTrailingArgumentType_ThrowsException() {
        validator.setTrailingArgumentExcludedType(Nil::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(Cons(T, Cons(Nil, Nil)))
        }
    }

    @Test
    fun excludedArgumentType_ThrowsExceptionOnFirstArgument() {
        validator.setEveryArgumentExcludedType(Nil::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(Cons(Nil, Cons(T, Nil)))
        }
    }

    @Test
    fun excludedArgumentType_ThrowsExceptionOnTrailingArgument() {
        validator.setEveryArgumentExcludedType(Nil::class.java)

        assertThrows(BadArgumentTypeException::class.java) {
            validator.validate(Cons(T, Cons(Nil, Nil)))
        }
    }
}
