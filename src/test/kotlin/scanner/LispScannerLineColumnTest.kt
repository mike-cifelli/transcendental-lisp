package scanner

import file.FilePosition
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

import testutil.TestUtilities.createInputStreamFromString

@LispTestInstance
class LispScannerLineColumnTest {

    companion object {
        const val TEST_FILE = "testFile"
    }

    private data class LineColumn(val line: Int = 0, val column: Int = 0)

    private fun assertTokenLineAndColumnsMatch(input: String, expectedLineColumnList: Array<LineColumn>) {
        val stringInputStream = createInputStreamFromString(input)
        val lispScanner = LispScanner(stringInputStream, TEST_FILE)

        for (lineColumn in expectedLineColumnList) {
            val position = lispScanner.nextToken.position
            val expectedPosition = FilePosition(TEST_FILE, lineColumn.line, lineColumn.column)

            assertThat(position).isEqualTo(expectedPosition)
        }
    }

    @Test
    fun `location of EOF`() {
        val input = ""
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 0))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of a string`() {
        val input = "\"string\""
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of a string with a trailing space`() {
        val input = "\"string\" "
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of an identifier`() {
        val input = "identifier"
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of an identifier with a trailing space`() {
        val input = "identifier "
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of a number`() {
        val input = "123456789"
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `location of a number with a trailing space`() {
        val input = "123456789 "
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `locations of multiple strings`() {
        val input = "\"string1\" \n \"string2 \n with newline\" \n  \"string3\""
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1), LineColumn(2, 2), LineColumn(4, 3))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `locations of a quoted list`() {
        val input = "'(1 2 3 4 5)"
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1),
                                              LineColumn(1, 2),
                                              LineColumn(1, 3),
                                              LineColumn(1, 5),
                                              LineColumn(1, 7),
                                              LineColumn(1, 9),
                                              LineColumn(1, 11),
                                              LineColumn(1, 12))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `locations of a list spanning multiple lines`() {
        val input = " ( 1 2 \n 3 4 \n5 ) "
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 2),
                                              LineColumn(1, 4),
                                              LineColumn(1, 6),
                                              LineColumn(2, 2),
                                              LineColumn(2, 4),
                                              LineColumn(3, 1),
                                              LineColumn(3, 3))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }

    @Test
    fun `locations of numbers separated by a comment`() {
        val input = "12;comment\n34"
        val expectedLinesAndColumns = arrayOf(LineColumn(1, 1), LineColumn(2, 1))

        assertTokenLineAndColumnsMatch(input, expectedLinesAndColumns)
    }
}
