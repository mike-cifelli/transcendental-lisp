package scanner

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import scanner.LispScanner.UnterminatedStringException
import testutil.LispTestInstance
import testutil.TestUtilities.assertIsErrorWithMessage
import testutil.TestUtilities.createInputStreamFromString
import token.AtSign
import token.Backquote
import token.Comma
import token.Eof
import token.Identifier
import token.LeftParenthesis
import token.Number
import token.QuoteMark
import token.QuotedString
import token.RightParenthesis
import token.Token
import token.TokenFactory.BadCharacterException

@LispTestInstance
class LispScannerTypeTest {

    private val expectedTypes = mutableListOf<Class<out Token>>()

    private fun assertTokenTypesMatch(input: String) {
        val stringInputStream = createInputStreamFromString(input)
        val lispScanner = LispScanner(stringInputStream, "testFile")

        for (type in expectedTypes)
            assertThat(type.isInstance(lispScanner.nextToken)).isTrue()

        assertThat(lispScanner.nextToken is Eof).isTrue()
    }

    @BeforeEach
    fun setUp() {
        expectedTypes.clear()
    }

    @Test
    fun `types for an empty file`() {
        val input = ""

        assertTokenTypesMatch(input)
    }

    @Test
    fun `bad character is not accepted`() {
        val input = "["

        assertThrows(BadCharacterException::class.java) { assertTokenTypesMatch(input) }
    }

    @Test
    fun `BadCharacterException is cool`() {
        val input = "abc\ndef["
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Identifier::class.java)

        try {
            assertTokenTypesMatch(input)
        } catch (e: BadCharacterException) {
            assertIsErrorWithMessage(e)
        }
    }

    @Test
    fun `types for nil`() {
        val input = "()"
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(RightParenthesis::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a list of numbers`() {
        val input = "(1 2)"
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(Number::class.java)
        expectedTypes.add(Number::class.java)
        expectedTypes.add(RightParenthesis::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a string`() {
        val input = "\"string\""
        expectedTypes.add(QuotedString::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a string with escaped double quote`() {
        val input = "\"string \n hi \\\" bye\""
        expectedTypes.add(QuotedString::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a string with an escaped double quote and comment`() {
        val input = "\"string \n hi \\\" ; bye\""
        expectedTypes.add(QuotedString::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for an unterminated string`() {
        val input = "\"oh no!"
        expectedTypes.add(QuotedString::class.java)

        assertThrows(UnterminatedStringException::class.java) { assertTokenTypesMatch(input) }
    }

    @Test
    fun `UnterminatedStringException is cool`() {
        val input = "\"oh no!"
        expectedTypes.add(QuotedString::class.java)

        try {
            assertTokenTypesMatch(input)
        } catch (e: UnterminatedStringException) {
            assertIsErrorWithMessage(e)
        }
    }

    @Test
    fun `types for an identifier`() {
        val input = "abcdefgHIJKLMNOP1234"
        expectedTypes.add(Identifier::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for prefixed identifiers`() {
        val input = "-a +b"
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Identifier::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun givenSingleDigitNumber_ReturnsCorrectTypes() {
        val input = "1"
        expectedTypes.add(Number::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed negative number`() {
        val input = "-1"
        expectedTypes.add(Number::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed positive number`() {
        val input = "+1"
        expectedTypes.add(Number::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed negative number and an identifier`() {
        val input = "-1apple"
        expectedTypes.add(Number::class.java)
        expectedTypes.add(Identifier::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed positive number and an identifier`() {
        val input = "+1apple"
        expectedTypes.add(Number::class.java)
        expectedTypes.add(Identifier::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed negative number and a string`() {
        val input = "-1\"apple\""
        expectedTypes.add(Number::class.java)
        expectedTypes.add(QuotedString::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a prefixed positive number and a string`() {
        val input = "+1\"apple\""
        expectedTypes.add(Number::class.java)
        expectedTypes.add(QuotedString::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a mulitple digit number`() {
        val input = "1234567890"
        expectedTypes.add(Number::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a quote`() {
        val input = "'"
        expectedTypes.add(QuoteMark::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for several expressions with no whitespace`() {
        val input = "xxx\"hi\"999()'aaa"
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(QuotedString::class.java)
        expectedTypes.add(Number::class.java)
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(RightParenthesis::class.java)
        expectedTypes.add(QuoteMark::class.java)
        expectedTypes.add(Identifier::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a function call`() {
        val input = "(defun myFunction (x)\n  (print x))"
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(RightParenthesis::class.java)
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(RightParenthesis::class.java)
        expectedTypes.add(RightParenthesis::class.java)

        assertTokenTypesMatch(input)
    }

    @Test
    fun `types for a back tick expression`() {
        val input = "`(list ,a ,@b)"
        expectedTypes.add(Backquote::class.java)
        expectedTypes.add(LeftParenthesis::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Comma::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(Comma::class.java)
        expectedTypes.add(AtSign::class.java)
        expectedTypes.add(Identifier::class.java)
        expectedTypes.add(RightParenthesis::class.java)

        assertTokenTypesMatch(input)
    }
}
