package scanner

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testutil.LispTestInstance

import testutil.TestUtilities.createInputStreamFromString

@LispTestInstance
class LispScannerTextTest {

    private fun assertTokenTextMatches(input: String, expectedText: Array<String>) {
        val lispScanner = createLispScanner(input)

        for (expected in expectedText)
            assertThat(lispScanner.nextToken.text).isEqualTo(expected)
    }

    private fun assertTokenTextMatches(input: String, expected: String) {
        val lispScanner = createLispScanner(input)

        assertThat(lispScanner.nextToken.text).isEqualTo(expected)
    }

    private fun createLispScanner(input: String) = LispScanner(createInputStreamFromString(input), "testFile")

    private fun assertInputFileNameMatches(input: String, expectedInputFileName: String) {
        val stringInputStream = createInputStreamFromString(input)
        val lispScanner = LispScanner(stringInputStream, expectedInputFileName)
        val (fileName) = lispScanner.nextToken.position

        assertThat(fileName).isEqualTo(expectedInputFileName)
    }

    @Test
    fun `an empty stream records the correct file name`() {
        val input = ""
        val expectedFileName = "testFileName"

        assertInputFileNameMatches(input, expectedFileName)
    }

    @Test
    fun `scan parenthesis`() {
        val input = "()"
        val expected = arrayOf("(", ")")

        assertTokenTextMatches(input, expected)
    }

    @Test
    fun `scan a quote`() {
        val input = "'"
        val expected = "'"

        assertTokenTextMatches(input, expected)
    }

    @Test
    fun `scan EOF`() {
        val input = ""
        val expected = "EOF"

        assertTokenTextMatches(input, expected)
    }

    @Test
    fun `scan an identifier`() {
        val input = "identifier"

        assertTokenTextMatches(input, input)
    }

    @Test
    fun `scan a number`() {
        val input = "192837456"

        assertTokenTextMatches(input, input)
    }

    @Test
    fun `scan a string`() {
        val input = "\"String!!! \n More... \""

        assertTokenTextMatches(input, input)
    }

    @Test
    fun `scan a number followed by a comment`() {
        val input = "192837456;comment"
        val expected = "192837456"

        assertTokenTextMatches(input, expected)
    }

    @Test
    fun `scan identifiers separated by a comment`() {
        val input = "abc123;comment\nabc222"
        val expected = arrayOf("abc123", "abc222")

        assertTokenTextMatches(input, expected)
    }

    @Test
    fun `scan a back tick expression`() {
        val input = "`(list ,a ,@b)"
        val expected = arrayOf("`", "(", "list", ",", "a", ",", "@", "b", ")")

        assertTokenTextMatches(input, expected)
    }
}
