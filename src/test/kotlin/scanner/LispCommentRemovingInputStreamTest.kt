package scanner

import error.Severity.CRITICAL
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import scanner.LispInputStream.MaximumUnreadsExceededException
import stream.LispIOException
import testutil.LispTestInstance
import testutil.TestUtilities.createIOExceptionThrowingInputStream
import testutil.TestUtilities.createInputStreamFromString

@LispTestInstance
class LispCommentRemovingInputStreamTest {

    private fun getLispCommentRemovingInputStreamResult(inputString: String) =
        readInputStreamIntoString(createLispInputStream(inputString))

    private fun createLispInputStream(inputString: String) =
        LispCommentRemovingInputStream(createInputStreamFromString(inputString))

    private fun readInputStreamIntoString(inputStream: LispInputStream) = StringBuilder().apply {
        for (c in inputStream)
            append(c.toChar())
    }.toString()

    @Test
    fun `no bytes in gives no bytes out`() {
        val input = ""

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `one character is not removed`() {
        val input = "x"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `all non-comment characters are preserved`() {
        val input = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=~!@#$%^&*()_+[]\\',./{}|:\"<>?"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `one comment is removed`() {
        val input = ";comment"
        val expectedResult = ""

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }

    @Test
    fun `multiple comments are removed`() {
        val input = ";comment1\n;comment2\n;comment3"
        val expectedResult = "\n\n"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }

    @Test
    fun `NIL is not removed`() {
        val input = "()"
        val expectedResult = "()"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }

    @Test
    fun `interior comment is removed`() {
        val input = "(;this is a comment\n)"
        val expectedResult = "(\n)"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }

    @Test
    fun `comment in a string is not removed`() {
        val input = "\"string;this should remain\""

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `comment in a string with a newline is not removed`() {
        val input = "\"string;this should\n remain\""

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `comment in a strign with an escaped double quote is not removed`() {
        val input = "\"string \\\" ;this should remain\""

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(input)
    }

    @Test
    fun `only the comments are removed from several lines with expressions`() {
        val input = ";first comment \n '(1 2 3) \n ;second comment \n (defun add1 (x) (+ x 1)) ;third comment"
        val expectedResult = "\n '(1 2 3) \n \n (defun add1 (x) (+ x 1)) "

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }

    @Test
    fun `reread one character`() {
        val input = "abc"
        val expectedResult = 'a'
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.unreadLastCharacter()

        assertThat(lispInputStream.read().toLong()).isEqualTo(expectedResult.toLong())
    }

    @Test
    fun `reread the same character multiple times`() {
        val input = "abc"
        val expectedResult = 'a'
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.unreadLastCharacter()
        lispInputStream.read()
        lispInputStream.unreadLastCharacter()
        lispInputStream.read()
        lispInputStream.unreadLastCharacter()

        assertThat(lispInputStream.read().toLong()).isEqualTo(expectedResult.toLong())
    }

    @Test
    fun `next character is read after a read and unread`() {
        val input = "abc"
        val expectedResult = 'b'
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.unreadLastCharacter()
        lispInputStream.read()

        assertThat(lispInputStream.read().toLong()).isEqualTo(expectedResult.toLong())
    }

    @Test
    fun `read a newline after an unread at the end of a comment`() {
        val input = "a;123\n"
        val expectedResult = '\n'
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.read()
        lispInputStream.unreadLastCharacter()

        assertThat(lispInputStream.read().toLong()).isEqualTo(expectedResult.toLong())
    }

    @Test
    fun `unable to unread more than one character`() {
        val input = "abc"
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.unreadLastCharacter()
        assertThrows(MaximumUnreadsExceededException::class.java) { lispInputStream.unreadLastCharacter() }
    }

    @Test
    fun `MaximumUnreadsExceededException is cool`() {
        val input = "abc"
        val lispInputStream = createLispInputStream(input)

        lispInputStream.read()
        lispInputStream.unreadLastCharacter()

        try {
            lispInputStream.unreadLastCharacter()
        } catch (e: MaximumUnreadsExceededException) {
            assertThat(e.severity).isEqualTo(CRITICAL)
        }
    }

    @Test
    fun `IOException is converted to LispIOException`() {
        val ioExceptionThrowingInputStream = createIOExceptionThrowingInputStream()
        val lispInputStream = LispCommentRemovingInputStream(ioExceptionThrowingInputStream)

        assertThrows(LispIOException::class.java) { lispInputStream.read() }
    }

    @Test
    fun `LispIOException is cool`() {
        val ioExceptionThrowingInputStream = createIOExceptionThrowingInputStream()
        val lispInputStream = LispCommentRemovingInputStream(ioExceptionThrowingInputStream)

        try {
            lispInputStream.read()
        } catch (e: LispIOException) {
            assertThat(e.message).isNotEmpty()
            assertThat(e.severity).isEqualTo(CRITICAL)
        }
    }

    @Test
    fun `read a unicode character`() {
        val input = "λ"
        val expectedResult = "λ"

        assertThat(getLispCommentRemovingInputStreamResult(input)).isEqualTo(expectedResult)
    }
}
