package acceptance.fixture

import application.LispMain
import environment.RuntimeEnvironment
import interpreter.LispInterpreter
import interpreter.LispInterpreterBuilder
import table.ExecutionContext
import table.FunctionTable
import util.Path

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.PrintStream

class LispInterpreterFixture {

    fun evaluateText(input: String): String {
        RuntimeEnvironment.inputName = "fitnesse"
        RuntimeEnvironment.input = ByteArrayInputStream(input.toByteArray())
        RuntimeEnvironment.path = ""

        return evaluate()
    }

    @Throws(FileNotFoundException::class)
    fun evaluateFile(inputFile: String): String {
        RuntimeEnvironment.inputName = inputFile
        RuntimeEnvironment.input = FileInputStream(inputFile)
        RuntimeEnvironment.path = Path.getPathPrefix(inputFile)

        return evaluate()
    }

    private fun evaluate(): String {
        interpreter.interpret()
        val output = outputStream.toString()
        outputStream.reset()

        return output.trim()
    }

    companion object {

        private val outputStream = ByteArrayOutputStream()
        private lateinit var interpreter: LispInterpreter

        @JvmStatic
        fun resetInterpreter() {
            cleanUp()
            buildInterpreter()
        }

        @JvmStatic
        fun cleanUp() {
            LispInterpreterBuilder.reset()
            FunctionTable.resetFunctionTable()
            ExecutionContext.clearContext()
            RuntimeEnvironment.reset()
        }

        private fun buildInterpreter() {
            LispInterpreterBuilder.setOutput(PrintStream(outputStream))
            LispInterpreterBuilder.setErrorOutput(PrintStream(outputStream))
            LispInterpreterBuilder.setNotInteractive()
            LispInterpreterBuilder.setLanguageFileNames(*LispMain.LANGUAGE_FILE_NAMES)
            LispInterpreterBuilder.setTerminationFunction { }
            LispInterpreterBuilder.setErrorTerminationFunction { throw RuntimeException("Error Termination") }

            interpreter = LispInterpreterBuilder.build()
        }
    }
}
