package acceptance

import fitnesse.junit.FitNesseRunner
import org.junit.runner.RunWith

@RunWith(FitNesseRunner::class)
@FitNesseRunner.Suite("TranscendentalLisp")
@FitNesseRunner.FitnesseDir("fitnesse")
@FitNesseRunner.ConfigFile("fitnesse/test.properties")
@FitNesseRunner.OutputDir("fitnesse/fitnesse-results")
class AcceptanceTest
